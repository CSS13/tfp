;'(a b c d)
;'(1 2 3 4)
;==>
;((a . 4)
; (b . 3)
; (c . 2)
; (d . 1))
;
;Mutual list right_fold

(define mutual_list_fold_naive
  (lambda (symlist natlist)
    (letrec ([visit (lambda(symlist natlist)
                      (if (pair? (cdr natlist))
                        (let ([vis (visit symlist (cdr natlist))])
                        ; Inductive case
                        (list
                          (cdr (car vis))
                          (cons
                            (car (cdr vis))
                            (cons (car (cdr (car vis))) (car natlist))
                        )))
                        ; Base case
                        (list
                          symlist
                          (cons (car symlist) (car natlist)))
                        ))])
      (car (cdr (visit symlist natlist))))))
(mutual_list_fold_naive '(a b c d) '(1 2 3 4))

(list (cons 1 2) (cons 3 4))

(define fold-right_binary-tree
  (lambda (lea nod err)
    (lambda (v_init)
      (letrec ([visit (lambda (v)
                        (cond
                          [(number? v)
                           (lea v)]
                          [(pair? v)
                           (nod (visit (car v))
                                (visit (cdr v)))]
                          [else
                           (err v)]))])
        (visit v_init)))))

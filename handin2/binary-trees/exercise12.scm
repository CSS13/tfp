(load "bt.scm")
(load "exercise6.scm")

(define bt-width_alt
  (lambda (init_v)
    (apply max
      (((fold-right_binary-tree (lambda (n)
                                 (lambda ()
                                   '(1)))
                               (lambda (r1 r2)
                                 (lambda ()
                                   (append '(1)
                                           (map sum-of-list
                                                (zero-padded-zip (r1) (r2))))))
                               (lambda (v)
                                 (lambda ()
                                   (errorf 'bt-width_alt
                                           "not a proper binary tree: ~s"
                                           (v)))))
       init_v)))))

(test-bt-width bt-width_alt)

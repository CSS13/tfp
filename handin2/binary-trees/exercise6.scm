(load "../unit-test.scm")
(load "../util.scm")

;; Describe how to compute the width of a binary tree
;; In english:
;;   The good idea for computing the width is to realize we would like to know
;;   how many nodes are at each level of the binary tree.
;;   Thus we would like to build up a list of the node count at each level of
;;   the tree recursivly.

;; Logically:
;;   //

;; Functionally:
(define test-bt-width
  (lambda (candidate)
    (and-all (try-candidate 'test-bt-width
                            candidate
                            1
                            1)
             (try-candidate 'test-bt-width
                            candidate
                            2
                            (cons 1
                                  2))
             (try-candidate 'test-bt-width
                            candidate
                            2
                            (cons (cons 11
                                        12)
                                  2))
             (try-candidate 'test-bt-width
                            candidate
                            4
                            (cons (cons 1
                                        2)
                                  (cons 3
                                        4)))
             (try-candidate 'test-bt-width
                            candidate
                            6
                            (cons (cons (cons 1
                                              2)
                                        0)
                                  (cons (cons 3
                                              4)
                                        (cons 5
                                              6))))
             (try-candidate 'test-bt-width
                            candidate
                            6
                            (cons (cons (cons 1
                                              2)
                                        0)
                                  (cons (cons (cons 31
                                                    32)
                                              4)
                                        (cons 5
                                              (cons 61
                                                    62)))))
             ;;; etc.
             )))

(define zeroes
  (lambda (n)
    (((fold-right-nat (lambda ()
                        '())
                      (lambda (p)
                        (lambda ()
                          (append '(0) (p)))))
      n))))

(define zero-padded
  (lambda (xs n)
    (if (< n (length xs))
        xs
        (append xs (zeroes (- n (length xs)))))))

(define zero-padded-zip
  (lambda (xs ys)
    (map list (zero-padded xs (length ys))
              (zero-padded ys (length xs)))))

(define sum-of-list
  (lambda (x)
    (apply + x)))

(define bt-width
  (lambda (init_v)
    (letrec ([visit (lambda (v)
                      (cond
                        [(number? v)
                         '(1)]
                        [(pair? v)
                         (append '(1)
                                 (map sum-of-list
                                      (zero-padded-zip (visit (car v))
                                                       (visit (cdr v)))))]
                        [else
                         (errorf 'bt-width
                                 "not a proper binary tree: ~s"
                                 v)]))])
      (apply max (visit init_v)))))

(test-bt-width bt-width)

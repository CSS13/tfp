(load "bt.scm")
(load "exercise8.scm")

(define well-balanced_alt?
  (lambda (init_v)
    (car
      (((fold-right_binary-tree (lambda (n)
                                 (lambda ()
                                   (cons (positive? n) n)))
                               (lambda (r1 r2)
                                 (lambda ()
                                   (let ([lhs (r1)])
                                     (let ([rhs (r2)])
                                       (cons (and (car lhs)
                                                  (car rhs)
                                                  (= (cdr lhs)
                                                     (cdr rhs)))
                                             (+ (cdr lhs)
                                                (cdr rhs)))))))
                               (lambda (v)
                                 (lambda ()
                                   (errorf 'well-balanced-alt?
                                           "not a proper binary tree: ~s"
                                           (v)))))
       init_v)))))

(test-well-balanced? well-balanced_alt?)

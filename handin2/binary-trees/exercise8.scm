(load "../unit-test.scm")

;; Describe how to check whether a binary tree represents a well-balanced mobile
;; In english:
;;   The good idea for computing the balance is to realize we would like to return
;;   a pair at each level of the binary tree. 
;;   The car containing 'balanceness' of the subtree.
;;   The cdr containing the total weight of the subtree.
;;  This allows us to inductively check the balance of subtrees 
;;   and therefor of the entire tree.

;; Logically:
;;  //

;; Functionally:
(define test-well-balanced?
  (lambda (candidate)
    (and-all (try-candidate 'test-well-balanced?
                            candidate
                            #t
                            1)
             (try-candidate 'test-well-balanced?
                            candidate
                            #t
                            '(1 . 1))
             (try-candidate 'test-well-balanced?
                            candidate
                            #t
                            '(2 . (1 . 1)))
             (try-candidate 'test-well-balanced?
                            candidate
                            #f
                            '(1 . (1 . 1)))
             ;;;
             )))

(define well-balanced?
  (lambda (init_v)
    (letrec ([visit (lambda (v)
                      (cond
                        [(number? v)
                         (cons (positive? v) v)]
                        [(pair? v)
                         (let ([lhs (visit (car v))])
                           (let ([rhs (visit (cdr v))])
                             (cons (and (car lhs)
                                        (car rhs)
                                        (= (cdr lhs)
                                           (cdr rhs)))
                                   (+ (cdr lhs)
                                      (cdr rhs)))))]
                        [else
                         (errorf 'well-balanced?
                                 "not a proper binary tree: ~s"
                                 v)]))])
      (car (visit init_v)))))

(test-well-balanced? well-balanced?)

;; First we added the predicate
(define is-char?
  char?)

;; Then we added the accessor
(define char_0
  (lambda (e)
    e))

;; We then added these to predefined-variables and predefined-values
;; along with the string-ref procedure.

;; Then we extended the interpreter with the case for characters
;;   [(is-char? e)
;;    (char_0 e)]

;; We tested the extension with the following:
;;   (interpret '#\s)                     ;; result was #\s
;;   (interpret '(string-ref "hello" 2))  ;; result was #\l

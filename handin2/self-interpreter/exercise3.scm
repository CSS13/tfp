;; First iteration
(define factorial
  (lambda (n)
    (if (= n 0)
      1
      (* n (factorial (1- n))))))

;; But we got in to the problem of not being able to define factorial for the
;; interpreters environment, e.g the following will not work since factorial2
;; is unknown to the interpreter:
;;   (interpret '(factorial 5))

;; Inlining the procedure does not help either:
;;   (interpret '(lambda (n)
;;                 (if (= n 0)
;;                   1
;;                   (* n (factorial (1- n))))))

;; Second iteration
;; To fix this problem of self referencing we of course need letrec:
(define factorial2
  (lambda (init_n)
    (letrec ([fac (lambda (n)
              (if (= n 0)
                1
                (* n (fac (1- n)))))])
     (fac init_n))))

;; Thus once we had extended the interpreter with {=, *, 1-} we were able to
;; evaluate the factorial of 5 as such:
;;   (interpret '(lambda (init_n)
;;                 (letrec ([fac (lambda (n)
;;                           (if (= n 0)
;;                             1
;;                             (* n (fac (1- n)))))])
;;                  (fac init_n))))

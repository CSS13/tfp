;;; code-generation.scm
;;; TFP 2015-2016, Q3
;;; Olivier Danvy <danvy@cs.au.dk>
;;; Version of 03 Mar 2016

;;; Accompanying material for the lecture note at
;;;   http://users-cs.au.dk/danvy/TFP/Lecture-notes/code-generation.html

;;;;;;;;;;;;;;;;;;;;

;;; Petite Chez Scheme paraphernalia:

(print-gensym #f)

(define reset-gensym-count
  (lambda ()
    (gensym-count 0)))

;;;;;;;;;;

(define default-stub
  "x")

(define make-variable
  (lambda (stub)
    (cond
      [(symbol? stub)
       stub]
      [(string? stub)
       (let ([x (gensym)])
         (begin
           (parameterize ([gensym-prefix stub])
             (symbol->string x))
           x))]
      [else
       (errorf 'make-variable
               "neither a string, nor a symbol: ~s"
               stub)])))

;;;;;;;;;;

(define make-lambda-abstraction-1_v0
  (lambda (p1 . rest)
    (let ([stub (if (null? rest)
                    default-stub
                    (let ([stub (car rest)])
                      (if (string? stub)
                          (if (null? (cdr rest))
                              stub
                              (errorf 'make-lambda-abstraction-1
                                      "too many actual parameters: ~s"
                                      rest))
                          (errorf 'make-lambda-abstraction-1
                                  "not a string: ~s"
                                  stub))))])
      (let* ([x1 (make-variable stub)])
        `(lambda (,x1)
           ,(p1 x1))))))

(define make-lambda-abstraction-1
  (lambda (p1 . rest)
    (let ([stub1 (case (length rest)
                   [(0)
                    default-stub]
                   [(1)
                    (let ([stub1 (list-ref rest 0)])
                      (if (or (string? stub1)
                              (symbol? stub1))
                          stub1
                          (errorf 'make-lambda-abstraction-1
                                  "not a string, nor a symbol: ~s"
                                  stub1)))]
                   [else
                    (errorf 'make-lambda-abstraction-1
                            "too many actual parameters: ~s"
                            rest)])])
      (let* ([x1 (make-variable stub1)])
        `(lambda (,x1)
           ,(p1 x1))))))

;;; > (make-lambda-abstraction-1 (lambda (x) x) 'xx)
;;; (lambda (xx) xx)
;;; > (make-lambda-abstraction-1 (lambda (x) x) "xx")
;;; (lambda (xx48) xx48)
;;; > (make-lambda-abstraction-1 (lambda (x) x))
;;; (lambda (x49) x49)
;;; > 

(define make-lambda-abstraction-2_v0
  (lambda (p2 . rest)
    (let-values ([(stub1 stub2)
                  (if (null? rest)
                      (values default-stub default-stub)
                      (let ([stub1 (car rest)])
                        (if (string? stub1)
                            (if (null? (cdr rest))
                                (values stub1 default-stub)
                                (let* ([rest2 (cdr rest)]
                                       [stub2 (car rest2)])
                                  (if (string? stub2)
                                      (if (null? (cdr rest2))
                                          (values stub1 stub2)
                                          (errorf 'make-lambda-abstraction-2
                                                  "too many actual parameters: ~s"
                                                  rest))
                                      (errorf 'make-lambda-abstraction-2
                                              "not a string: ~s"
                                              stub2))))
                            (errorf 'make-lambda-abstraction-2
                                    "not a string: ~s"
                                    stub1))))])
      (let* ([x1 (make-variable stub1)]
             [x2 (make-variable stub2)])
        `(lambda (,x1 ,x2)
           ,(p2 x1 x2))))))

(define make-lambda-abstraction-2
  (lambda (p2 . rest)
    (let-values ([(stub1 stub2)
                  (case (length rest)
                    [(0)
                     (values default-stub default-stub)]
                    [(1)
                     (let ([stub1 (list-ref rest 0)])
                       (if (or (string? stub1)
                               (symbol? stub1))
                           (values stub1 default-stub)
                           (errorf 'make-lambda-abstraction-2
                                   "not a string: ~s"
                                   stub1)))]
                    [(2)
                     (let ([stub1 (list-ref rest 0)]
                           [stub2 (list-ref rest 1)])
                       (if (or (string? stub1)
                               (symbol? stub2))
                           (if (or (string? stub2)
                                   (symbol? stub2))
                               (values stub1 stub2)
                               (errorf 'make-lambda-abstraction-2
                                       "not a string: ~s"
                                       stub2))
                           (errorf 'make-lambda-abstraction-2
                                   "not a string: ~s"
                                   stub1)))]
                    [else
                     (errorf 'make-lambda-abstraction-2
                             "too many actual parameters: ~s"
                             rest)])])
      (let* ([x1 (make-variable stub1)]
             [x2 (make-variable stub2)])
        `(lambda (,x1 ,x2)
           ,(p2 x1 x2))))))

(define make-lambda-abstraction-3
  (lambda (p3 . rest)
    (let-values ([(stub1 stub2 stub3)
                  (case (length rest)
                    [(0)
                     (values default-stub default-stub)]
                    [(1)
                     (let ([stub1 (list-ref rest 0)])
                       (if (or (string? stub1)
                               (symbol? stub1))
                           (values stub1 default-stub default-stub)
                           (errorf 'make-lambda-abstraction-3
                                   "not a string: ~s"
                                   stub1)))]
                    [(2)
                     (let ([stub1 (list-ref rest 0)]
                           [stub2 (list-ref rest 1)])
                       (if (or (string? stub1)
                               (symbol? stub1))
                           (if (or (string? stub2)
                                   (symbol? stub2))
                               (values stub1 stub2 default-stub)
                               (errorf 'make-lambda-abstraction-3
                                       "not a string: ~s"
                                       stub2))
                           (errorf 'make-lambda-abstraction-3
                                   "not a string: ~s"
                                   stub1)))]
                    [(3)
                     (let ([stub1 (list-ref rest 0)]
                           [stub2 (list-ref rest 1)]
                           [stub3 (list-ref rest 2)])
                       (if (or (string? stub1)
                               (symbol? stub1))
                           (if (or (string? stub2)
                                   (symbol? stub2))
                               (if (or (string? stub3)
                                       (symbol? stub3))
                                   (values stub1 stub2 stub3)
                                   (errorf 'make-lambda-abstraction-3
                                           "not a string: ~s"
                                           stub3))
                               (errorf 'make-lambda-abstraction-3
                                       "not a string: ~s"
                                       stub2))
                           (errorf 'make-lambda-abstraction-3
                                   "not a string: ~s"
                                   stub1)))]
                    [else
                     (errorf 'make-lambda-abstraction-3
                             "too many actual parameters: ~s"
                             rest)])])
      (let* ([x1 (make-variable stub1)]
             [x2 (make-variable stub2)]
             [x3 (make-variable stub3)])
        `(lambda (,x1 ,x2 ,x3)
           ,(p2 x1 x2 x3))))))

;;;;;;;;;;

(define make-let-1
  (lambda (stub1 definiens p-body)
    (let* ([x1 (make-variable stub1)])
      `(let ([,x1 ,definiens])
         ,(p-body x1)))))

(define make-let-2
  (lambda (stub1 definiens1 stub2 definiens2 p-body)
    (let* ([x1 (make-variable stub1)]
           [x2 (make-variable stub2)])
      `(let ([,x1 ,definiens1]
             [,x2 ,definiens2])
         ,(p-body x1 x2)))))

(define make-let-values-2
  (lambda (stub1 stub2 definiens p-body)
    (let* ([x1 (make-variable stub1)]
           [x2 (make-variable stub2)])
      `(let-values ([(,x1 ,x2) ,definiens])
         ,(p-body x1 x2)))))

(define make-let-3
  (lambda (stub1 definiens1 stub2 definiens2 stub3 definiens3 p-body)
    (let* ([x1 (make-variable stub1)]
           [x2 (make-variable stub2)]
           [x3 (make-variable stub3)])
      `(let ([,x1 ,definiens1]
             [,x2 ,definiens2]
             [,x3 ,definiens3])
         ,(p-body x1 x2 x3)))))

(define make-let-values-3
  (lambda (stub1 stub2 stub3 definiens p-body)
    (let* ([x1 (make-variable stub1)]
           [x2 (make-variable stub2)]
           [x3 (make-variable stub3)])
      `(let-values ([(,x1 ,x2, x3) ,definiens])
         ,(p-body x1 x2 x3)))))

;;;;;;;;;;

(define make-letrec-1-1
  (lambda (p-definiens stub0 stub1 p-body)
    (let* ([p (make-variable stub0)]
           [x1 (make-variable stub1)])
      `(letrec ([,p (lambda (,x1)
                      ,(p-definiens p x1))])
         ,(p-body p)))))

(define make-letrec-1-2
  (lambda (p-definiens stub0 stub1 stub2 p-body)
    (let* ([p (make-variable stub0)]
           [x1 (make-variable stub1)]
           [x2 (make-variable stub2)])
      `(letrec ([,p (lambda (,x1 ,x2)
                      ,(p-definiens p x1 x2))])
         ,(p-body p)))))

;;;;;;;;;;

(define fold-right_nat
  (lambda (zero-case succ-case error-case init-case)
    (lambda (v_init)
      (letrec ([visit (lambda (x)
                        (if (= x 0)
                            zero-case
                            (succ-case (visit (- x 1)))))])
        (if (and (integer? v_init)
                 (not (negative? v_init)))
            (init-case (visit v_init))
            (error-case v_init))))))

(define fold-right_nat-gen
  (lambda (zero-case succ-case error-case init-case)
    (begin
      (reset-gensym-count)
      (make-lambda-abstraction-1
       (lambda (v_init)
         (make-letrec-1-1
          (lambda (visit x)
            `(if (= ,x 0)
                 ,zero-case
                 (,succ-case (,visit (- ,x 1)))))
          "visit"
          "x"
          (lambda (visit)
            `(if (and (integer? ,v_init)
                      (not (negative? ,v_init)))
                 (,init-case (,visit ,v_init))
                 (,error-case ,v_init)))))
       "v_init"))))

;; > (fold-right_nat-gen 3
;;                      (make-lambda-abstraction-1 (lambda (v)
;;                                                   `(+ 1 ,v)) "v")
;;                      (make-lambda-abstraction-1 (lambda (v)
;;                                                   `(errorf 'plus-3
;;                                                            "not a positive integer: ~s"
;;                                                            ,v))
;;                                                 "v")
;;                      (make-lambda-abstraction-1 (lambda (v) v)
;;                                                 "v"))
;;
;; (lambda (v_init0)
;;   (letrec ([visit1 (lambda (x2)
;;                      (if (= x2 0)
;;                          3
;;                          ((lambda (v5) (+ 1 v5)) (visit1 (- x2 1)))))])
;;     (if (and (integer? v_init0) (not (negative? v_init0)))
;;         ((lambda (v3) v3) (visit1 v_init0))
;;         ((lambda (v4) (errorf 'plus-3 "not a positive integer: ~s" v4)) v_init0))))

;;;;;;;;;;

(define fold-left_nat
  (lambda (zero-case succ-case error-case init-case)
    (lambda (v_init)
      (letrec ([visit (lambda (x a)
                        (if (= x 0)
                            a
                            (visit (- x 1) (succ-case a))))])
        (if (and (integer? v_init)
                 (not (negative? v_init)))
            (init-case (visit v_init zero-case))
            (error-case v_init))))))

(define fold-left_nat-gen
  (lambda (zero-case succ-case error-case init-case)
    (begin
      (reset-gensym-count)
      (make-lambda-abstraction-1
       (lambda (v_init)
         (make-letrec-1-2
          (lambda (visit x a)
            `(if (= ,x 0)
                 ,a
                 (,visit (- ,x 1) (,succ-case ,a))))
          "visit"
          "x"
          "a"
          (lambda (visit)
            `(if (and (integer? ,v_init)
                      (not (negative? ,v_init)))
                 (,init-case (,visit ,v_init ,zero-case))
                 (,error-case ,v_init)))))
       "v_init"))))

;; > (fold-left_nat-gen 3
;;                      (make-lambda-abstraction-1 (lambda (v)
;;                                                   `(+ 1 ,v)) "v")
;;                      (make-lambda-abstraction-1 (lambda (v)
;;                                                   `(errorf 'plus-3
;;                                                            "not a positive integer: ~s"
;;                                                            ,v))
;;                                                 "v")
;;                      (make-lambda-abstraction-1 (lambda (v) v)
;;                                                 "v"))
;; 
;; (lambda (v_init0)
;;   (letrec ([visit1 (lambda (x2 a3)
;;                      (if (= x2 0)
;;                          a3
;;                          (visit1 (- x2 1) ((lambda (v6) (+ 1 v6)) a3))))])
;;     (if (and (integer? v_init0) (not (negative? v_init0)))
;;         ((lambda (v4) v4) (visit1 v_init0 3))
;;         ((lambda (v5) (errorf 'plus-3 "not a positive integer: ~s" v5)) v_init0))))

;;;;;;;;;;

(define fold-right_proper-list
  (lambda (nil-case cons-case error-case init-case)
    (lambda (v_init)
      (letrec ([visit (lambda (v)
                        (cond
                          [(null? v)
                           nil-case]
                          [(pair? v)
                           (cons-case (car v)
                                      (visit (cdr v)))]
                          [else
                           (error-case v)]))])
        (init-case (visit v_init))))))

(define fold-right_proper-list-gen
  (lambda (nil-case cons-case error-case init-case)
    (begin
      (reset-gensym-count)
      (make-lambda-abstraction-1
       (lambda (v_init)
         (make-letrec-1-1
          (lambda (visit v)
            `(cond
               [(null? ,v)
                ,nil-case]
               [(pair? ,v)
                (,cons-case (car ,v)
                            (,visit (cdr ,v)))]
               [else
                (,error-case ,v)]))
          "visit"
          "v"
          (lambda (visit)
            `(,init-case (,visit ,v_init)))))
       "v_init"))))

;; > (fold-right_proper-list-gen ''()
;;                               'cons
;;                               (make-lambda-abstraction-1 (lambda (v)
;;                                                            `(errorf 'proper-list-identity
;;                                                                     "not a proper list: ~s"
;;                                                                     ,v))
;;                                                          "v")
;;                               (make-lambda-abstraction-1 (lambda (v) v)
;;                                                          "v"))
;;
;; (lambda (v_init0)
;;   (letrec ([visit1 (lambda (v2)
;;                      (cond
;;                        [(null? v2) '()]
;;                        [(pair? v2) (cons (car v2)
;;                                          (visit1 (cdr v2)))]
;;                        [else
;;                         ((lambda (v5) (errorf 'proper-list-identity
;;                                               "not a proper list: ~s"
;;                                               v5))
;;                           v2)]))])
;;     ((lambda (v4) v4) (visit1 v_init0))))

;;;;;;;;;;

(define fold-left_proper-list
  (lambda (nil-case cons-case error-case init-case)
    (lambda (v_init)
      (letrec ([visit (lambda (v a)
                        (cond
                          [(null? v)
                           a]
                          [(pair? v)
                           (visit (cdr v)
                                  (cons-case (car v) a))]
                          [else
                           (error-case v)]))])
        (init-case (visit v_init nil-case))))))

(define fold-left_proper-list-gen
  (lambda (nil-case cons-case error-case init-case)
    (begin
      (reset-gensym-count)
      (make-lambda-abstraction-1
       (lambda (v_init)
         (make-letrec-1-2
          (lambda (visit v a)
            `(cond
               [(null? ,v)
                ,a]
               [(pair? ,v)
                (,visit (cdr ,v)
                        (,cons-case (car ,v) ,a))]
               [else
                (,error-case ,v)]))
          "visit"
          "v"
          "a"
          (lambda (visit)
            `(,init-case (,visit ,v_init ,nil-case)))))
       "v_init"))))

;; > (fold-left_proper-list-gen ''()
;;                              'cons
;;                              (make-lambda-abstraction-1 (lambda (v)
;;                                                           `(errorf 'proper-list-reverse
;;                                                                    "not a proper list: ~s"
;;                                                                    ,v))
;;                                                         "v")
;;                              (make-lambda-abstraction-1 (lambda (v) v)
;;                                                         "v"))
;;
;; (lambda (v_init0)
;;   (letrec ([visit1 (lambda (v2 a3)
;;                      (cond
;;                        [(null? v2) a3]
;;                        [(pair? v2) (visit1 (cdr v2)
;;                                            (cons (car v2) a3))]
;;                        [else
;;                         ((lambda (v4) (errorf 'proper-list-reverse
;;                                               "not a proper list: ~s"
;;                                               v4))
;;                           v2)]))])
;;     ((lambda (v3) v3) (visit1 v_init0 '()))))

;;;;;;;;;;

;;; <binary-tree> ::= () | <number> | (<binary-tree> . <binary-tree>)

(define binary-tree-occurs?_v0
  (lambda (x bt_init)
    (letrec ([visit (lambda (bt)
                      (cond
                        [(null? bt)
                         #f]
                        [(number? bt)
                         (= x bt)]
                        [(pair? bt)
                         (or (visit (car bt))
                             (visit (cdr bt)))]
                        [else
                         (errorf 'binary-tree-occurs?_v0
                                 "not a binary tree: ~s"
                                 bt)]))])
      (visit bt_init))))

(define binary-tree-occurs?_v0-gen
  (lambda (x)
    (begin
      (reset-gensym-count)
      (make-lambda-abstraction-1
       (lambda (bt_init)
         (make-letrec-1-1
          (lambda (visit bt)
            `(cond
               [(null? ,bt)
                #f]
               [(number? ,bt)
                (= ,x ,bt)]
               [(pair? ,bt)
                (or (,visit (car ,bt))
                    (,visit (cdr ,bt)))]
               [else
                (errorf 'binary-tree-occurs?_v0
                        "not a binary tree: ~s"
                        ,bt)]))
          'visit
          'bt
          (lambda (visit)
            `(,visit ,bt_init))))
       'bt_init))))

;; > (binary-tree-occurs?_v0-gen 3)
;;
;; (lambda (bt_init)
;;   (letrec ([visit (lambda (bt)
;;                     (cond
;;                       [(null? bt) #f]
;;                       [(number? bt) (= 3 bt)]
;;                       [(pair? bt) (or (visit (car bt)) (visit (cdr bt)))]
;;                       [else
;;                        (errorf 'binary-tree-occurs?_v0
;;                          "not a binary tree: ~s"
;;                          bt)]))])
;;     (visit bt_init)))

;;;;;;;;;;

;;; utilities:

(define proper-list-of-given-length?
  (lambda (v n)
    (or (and (null? v)
             (= n 0))
        (and (pair? v)
             (> n 0)
             (proper-list-of-given-length? (cdr v)
                                           (1- n))))))

;;;;;;;;;;

;;; implementation of the BNF for arithmetic expressions:

;;;;;

;;; the constructors:

(define make-literal
  (lambda (n)
    (list 'literal n)))

(define make-plus
  (lambda (e1 e2)
    (list 'plus e1 e2)))

(define make-times
  (lambda (e1 e2)
    (list 'times e1 e2)))

;;; the predicates:

(define is-literal?
  (lambda (v)
    (and (pair? v)
         (equal? (car v) 'literal)
         (proper-list-of-given-length? (cdr v) 1))))

(define is-plus?
  (lambda (v)
    (and (pair? v)
         (equal? (car v) 'plus)
         (proper-list-of-given-length? (cdr v) 2))))

(define is-times?
  (lambda (v)
    (and (pair? v)
         (equal? (car v) 'times)
         (proper-list-of-given-length? (cdr v) 2))))

;;; the accessors:

(define literal-1
  (lambda (v)
    (list-ref v 1)))

(define plus-1
  (lambda (v)
    (list-ref v 1)))

(define plus-2
  (lambda (v)
    (list-ref v 2)))

(define times-1
  (lambda (v)
    (list-ref v 1)))

(define times-2
  (lambda (v)
    (list-ref v 2)))

;;;;;;;;;;

;;; parser:

(define parse-arithmetic-expression
  (lambda (v)
    (cond
      [(number? v)
       (make-literal v)]
      [(proper-list-of-given-length? v 3)
       (case (list-ref v 0)
         [(+)
          (make-plus (parse-arithmetic-expression (list-ref v 1))
                     (parse-arithmetic-expression (list-ref v 2)))]
         [(*)
          (make-times (parse-arithmetic-expression (list-ref v 1))
                      (parse-arithmetic-expression (list-ref v 2)))]
         [else
          (errorf 'parse-arithmetic-expression
                  "unrecognized operator: ~s"
                  v)])]
      [else
       (errorf 'parse-arithmetic-expression
               "unrecognized concrete syntax: ~s"
               v)])))

;;;;;;;;;;

(define sequentialize-parsed-arithmetic-expression_v0
  (lambda (e_init)
    (begin
      (reset-gensym-count)
      (letrec ([visit
                (lambda (e k)
                  (cond
                    [(is-literal? e)
                     (k e)]
                    [(is-plus? e)
                     (visit (plus-2 e)
                            (lambda (e2)
                              (visit (plus-1 e)
                                     (lambda (e1)
                                       (make-let-1
                                        "x"
                                        (make-plus e1 e2)
                                        (lambda (x)
                                          (k x)))))))]
                    [(is-times? e)
                     (visit (times-2 e)
                            (lambda (e2)
                              (visit (times-1 e)
                                     (lambda (e1)
                                       (make-let-1
                                        "x"
                                        (make-times e1 e2)
                                        (lambda (x)
                                          (k x)))))))]
                    [else
                     (errorf 'sequentialize-parsed-arithmetic-expression_v0
                             "unrecognized expression: ~s"
                             e)]))])
        (visit e_init (lambda (x) x))))))

;; > (sequentialize-parsed-arithmetic-expression_v0
;;    (parse-arithmetic-expression '(+ (* 1 2) (+ (* 3 4) 5))))
;; 
;; (let ([x0 (times (literal 3) (literal 4))])
;;   (let ([x1 (plus x0 (literal 5))])
;;     (let ([x2 (times (literal 1) (literal 2))])
;;       (let ([x3 (plus x2 x1)]) x3))))

(define is-let*?
  (lambda (e)
    (and (proper-list-of-given-length? e 3)
         (equal? (car e) 'let*))))

(define let*-1
  (lambda (e)
    (list-ref e 1)))

(define let*-2
  (lambda (e)
    (list-ref e 2)))

(define make-let*
  (lambda (stub1 definiens p-body)
    (let* ([x1 (make-variable stub1)]
           [body (p-body x1)])
      (cond
        [(equal? body x1)
         definiens]
        [(is-let*? body)
         `(let* ([,x1 ,definiens] ,@(let*-1 body))
            ,(let*-2 body))]
        [else
         `(let* ([,x1 ,definiens])
            ,body)]))))

(define sequentialize-parsed-arithmetic-expression_v1
  (lambda (e_init)
    (begin
      (reset-gensym-count)
      (letrec ([visit
                (lambda (e k)
                  (cond
                    [(is-literal? e)
                     (k e)]
                    [(is-plus? e)
                     (visit (plus-2 e)
                            (lambda (e2)
                              (visit (plus-1 e)
                                     (lambda (e1)
                                       (make-let*
                                        "x"
                                        (make-plus e1 e2)
                                        (lambda (x)
                                          (k x)))))))]
                    [(is-times? e)
                     (visit (times-2 e)
                            (lambda (e2)
                              (visit (times-1 e)
                                     (lambda (e1)
                                       (make-let*
                                        "x"
                                        (make-times e1 e2)
                                        (lambda (x)
                                          (k x)))))))]
                    [else
                     (errorf 'sequentialize-parsed-arithmetic-expression_v0
                             "unrecognized expression: ~s"
                             e)]))])
        (visit e_init (lambda (x) x))))))

;; > (sequentialize-parsed-arithmetic-expression_v1
;;    (parse-arithmetic-expression '(+ (* 1 2) (+ (* 3 4) 5))))
;; 
;; (let* ([x0 (times (literal 3) (literal 4))]
;;        [x1 (plus x0 (literal 5))]
;;        [x2 (times (literal 1) (literal 2))])
;;   (plus x2 x1))

;;;;;;;;;;

;;; <pat> ::= (patnum <number>) | (patvar <symbol>) | (patpair <pat> <pat>)

(define make-patnum
  (lambda (v)
    (list 'patnum v)))

(define is-patnum?
  (lambda (v)
    (and (proper-list-of-given-length? v 2)
         (equal? (car v) 'patnum))))

(define patnum-1
  (lambda (v)
    (list-ref v 1)))

(define make-patvar
  (lambda (v)
    (list 'patvar v)))

(define patvar-1
  (lambda (v)
    (list-ref v 1)))

(define is-patvar?
  (lambda (v)
    (and (proper-list-of-given-length? v 2)
         (equal? (car v) 'patvar))))

(define make-patpair
  (lambda (v1 v2)
    (list 'patpair v1 v2)))

(define is-patpair?
  (lambda (v)
    (and (proper-list-of-given-length? v 3)
         (equal? (car v) 'patpair))))

(define patpair-1
  (lambda (v)
    (list-ref v 1)))

(define patpair-2
  (lambda (v)
    (list-ref v 2)))

(define alist-mt
  '())

(define alist-extend
  (lambda (name denotable environment)
    (cons (cons name denotable)
          environment)))

(define alist-lookup
  (lambda (name environment found not-found)
    (letrec ([visit (lambda (e)
                      (if (null? e)
                          (not-found)
                          (let ([binding (car e)])
                            (if (equal? name (car binding))
                                (found (cdr binding))
                                (visit (cdr e))))))])
      (visit environment))))

(define try-candidate
  (lambda (name candidate expected-output . input)
    (or (equal? expected-output
                (apply candidate input))
        (begin
          (printf "~s: error for ~s~n" name input)
          #f))))

(define and-all
  (lambda bs_init
    (letrec ([visit (lambda (bs)
                      (or (null? bs)
                          (and (car bs)
                               (visit (cdr bs)))))])
      (visit bs_init))))

(define test-match-pat
  (lambda (candidate)
    (and-all (try-candidate 'match-pat
                            candidate
                            '()
                            (make-patnum 42)
                            42)
             (try-candidate 'match-pat
                            candidate
                            #f
                            (make-patnum 42)
                            43)
             (try-candidate 'match-pat
                            candidate
                            '()
                            (make-patpair (make-patnum 42)
                                          (make-patnum 43))
                            (cons 42
                                  43))
             (try-candidate 'match-pat
                            candidate
                            '((y . 43) (x . 42))
                            (make-patpair (make-patvar 'x)
                                          (make-patvar 'y))
                            (cons 42
                                  43))
             (try-candidate 'match-pat
                            candidate
                            '((x . 42))
                            (make-patpair (make-patvar 'x)
                                          (make-patvar 'x))
                            (cons 42
                                  42))
             (try-candidate 'match-pat
                            candidate
                            #f
                            (make-patpair (make-patvar 'x)
                                          (make-patvar 'x))
                            (cons 42
                                  43))
             ;;;
             )))

(define match-pat
  (lambda (p v)
    (letrec ([visit
              (lambda (p v env)
                (cond
                  [(is-patnum? p)
                   (and (= (patnum-1 p) v)
                        env)]
                  [(is-patvar? p)
                   (alist-lookup (patvar-1 p)
                                 env
                                 (lambda (n)
                                   (and (= n v)
                                        env))
                                 (lambda ()
                                   (alist-extend (patvar-1 p)
                                                 v
                                                 env)))]
                  [(is-patpair? p)
                   (and (pair? v)
                        (cond
                          [(visit (patpair-1 p)
                                  (car v)
                                  env)
                           =>
                           (lambda (env1)
                             (visit (patpair-2 p)
                                    (cdr v)
                                    env1))]
                          [else
                           #f]))]
                  [else
                   (errorf 'match-pat
                           "invalid pattern: ~s"
                           p)]))])
      (visit p v alist-mt))))

(define match-pat-cb
  (lambda (p v)
    (letrec ([visit
              (lambda (p v env k)
                (cond
                  [(is-patnum? p)
                   (and (= (patnum-1 p) v)
                        (k env))]
                  [(is-patvar? p)
                   (alist-lookup (patvar-1 p)
                                 env
                                 (lambda (n)
                                   (and (= n v)
                                        (k env)))
                                 (lambda ()
                                   (k (alist-extend (patvar-1 p)
                                                    v
                                                    env))))]
                  [(is-patpair? p)
                   (and (pair? v)
                        (visit (patpair-1 p)
                               (car v)
                               env
                               (lambda (env1)
                                 (visit (patpair-2 p)
                                        (cdr v)
                                        env1
                                        k))))]
                  [else
                   (errorf 'match-pat-cb
                           "invalid pattern: ~s"
                           p)]))])
      (visit p v alist-mt (lambda (env) env)))))

(define try-candidate-2-gen
  (lambda (name candidate expected-output input1 input2)
    (let* ([residual (candidate input1)]
           [actual-output ((eval residual) input2)])
      (or (equal? expected-output actual-output)
          (begin
            (printf "~s: error for (~s '~s)~n~s instead of ~s~n~n"
                    name residual input2
                    actual-output expected-output)
            #f)))))

(define test-match-pat-cb-gen
  (lambda (candidate)
    (and-all (try-candidate-2-gen 'match-pat
                                  candidate
                                  '()
                                  (make-patnum 42)
                                  42)
             (try-candidate-2-gen 'match-pat
                                  candidate
                                  #f
                                  (make-patnum 42)
                                  43)
             (try-candidate-2-gen 'match-pat
                                  candidate
                                  '()
                                  (make-patpair (make-patnum 42)
                                                (make-patnum 43))
                                  (cons 42
                                        43))
             (try-candidate-2-gen 'match-pat
                                  candidate
                                  '((y . 43) (x . 42))
                                  (make-patpair (make-patvar 'x)
                                                (make-patvar 'y))
                                  (cons 42
                                        43))
             (try-candidate-2-gen 'match-pat
                                  candidate
                                  '((x . 42))
                                  (make-patpair (make-patvar 'x)
                                                (make-patvar 'x))
                                  (cons 42
                                        42))
             (try-candidate-2-gen 'match-pat
                                  candidate
                                  #f
                                  (make-patpair (make-patvar 'x)
                                                (make-patvar 'x))
                                  (cons 42
                                        43))
             ;;;
             )))

(define reify-env
  (lambda (env)
    (cond
      [(null? env)
        `alist-mt]
      [(pair? env)
       (let* ([entry (car env)]
              [key (car entry)]
              [value (cdr entry)]
              [rest (cdr env)])
         `(alist-extend ',key ,value ,(reify-env rest)))]
      [else
    (errorf 'reify-env
            "not an environment: ~s"
            env)])))

(define match-pat-cb-gen
  (lambda (p)
    (make-lambda-abstraction-1
      (lambda (v)
        (letrec ([visit
                   (lambda (p v env k)
                     (cond
                       [(is-patnum? p)
                        `(and (= ,(patnum-1 p) ,v)
                             ,(k env))]
                       [(is-patvar? p)
                        (alist-lookup (patvar-1 p)
                                      env
                                      (lambda (n)
                                        `(and (= ,n ,v)
                                             ,(k env)))
                                      (lambda ()
                                        (k (alist-extend (patvar-1 p)
                                                         v
                                                         env))))]
                       [(is-patpair? p)
                        `(and (pair? ,v)
                             ,(visit (patpair-1 p)
                                    `(car ,v)
                                    env
                                    (lambda (env1)
                                      (visit (patpair-2 p)
                                             `(cdr ,v)
                                             env1
                                             k))))]
                       [else
                         (errorf 'match-pat-cb
                                 "invalid pattern: ~s"
                                 p)]))])
          (visit p v alist-mt reify-env))))))

;;;;;;;;;;;;;;;;;;;;

;;; end of code-generation.scm

"code-generation.scm"

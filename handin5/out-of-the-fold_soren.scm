;;; out-of-the-fold.scm
;;; TFP 2015-2016, Q3
;;; Olivier Danvy <danvy@cs.au.dk>
;;; Version of 26 Feb 2016

;;; Accompanying material for the lecture note at
;;;   http://users-cs.au.dk/danvy/TFP/Lecture-notes/out-of-the-fold.html

;;;;;;;;;;;;;;;;;;;;

(define try-candidate
  (lambda (name candidate expected-output . input)
    (or (equal? expected-output
                (apply candidate input))
        (begin
          (printf "~s: error for ~s~n" name input)
          #f))))

(define and-all
  (lambda bs_init
    (letrec ([visit (lambda (bs)
                      (or (null? bs)
                          (and (car bs)
                               (visit (cdr bs)))))])
      (visit bs_init))))

;;;;;;;;;;;;;;;;;;;;

(define fold-right_nat
  (lambda (zero-case succ-case . rest)
    (let ([error-case (cond
                        [(null? rest)
                         (lambda (v)
                           (errorf 'fold-right_nat
                                   "not a non-negative integer: ~s"
                                   v))]
                        [(null? (cdr rest))
                         (let ([the-error-case (car rest)])
                           (if (procedure? the-error-case)
                               the-error-case
                               (lambda (v)
                                 (errorf the-error-case
                                         "not a non-negative integer: ~s"
                                         v))))]
                        [else
                         (errorf 'fold-right_nat
                                 "improper actual parameters: ~s"
                                 rest)])])
      (lambda (v_init)
        (letrec ([visit
                  (lambda (i)
                    (if (= i 0)
                        zero-case
                        (succ-case (visit (1- i)))))])
          (if (and (integer? v_init)
                   (not (negative? v_init)))
              (visit v_init)
              (error-case v_init)))))))

;;;;;;;;;;;;;;;;;;;;

(define test-plus
  (lambda (name candidate)
    (and-all (try-candidate name candidate 5 0 5)
             (try-candidate name candidate 5 5 0)
             (try-candidate name candidate 6 5 1)
             (try-candidate name candidate 6 1 5)
             ;;;
             )))

;;; Generic definition of addition:

(define plus_v0
  (lambda (x y)
    ((fold-right_nat y 1+ 'plus_v0) x)))

(unless (test-plus 'plus_v0 plus_v0)
  (printf "(test-plus 'plus_v0 plus_v0) failed~n"))

;;; Renaming of plus_v0 into plus_v1 and
;;; inlining of fold-right_nat:

(define plus_v1a
  (lambda (x y)
    (((lambda (zero-case succ-case . rest)
        (let ([error-case (cond
                            [(null? rest)
                             (lambda (v)
                               (errorf 'fold-right_nat
                                       "not a non-negative integer: ~s"
                                       v))]
                            [(null? (cdr rest))
                             (let ([the-error-case (car rest)])
                               (if (procedure? the-error-case)
                                   the-error-case
                                   (lambda (v)
                                     (errorf the-error-case
                                             "not a non-negative integer: ~s"
                                             v))))]
                            [else
                             (errorf 'fold-right_nat
                                     "improper actual parameters: ~s"
                                     rest)])])
          (lambda (v_init)
            (letrec ([visit
                      (lambda (i)
                        (if (= i 0)
                            zero-case
                            (succ-case (visit (1- i)))))])
              (if (and (integer? v_init)
                       (not (negative? v_init)))
                  (visit v_init)
                  (error-case v_init))))))
      y
      1+
      'plus_v0)
     x)))

(unless (test-plus 'plus_v1a plus_v1a)
  (printf "(test-plus 'plus_v1a plus_v1a) failed~n"))

;;; Application of (lambda (zero-case succ-case . rest) ...)
;;; to y, 1+, and 'plus_v0:

(define plus_v1b
  (lambda (x y)
    ((let ([zero-case y]
           [succ-case 1+]
           [rest (list 'plus_v1)])
       (let ([error-case (cond
                           [(null? rest)
                            (lambda (v)
                              (errorf 'fold-right_nat
                                      "not a non-negative integer: ~s"
                                      v))]
                           [(null? (cdr rest))
                            (let ([the-error-case (car rest)])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a non-negative integer: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-right_nat
                                    "improper actual parameters: ~s"
                                    rest)])])
         (lambda (v_init)
           (letrec ([visit
                     (lambda (i)
                       (if (= i 0)
                           zero-case
                           (succ-case (visit (1- i)))))])
             (if (and (integer? v_init)
                      (not (negative? v_init)))
                 (visit v_init)
                 (error-case v_init))))))
     x)))

(unless (test-plus 'plus_v1b plus_v1b)
  (printf "(test-plus 'plus_v1b plus_v1b) failed~n"))

;;; Computation of (list 'plus_v1):

(define plus_v1c
  (lambda (x y)
    ((let ([zero-case y]
           [succ-case 1+]
           [rest '(plus_v1)])
       (let ([error-case (cond
                           [(null? rest)
                            (lambda (v)
                              (errorf 'fold-right_nat
                                      "not a non-negative integer: ~s"
                                      v))]
                           [(null? (cdr rest))
                            (let ([the-error-case (car rest)])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a non-negative integer: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-right_nat
                                    "improper actual parameters: ~s"
                                    rest)])])
         (lambda (v_init)
           (letrec ([visit
                     (lambda (i)
                       (if (= i 0)
                           zero-case
                           (succ-case (visit (1- i)))))])
             (if (and (integer? v_init)
                      (not (negative? v_init)))
                 (visit v_init)
                 (error-case v_init))))))
     x)))

(unless (test-plus 'plus_v1c plus_v1c)
  (printf "(test-plus 'plus_v1c plus_v1c) failed~n"))

;;; Unfolding of the let-expression declaring zero-case, succ-case, and rest:

(define plus_v1d
  (lambda (x y)
    ((let ([error-case (cond
                         [(null? '(plus_v1))
                          (lambda (v)
                            (errorf 'fold-right_nat
                                    "not a non-negative integer: ~s"
                                    v))]
                         [(null? (cdr '(plus_v1)))
                          (let ([the-error-case (car '(plus_v1))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a non-negative integer: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-right_nat
                                  "improper actual parameters: ~s"
                                  '(plus_v1))])])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         y
                         (1+ (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     x)))

(unless (test-plus 'plus_v1d plus_v1d)
  (printf "(test-plus 'plus_v1d plus_v1d) failed~n"))

;;; Computation of (null? '(plus_v1)):

(define plus_v1e
  (lambda (x y)
    ((let ([error-case (cond
                         [#f
                          (lambda (v)
                            (errorf 'fold-right_nat
                                    "not a non-negative integer: ~s"
                                    v))]
                         [(null? (cdr '(plus_v1)))
                          (let ([the-error-case (car '(plus_v1))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a non-negative integer: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-right_nat
                                  "improper actual parameters: ~s"
                                  '(plus_v1))])])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         y
                         (1+ (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     x)))

(unless (test-plus 'plus_v1e plus_v1e)
  (printf "(test-plus 'plus_v1e plus_v1e) failed~n"))

;;; Elimination of the first cond-clause:

(define plus_v1f
  (lambda (x y)
    ((let ([error-case (cond
                         [(null? (cdr '(plus_v1)))
                          (let ([the-error-case (car '(plus_v1))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a non-negative integer: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-right_nat
                                  "improper actual parameters: ~s"
                                  '(plus_v1))])])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         y
                         (1+ (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     x)))

(unless (test-plus 'plus_v1f plus_v1f)
  (printf "(test-plus 'plus_v1f plus_v1f) failed~n"))

;;; Computation of (cdr '(plus_v1)):

(define plus_v1g
  (lambda (x y)
    ((let ([error-case (cond
                         [(null? '())
                          (let ([the-error-case (car '(plus_v1))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a non-negative integer: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-right_nat
                                  "improper actual parameters: ~s"
                                  '(plus_v1))])])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         y
                         (1+ (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     x)))

(unless (test-plus 'plus_v1g plus_v1g)
  (printf "(test-plus 'plus_v1g plus_v1g) failed~n"))

;;; Computation of (null? '()):

(define plus_v1h
  (lambda (x y)
    ((let ([error-case (cond
                         [#t
                          (let ([the-error-case (car '(plus_v1))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a non-negative integer: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-right_nat
                                  "improper actual parameters: ~s"
                                  '(plus_v1))])])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         y
                         (1+ (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     x)))

(unless (test-plus 'plus_v1h plus_v1h)
  (printf "(test-plus 'plus_v1h plus_v1h) failed~n"))

;;; Selection of the first cond-clause:

(define plus_v1i
  (lambda (x y)
    ((let ([error-case (let ([the-error-case (car '(plus_v1))])
                         (if (procedure? the-error-case)
                             the-error-case
                             (lambda (v)
                               (errorf the-error-case
                                       "not a non-negative integer: ~s"
                                       v))))])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         y
                         (1+ (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     x)))

(unless (test-plus 'plus_v1i plus_v1i)
  (printf "(test-plus 'plus_v1i plus_v1i) failed~n"))

;;; Computation of (car '(plus_v1)):

(define plus_v1j
  (lambda (x y)
    ((let ([error-case (let ([the-error-case 'plus_v1])
                         (if (procedure? the-error-case)
                             the-error-case
                             (lambda (v)
                               (errorf the-error-case
                                       "not a non-negative integer: ~s"
                                       v))))])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         y
                         (1+ (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     x)))

(unless (test-plus 'plus_v1j plus_v1j)
  (printf "(test-plus 'plus_v1j plus_v1j) failed~n"))

;;; Unfolding of the let-expression declaring the-error-case:

(define plus_v1k
  (lambda (x y)
    ((let ([error-case (if (procedure? 'plus_v1)
                           'plus_v1
                           (lambda (v)
                             (errorf 'plus_v1
                                     "not a non-negative integer: ~s"
                                     v)))])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         y
                         (1+ (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     x)))

(unless (test-plus 'plus_v1k plus_v1k)
  (printf "(test-plus 'plus_v1k plus_v1k) failed~n"))

;;; Computation of (procedure? 'plus_v1):

(define plus_v1l
  (lambda (x y)
    ((let ([error-case (if #f
                           'plus_v1
                           (lambda (v)
                             (errorf 'plus_v1
                                     "not a non-negative integer: ~s"
                                     v)))])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         y
                         (1+ (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     x)))

(unless (test-plus 'plus_v1l plus_v1l)
  (printf "(test-plus 'plus_v1l plus_v1l) failed~n"))

;;; Selection of the alternative:

(define plus_v1m
  (lambda (x y)
    ((let ([error-case (lambda (v)
                         (errorf 'plus_v1
                                 "not a non-negative integer: ~s"
                                 v))])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         y
                         (1+ (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     x)))

(unless (test-plus 'plus_v1m plus_v1m)
  (printf "(test-plus 'plus_v1m plus_v1m) failed~n"))

;;; Unfolding of the let-expression declaring error-case:

(define plus_v1n
  (lambda (x y)
    ((lambda (v_init)
       (letrec ([visit
                 (lambda (i)
                   (if (= i 0)
                       y
                       (1+ (visit (1- i)))))])
         (if (and (integer? v_init)
                  (not (negative? v_init)))
             (visit v_init)
             ((lambda (v)
                (errorf 'plus_v1
                        "not a non-negative integer: ~s"
                        v)) v_init))))
     x)))

(unless (test-plus 'plus_v1n plus_v1n)
  (printf "(test-plus 'plus_v1n plus_v1n) failed~n"))

;;; Application of (lambda (v_init) ...) to x:

(define plus_v1o
  (lambda (x y)
    (letrec ([visit
              (lambda (i)
                (if (= i 0)
                    y
                    (1+ (visit (1- i)))))])
      (if (and (integer? x)
               (not (negative? x)))
          (visit x)
          ((lambda (v)
             (errorf 'plus_v1
                     "not a non-negative integer: ~s"
                     v)) x)))))

(unless (test-plus 'plus_v1o plus_v1o)
  (printf "(test-plus 'plus_v1o plus_v1o) failed~n"))

;;; Application of (lambda (v) ...) to x:

(define plus_v1
  (lambda (x y)
    (letrec ([visit
              (lambda (i)
                (if (= i 0)
                    y
                    (1+ (visit (1- i)))))])
      (if (and (integer? x)
               (not (negative? x)))
          (visit x)
          (errorf 'plus_v1
                  "not a non-negative integer: ~s"
                  x)))))

(unless (test-plus 'plus_v1 plus_v1)
  (printf "(test-plus 'plus_v1 plus_v1) failed~n"))

;;;;;;;;;;;;;;;;;;;;

(define test-power
  (lambda (name candidate)
    (and-all (try-candidate name candidate 1 2 0)
             (try-candidate name candidate 2 2 1)
             (try-candidate name candidate 1024 2 10)
             ;;;
             )))

;;; Generic definition of the power procedure:

(define power_v0
  (lambda (x n)
    ((fold-right_nat 1 (lambda (c) (* x c)) 'power_v0) n)))

(unless (test-power 'power_v0 power_v0)
  (printf "(test-power 'power_v0 power_v0) failed~n"))

;;; Renaming of power_v0 into power_v1 and
;;; inlining of fold-right_nat:

(define power_v1a
  (lambda (x n)
    (((lambda (zero-case succ-case . rest)
        (let ([error-case (cond
                            [(null? rest)
                             (lambda (v)
                               (errorf 'fold-right_nat
                                       "not a non-negative integer: ~s"
                                       v))]
                            [(null? (cdr rest))
                             (let ([the-error-case (car rest)])
                               (if (procedure? the-error-case)
                                   the-error-case
                                   (lambda (v)
                                     (errorf the-error-case
                                             "not a non-negative integer: ~s"
                                             v))))]
                            [else
                             (errorf 'fold-right_nat
                                     "improper actual parameters: ~s"
                                     rest)])])
          (lambda (v_init)
            (letrec ([visit
                      (lambda (i)
                        (if (= i 0)
                            zero-case
                            (succ-case (visit (1- i)))))])
              (if (and (integer? v_init)
                       (not (negative? v_init)))
                  (visit v_init)
                  (error-case v_init))))))
      1
      (lambda (c)
        (* x c))
      'power_v1)
     n)))

(unless (test-power 'power_v1a power_v1a)
  (printf "(test-power 'power_v1a power_v1a) failed~n"))

;;; Application of (lambda (zero-case succ-case . rest) ...)
;;; to 1, (lambda (c) (* x c)), and 'power_v1:

(define power_v1b
  (lambda (x n)
    ((let ([zero-case 1]
           [succ-case (lambda (c) (* x c))]
           [rest (list 'power_v1)])
       (let ([error-case (cond
                            [(null? rest)
                             (lambda (v)
                               (errorf 'fold-right_nat
                                       "not a non-negative integer: ~s"
                                       v))]
                            [(null? (cdr rest))
                             (let ([the-error-case (car rest)])
                               (if (procedure? the-error-case)
                                   the-error-case
                                   (lambda (v)
                                     (errorf the-error-case
                                             "not a non-negative integer: ~s"
                                             v))))]
                            [else
                             (errorf 'fold-right_nat
                                     "improper actual parameters: ~s"
                                     rest)])])
          (lambda (v_init)
            (letrec ([visit
                      (lambda (i)
                        (if (= i 0)
                            zero-case
                            (succ-case (visit (1- i)))))])
              (if (and (integer? v_init)
                       (not (negative? v_init)))
                  (visit v_init)
                  (error-case v_init))))))
     n)))

(unless (test-power 'power_v1b power_v1b)
  (printf "(test-power 'power_v1b power_v1b) failed~n"))

;;; Computation of (list 'power_v1):

(define power_v1c
  (lambda (x n)
    ((let ([zero-case 1]
           [succ-case (lambda (c) (* x c))]
           [rest '(power_v1)])
       (let ([error-case (cond
                            [(null? rest)
                             (lambda (v)
                               (errorf 'fold-right_nat
                                       "not a non-negative integer: ~s"
                                       v))]
                            [(null? (cdr rest))
                             (let ([the-error-case (car rest)])
                               (if (procedure? the-error-case)
                                   the-error-case
                                   (lambda (v)
                                     (errorf the-error-case
                                             "not a non-negative integer: ~s"
                                             v))))]
                            [else
                             (errorf 'fold-right_nat
                                     "improper actual parameters: ~s"
                                     rest)])])
          (lambda (v_init)
            (letrec ([visit
                      (lambda (i)
                        (if (= i 0)
                            zero-case
                            (succ-case (visit (1- i)))))])
              (if (and (integer? v_init)
                       (not (negative? v_init)))
                  (visit v_init)
                  (error-case v_init))))))
     n)))

(unless (test-power 'power_v1c power_v1c)
  (printf "(test-power 'power_v1c power_v1c) failed~n"))

;;; Unfolding of the let-expression declaring zero-case, succ-case, and rest:

(define power_v1d
  (lambda (x n)
    ((let ([error-case (cond
                         [(null? '(power_v1))
                          (lambda (v)
                            (errorf 'fold-right_nat
                                    "not a non-negative integer: ~s"
                                    v))]
                         [(null? (cdr '(power_v1)))
                          (let ([the-error-case (car '(power_v1))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a non-negative integer: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-right_nat
                                  "improper actual parameters: ~s"
                                  '(power_v1))])])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         1
                         ((lambda (c) (* x c)) (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     n)))

(unless (test-power 'power_v1d power_v1d)
  (printf "(test-power 'power_v1d power_v1d) failed~n"))

;;; Computation of (null? '(power_v1)):

(define power_v1e
  (lambda (x n)
    ((let ([error-case (cond
                         [#f
                          (lambda (v)
                            (errorf 'fold-right_nat
                                    "not a non-negative integer: ~s"
                                    v))]
                         [(null? (cdr '(power_v1)))
                          (let ([the-error-case (car '(power_v1))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a non-negative integer: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-right_nat
                                  "improper actual parameters: ~s"
                                  '(power_v1))])])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         1
                         ((lambda (c) (* x c)) (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     n)))

(unless (test-power 'power_v1e power_v1e)
  (printf "(test-power 'power_v1e power_v1e) failed~n"))

;;; Elimination of the first cond-clause:

(define power_v1f
  (lambda (x n)
    ((let ([error-case (cond
                         [(null? (cdr '(power_v1)))
                          (let ([the-error-case (car '(power_v1))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a non-negative integer: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-right_nat
                                  "improper actual parameters: ~s"
                                  '(power_v1))])])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         1
                         ((lambda (c) (* x c)) (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     n)))

(unless (test-power 'power_v1f power_v1f)
  (printf "(test-power 'power_v1f power_v1f) failed~n"))

;;; Computation of (cdr '(power_v1)):

(define power_v1g
  (lambda (x n)
    ((let ([error-case (cond
                         [(null? '())
                          (let ([the-error-case (car '(power_v1))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a non-negative integer: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-right_nat
                                  "improper actual parameters: ~s"
                                  '(power_v1))])])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         1
                         ((lambda (c) (* x c)) (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     n)))

(unless (test-power 'power_v1g power_v1g)
  (printf "(test-power 'power_v1g power_v1g) failed~n"))

;;; Computation of (null? '()):

(define power_v1h
  (lambda (x n)
    ((let ([error-case (cond
                         [#t
                          (let ([the-error-case (car '(power_v1))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a non-negative integer: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-right_nat
                                  "improper actual parameters: ~s"
                                  '(power_v1))])])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         1
                         ((lambda (c) (* x c)) (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     n)))

(unless (test-power 'power_v1h power_v1h)
  (printf "(test-power 'power_v1h power_v1h) failed~n"))

;;; Selection of the first cond-clause:

(define power_v1i
  (lambda (x n)
    ((let ([error-case (let ([the-error-case (car '(power_v1))])
                         (if (procedure? the-error-case)
                             the-error-case
                             (lambda (v)
                               (errorf the-error-case
                                       "not a non-negative integer: ~s"
                                       v))))])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         1
                         ((lambda (c) (* x c)) (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     n)))

(unless (test-power 'power_v1i power_v1i)
  (printf "(test-power 'power_v1i power_v1i) failed~n"))

;;; Computation of (car '(power_v1)):

(define power_v1j
  (lambda (x n)
    ((let ([error-case (let ([the-error-case 'power_v1])
                         (if (procedure? the-error-case)
                             the-error-case
                             (lambda (v)
                               (errorf the-error-case
                                       "not a non-negative integer: ~s"
                                       v))))])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         1
                         ((lambda (c) (* x c)) (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     n)))

(unless (test-power 'power_v1j power_v1j)
  (printf "(test-power 'power_v1j power_v1j) failed~n"))

;;; Unfolding of the let-expression declaring the-error-case:

(define power_v1k
  (lambda (x n)
    ((let ([error-case (if (procedure? 'power_v1)
                           'power_v1
                           (lambda (v)
                             (errorf 'power_v1
                                     "not a non-negative integer: ~s"
                                     v)))])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         1
                         ((lambda (c) (* x c)) (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     n)))

(unless (test-power 'power_v1k power_v1k)
  (printf "(test-power 'power_v1k power_v1k) failed~n"))

;;; Computation of (procedure? 'power_v1):

(define power_v1l
  (lambda (x n)
    ((let ([error-case (if #f
                           'power_v1
                           (lambda (v)
                             (errorf 'power_v1
                                     "not a non-negative integer: ~s"
                                     v)))])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         1
                         ((lambda (c) (* x c)) (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     n)))

(unless (test-power 'power_v1l power_v1l)
  (printf "(test-power 'power_v1l power_v1l) failed~n"))

;;; Selection of the alternative:

(define power_v1m
  (lambda (x n)
    ((let ([error-case (lambda (v)
                         (errorf 'power_v1
                                 "not a non-negative integer: ~s"
                                 v))])
       (lambda (v_init)
         (letrec ([visit
                   (lambda (i)
                     (if (= i 0)
                         1
                         ((lambda (c) (* x c)) (visit (1- i)))))])
           (if (and (integer? v_init)
                    (not (negative? v_init)))
               (visit v_init)
               (error-case v_init)))))
     n)))

(unless (test-power 'power_v1m power_v1m)
  (printf "(test-power 'power_v1m power_v1m) failed~n"))

;;; Unfolding of the let-expression declaring error-case:

(define power_v1n
  (lambda (x n)
    ((lambda (v_init)
       (letrec ([visit
                 (lambda (i)
                   (if (= i 0)
                       1
                       ((lambda (c) (* x c)) (visit (1- i)))))])
         (if (and (integer? v_init)
                  (not (negative? v_init)))
             (visit v_init)
             ((lambda (v)
                (errorf 'power_v1
                        "not a non-negative integer: ~s"
                        v)) v_init))))
     n)))

(unless (test-power 'power_v1n power_v1n)
  (printf "(test-power 'power_v1n power_v1n) failed~n"))

;;; Application of (lambda (v_init) ...) to n:

(define power_v1o
  (lambda (x n)
    (letrec ([visit
                 (lambda (i)
                   (if (= i 0)
                       1
                       ((lambda (c) (* x c)) (visit (1- i)))))])
         (if (and (integer? n)
                  (not (negative? n)))
             (visit n)
             ((lambda (v)
                (errorf 'power_v1
                        "not a non-negative integer: ~s"
                        v)) n)))))

(unless (test-power 'power_v1o power_v1o)
  (printf "(test-power 'power_v1o power_v1o) failed~n"))

;;; Application of (lambda (v) ...) to n:

(define power_v1
  (lambda (x n)
    (letrec ([visit
                 (lambda (i)
                   (if (= i 0)
                       1
                       ((lambda (c) (* x c)) (visit (1- i)))))])
         (if (and (integer? n)
                  (not (negative? n)))
             (visit n)
             (errorf 'power_v1
                     "not a non-negative integer: ~s"
                     n)))))

(unless (test-power 'power_v1 power_v1)
  (printf "(test-power 'power_v1 power_v1) failed~n"))

;;;;;;;;;;;;;;;;;;;;

(define fold-right_proper-list
  (lambda (nil-case cons-case . rest)
    (let ([error-case (cond
                        [(null? rest)
                         (lambda (v)
                           (errorf 'fold-right_proper-list
                                   "not a proper list: ~s"
                                   v))]
                        [(null? (cdr rest))
                         (let ([the-error-case (car rest)])
                           (if (procedure? the-error-case)
                               the-error-case
                               (lambda (v)
                                 (errorf the-error-case
                                         "not a proper list: ~s"
                                         v))))]
                        [else
                         (errorf 'fold-right_proper-list
                                 "improper actual parameters: ~s"
                                 rest)])])
      (lambda (v_init)
        (letrec ([visit (lambda (v)
                          (cond
                            [(null? v)
                             nil-case]
                            [(pair? v)
                             (cons-case (car v)
                                        (visit (cdr v)))]
                            [else
                             (error-case v)]))])
          (visit v_init))))))

;;;;;;;;;;;;;;;;;;;;

(define test-append2
  (lambda (name candidate)
    (and-all (try-candidate name candidate (list) (list) (list))
             (try-candidate name candidate (list 20 30 40) (list 20) (list 30 40))
             (try-candidate name candidate (list 10 20 30 40) (list 10 20) (list 30 40))
             (try-candidate name candidate (list 10 20) (list 10 20) (list))
             ;;;
             )))

;;; Generic definition of proper-list concatenation:

(define append2_v0
  (lambda (xs ys)
    ((fold-right_proper-list ys cons 'append2_v0) xs)))

(unless (test-append2 'append2_v0 append2_v0)
  (printf "(test-append2 'append2_v0 append2_v0) failed~n"))

;;; Renaming of append_v0 into append_v1 and
;;; inlining of fold-right_proper-list:

(define append2_v1a
  (lambda (xs ys)
    (((lambda (nil-case cons-case . rest)
       (let ([error-case (cond
                           [(null? rest)
                            (lambda (v)
                              (errorf 'fold-right_proper-list
                                      "not a proper list: ~s"
                                      v))]
                           [(null? (cdr rest))
                            (let ([the-error-case (car rest)])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a proper list: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-right_proper-list
                                    "improper actual parameters: ~s"
                                    rest)])])
         (lambda (v_init)
           (letrec ([visit (lambda (v)
                             (cond
                               [(null? v)
                                nil-case]
                               [(pair? v)
                                (cons-case (car v)
                                           (visit (cdr v)))]
                               [else
                                (error-case v)]))])
             (visit v_init)))))
      ys
      cons
      'append2_v0)
     xs)))

(unless (test-append2 'append2_v1a append2_v1a)
  (printf "(test-append2 'append2_v1a append2_v1a) failed~n"))

;;; Application of (lambda (nil-case cons-case . rest) ...)
;;; to ys, cons, and 'append2_v0:

(define append2_v1b
  (lambda (xs ys)
    ((let ([nil-case ys]
           [cons-case cons]
           [rest (list 'append2_v1)])
       (let ([error-case (cond
                           [(null? rest)
                            (lambda (v)
                              (errorf 'fold-right_proper-list
                                      "not a proper list: ~s"
                                      v))]
                           [(null? (cdr rest))
                            (let ([the-error-case (car rest)])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a proper list: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-right_proper-list
                                    "improper actual parameters: ~s"
                                    rest)])])
         (lambda (v_init)
           (letrec ([visit (lambda (v)
                             (cond
                               [(null? v)
                                nil-case]
                               [(pair? v)
                                (cons-case (car v)
                                           (visit (cdr v)))]
                               [else
                                (error-case v)]))])
             (visit v_init)))))
     xs)))

(unless (test-append2 'append2_v1b append2_v1b)
  (printf "(test-append2 'append2_v1b append2_v1b) failed~n"))

;;; Computation of (list 'append2_v1)

(define append2_v1c
  (lambda (xs ys)
    ((let ([nil-case ys]
           [cons-case cons]
           [rest '(append2_v1)])
       (let ([error-case (cond
                           [(null? rest)
                            (lambda (v)
                              (errorf 'fold-right_proper-list
                                      "not a proper list: ~s"
                                      v))]
                           [(null? (cdr rest))
                            (let ([the-error-case (car rest)])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a proper list: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-right_proper-list
                                    "improper actual parameters: ~s"
                                    rest)])])
         (lambda (v_init)
           (letrec ([visit (lambda (v)
                             (cond
                               [(null? v)
                                nil-case]
                               [(pair? v)
                                (cons-case (car v)
                                           (visit (cdr v)))]
                               [else
                                (error-case v)]))])
             (visit v_init)))))
     xs)))

(unless (test-append2 'append2_v1c append2_v1c)
  (printf "(test-append2 'append2_v1c append2_v1c) failed~n"))

;;; Unfolding of the let-expression declaring nil-case, cons-case, and rest:

(define append2_v1d
  (lambda (xs ys)
    ((let ([error-case (cond
                           [(null? '(append2_v1))
                            (lambda (v)
                              (errorf 'fold-right_proper-list
                                      "not a proper list: ~s"
                                      v))]
                           [(null? (cdr '(append2_v1)))
                            (let ([the-error-case (car '(append2_v1))])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a proper list: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-right_proper-list
                                    "improper actual parameters: ~s"
                                    '(append2_v1))])])
         (lambda (v_init)
           (letrec ([visit (lambda (v)
                             (cond
                               [(null? v)
                                ys]
                               [(pair? v)
                                (cons (car v)
                                      (visit (cdr v)))]
                               [else
                                (error-case v)]))])
             (visit v_init))))
     xs)))

(unless (test-append2 'append2_v1d append2_v1d)
  (printf "(test-append2 'append2_v1d append2_v1d) failed~n"))

;;; Computation of (null? '(append2_v1)):

(define append2_v1e
  (lambda (xs ys)
    ((let ([error-case (cond
                           [#f
                            (lambda (v)
                              (errorf 'fold-right_proper-list
                                      "not a proper list: ~s"
                                      v))]
                           [(null? (cdr '(append2_v1)))
                            (let ([the-error-case (car '(append2_v1))])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a proper list: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-right_proper-list
                                    "improper actual parameters: ~s"
                                    '(append2_v1))])])
         (lambda (v_init)
           (letrec ([visit (lambda (v)
                             (cond
                               [(null? v)
                                ys]
                               [(pair? v)
                                (cons (car v)
                                      (visit (cdr v)))]
                               [else
                                (error-case v)]))])
             (visit v_init))))
     xs)))

(unless (test-append2 'append2_v1e append2_v1e)
  (printf "(test-append2 'append2_v1e append2_v1e) failed~n"))

;;; Elimination of the first cond-clause:

(define append2_v1f
  (lambda (xs ys)
    ((let ([error-case (cond
                           [(null? (cdr '(append2_v1)))
                            (let ([the-error-case (car '(append2_v1))])
                              (if (procedure? the-error-case)
                                  error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a proper list: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-right_proper-list
                                    "improper actual parameters: ~s"
                                    '(append2_v1))])])
         (lambda (v_init)
           (letrec ([visit (lambda (v)
                             (cond
                               [(null? v)
                                ys]
                               [(pair? v)
                                (cons (car v)
                                      (visit (cdr v)))]
                               [else
                                (error-case v)]))])
             (visit v_init))))
     xs)))

(unless (test-append2 'append2_v1f append2_v1f)
  (printf "(test-append2 'append2_v1f append2_v1f) failed~n"))

;;; Computation of (cdr '(append2_v1)):

(define append2_v1g
  (lambda (xs ys)
    ((let ([error-case (cond
                           [(null? '())
                            (let ([the-error-case (car '(append2_v1))])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a proper list: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-right_proper-list
                                    "improper actual parameters: ~s"
                                    '(append2_v1))])])
         (lambda (v_init)
           (letrec ([visit (lambda (v)
                             (cond
                               [(null? v)
                                ys]
                               [(pair? v)
                                (cons (car v)
                                      (visit (cdr v)))]
                               [else
                                (error-case v)]))])
             (visit v_init))))
     xs)))

(unless (test-append2 'append2_v1g append2_v1g)
  (printf "(test-append2 'append2_v1g append2_v1g) failed~n"))

;;; Computation of (null? '()):

(define append2_v1h
  (lambda (xs ys)
    ((let ([error-case (cond
                           [#t
                            (let ([the-error-case (car '(append2_v1))])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a proper list: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-right_proper-list
                                    "improper actual parameters: ~s"
                                    '(append2_v1))])])
         (lambda (v_init)
           (letrec ([visit (lambda (v)
                             (cond
                               [(null? v)
                                ys]
                               [(pair? v)
                                (cons (car v)
                                      (visit (cdr v)))]
                               [else
                                (error-case v)]))])
             (visit v_init))))
     xs)))

(unless (test-append2 'append2_v1h append2_v1h)
  (printf "(test-append2 'append2_v1h append2_v1h) failed~n"))

;;; Selection of the first cond-clause:

(define append2_v1i
  (lambda (xs ys)
    ((let ([error-case (let ([the-error-case (car '(append2_v1))])
                         (if (procedure? the-error-case)
                             the-error-case
                             (lambda (v)
                               (errorf the-error-case
                                       "not a proper list: ~s"
                                       v))))])
       (lambda (v_init)
         (letrec ([visit (lambda (v)
                           (cond
                             [(null? v)
                              ys]
                             [(pair? v)
                              (cons (car v)
                                    (visit (cdr v)))]
                             [else
                              (error-case v)]))])
           (visit v_init))))
     xs)))

(unless (test-append2 'append2_v1i append2_v1i)
  (printf "(test-append2 'append2_v1i append2_v1i) failed~n"))

;;; Computation of (car '(append2_v1)):

(define append2_v1j
  (lambda (xs ys)
    ((let ([error-case (let ([the-error-case 'append2_v1])
                         (if (procedure? the-error-case)
                             the-error-case
                             (lambda (v)
                               (errorf the-error-case
                                       "not a proper list: ~s"
                                       v))))])
       (lambda (v_init)
         (letrec ([visit (lambda (v)
                           (cond
                             [(null? v)
                              ys]
                             [(pair? v)
                              (cons (car v)
                                    (visit (cdr v)))]
                             [else
                              (error-case v)]))])
           (visit v_init))))
     xs)))

(unless (test-append2 'append2_v1j append2_v1j)
  (printf "(test-append2 'append2_v1j append2_v1j) failed~n"))

;;; Unfolding of the let-expression declaring the-error-case:

(define append2_v1k
  (lambda (xs ys)
    ((let ([error-case (if (procedure? 'append2_v1)
                           'append2_v1
                           (lambda (v)
                             (errorf 'append2_v1
                                     "not a proper list: ~s"
                                     v)))])
       (lambda (v_init)
         (letrec ([visit (lambda (v)
                           (cond
                             [(null? v)
                              ys]
                             [(pair? v)
                              (cons (car v)
                                    (visit (cdr v)))]
                             [else
                              (error-case v)]))])
           (visit v_init))))
     xs)))

(unless (test-append2 'append2_v1k append2_v1k)
  (printf "(test-append2 'append2_v1k append2_v1k) failed~n"))

;;; Computation of (procedure? 'append2_v1):

(define append2_v1l
  (lambda (xs ys)
    ((let ([error-case (if #t
                           'append2_v1
                           (lambda (v)
                             (errorf 'append2_v1
                                     "not a proper list: ~s"
                                     v)))])
       (lambda (v_init)
         (letrec ([visit (lambda (v)
                           (cond
                             [(null? v)
                              ys]
                             [(pair? v)
                              (cons (car v)
                                    (visit (cdr v)))]
                             [else
                              (error-case v)]))])
           (visit v_init))))
     xs)))

(unless (test-append2 'append2_v1l append2_v1l)
  (printf "(test-append2 'append2_v1l append2_v1l) failed~n"))

;;; Selection of the alternative:

(define append2_v1m
  (lambda (xs ys)
    ((let ([error-case (lambda (v)
                         (errorf 'append2_v1
                                 "not a proper list: ~s"
                                 v))])
       (lambda (v_init)
         (letrec ([visit (lambda (v)
                           (cond
                             [(null? v)
                              ys]
                             [(pair? v)
                              (cons (car v)
                                    (visit (cdr v)))]
                             [else
                              (error-case v)]))])
           (visit v_init))))
     xs)))

(unless (test-append2 'append2_v1m append2_v1m)
  (printf "(test-append2 'append2_v1m append2_v1m) failed~n"))

;;; Unfolding of the let-expression declaring error-case:

(define append2_v1n
  (lambda (xs ys)
    ((lambda (v_init)
       (letrec ([visit (lambda (v)
                         (cond
                           [(null? v)
                            ys]
                           [(pair? v)
                            (cons (car v)
                                  (visit (cdr v)))]
                           [else
                            ((lambda (v)
                               (errorf 'append2_v1
                                       "not a proper list: ~s"
                                       v)) v)]))])
         (visit v_init)))
     xs)))

(unless (test-append2 'append2_v1n append2_v1n)
  (printf "(test-append2 'append2_v1n append2_v1n) failed~n"))

;;; Renaming of the inner (lambda (v) ...) into (lambda (w) ...)
;;; for hygiene:

(define append2_v1o
  (lambda (xs ys)
    ((lambda (v_init)
       (letrec ([visit (lambda (v)
                         (cond
                           [(null? v)
                            ys]
                           [(pair? v)
                            (cons (car v)
                                  (visit (cdr v)))]
                           [else
                            ((lambda (w)
                               (errorf 'append2_v1
                                       "not a proper list: ~s"
                                       w)) v)]))])
         (visit v_init)))
     xs)))

(unless (test-append2 'append2_v1o append2_v1o)
  (printf "(test-append2 'append2_v1o append2_v1o) failed~n"))

;;; Application of (lambda (v_init) ...) to xs:

(define append2_v1p
  (lambda (xs ys)
    (letrec ([visit (lambda (v)
                         (cond
                           [(null? v)
                            ys]
                           [(pair? v)
                            (cons (car v)
                                  (visit (cdr v)))]
                           [else
                            ((lambda (w)
                               (errorf 'append2_v1
                                       "not a proper list: ~s"
                                       w)) v)]))])
         (visit xs))))

(unless (test-append2 'append2_v1p append2_v1p)
  (printf "(test-append2 'append2_v1p append2_v1p) failed~n"))

;;; application of (lambda (w) ...) to v:

(define append2_v1
  (lambda (xs ys)
    (letrec ([visit (lambda (v)
                         (cond
                           [(null? v)
                            ys]
                           [(pair? v)
                            (cons (car v)
                                  (visit (cdr v)))]
                           [else
                            (errorf 'append2_v1
                                    "not a proper list: ~s"
                                    v)]))])
         (visit xs))))

(unless (test-append2 'append2_v1 append2_v1)
  (printf "(test-append2 'append2_v1 append2_v1) failed~n"))

;;;;;;;;;;;;;;;;;;;;

(define test-reverse
  (lambda (name candidate)
    (and-all (try-candidate name candidate (list) (list))
             (try-candidate name candidate (list 10) (list 10))
             (try-candidate name candidate (list 20 10) (list 10 20))
             (try-candidate name candidate (list 30 20 10) (list 10 20 30))
             ;;;
             )))

;;; Generic definition of proper-list reversal:

(define reverse_v0
  (lambda (xs)
    (((fold-right_proper-list (lambda (ys)
                                ys)
                              (lambda (x c)
                                (lambda (ys)
                                  (c (cons x ys))))
                              'reverse_v0)
      xs)
     '())))

(unless (test-reverse 'reverse_v0 reverse_v0)
  (printf "(test-reverse 'reverse_v0 reverse_v0) failed~n"))

;;; Exercise: inline the definition of fold-right_proper-list in reverse_v0,
;;; all the way.

;;; Inline fold-right_proper-list

(define reverse_v1a
  (lambda (xs)
    ((((lambda (nil-case cons-case . rest)
         (let ([error-case (cond
                             [(null? rest)
                              (lambda (v)
                                (errorf 'fold-right_proper-list
                                        "not a proper list: ~s"
                                        v))]
                             [(null? (cdr rest))
                              (let ([the-error-case (car rest)])
                                (if (procedure? the-error-case)
                                    the-error-case
                                    (lambda (v)
                                      (errorf the-error-case
                                              "not a proper list: ~s"
                                              v))))]
                             [else
                              (errorf 'fold-right_proper-list
                                      "improper actual parameters: ~s"
                                      rest)])])
           (lambda (v_init)
             (letrec ([visit (lambda (v)
                               (cond
                                 [(null? v)
                                  nil-case]
                                 [(pair? v)
                                  (cons-case (car v)
                                             (visit (cdr v)))]
                                 [else
                                  (error-case v)]))])
               (visit v_init)))))
       (lambda (ys)
         ys)
       (lambda (x c)
         (lambda (ys)
           (c (cons x ys))))
       'reverse_v1a)
      xs)
     '())))

(unless (test-reverse 'reverse_v1a reverse_v1a)
  (printf "(test-reverse 'reverse_v1a reverse_v1a) failed~n"))

;;; Application of (lambda (nil-case cons-case . rest) (let ...)) on
;;; (lambda (ys) ys), (lambda (x c) ...) and 'reverse_v1b

(define reverse_v1b
  (lambda (xs)
    (((let ([nil-case
             (lambda (ys)
               ys)]
            [cons-case
             (lambda (x c)
               (lambda (ys)
                 (c (cons x ys))))]
            [rest (list 'reverse_v1b)])
        (let ([error-case (cond
                            [(null? rest)
                             (lambda (v)
                               (errorf 'fold-right_proper-list
                                       "not a proper list: ~s"
                                       v))]
                            [(null? (cdr rest))
                             (let ([the-error-case (car rest)])
                               (if (procedure? the-error-case)
                                   the-error-case
                                   (lambda (v)
                                     (errorf the-error-case
                                             "not a proper list: ~s"
                                             v))))]
                            [else
                             (errorf 'fold-right_proper-list
                                     "improper actual parameters: ~s"
                                     rest)])])
          (lambda (v_init)
            (letrec ([visit (lambda (v)
                              (cond
                                [(null? v)
                                 nil-case]
                                [(pair? v)
                                 (cons-case (car v)
                                            (visit (cdr v)))]
                                [else
                                 (error-case v)]))])
              (visit v_init)))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1b reverse_v1b)
  (printf "(test-reverse 'reverse_v1b reverse_v1b) failed~n"))

;;; Computation of (list 'reverse_v1c)

(define reverse_v1c
  (lambda (xs)
    (((let ([nil-case
             (lambda (ys)
               ys)]
            [cons-case
             (lambda (x c)
               (lambda (ys)
                 (c (cons x ys))))]
            [rest '(reverse_v1c)])
        (let ([error-case (cond
                            [(null? rest)
                             (lambda (v)
                               (errorf 'fold-right_proper-list
                                       "not a proper list: ~s"
                                       v))]
                            [(null? (cdr rest))
                             (let ([the-error-case (car rest)])
                               (if (procedure? the-error-case)
                                   the-error-case
                                   (lambda (v)
                                     (errorf the-error-case
                                             "not a proper list: ~s"
                                             v))))]
                            [else
                             (errorf 'fold-right_proper-list
                                     "improper actual parameters: ~s"
                                     rest)])])
          (lambda (v_init)
            (letrec ([visit (lambda (v)
                              (cond
                                [(null? v)
                                 nil-case]
                                [(pair? v)
                                 (cons-case (car v)
                                            (visit (cdr v)))]
                                [else
                                 (error-case v)]))])
              (visit v_init)))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1c reverse_v1c)
  (printf "(test-reverse 'reverse_v1c reverse_v1c) failed~n"))

;;; Unfolding of the let-expression declaring nil-case, cons-case, and rest

(define reverse_v1d
  (lambda (xs)
    (((let ([error-case (cond
                          [(null? '(reverse_v1d))
                           (lambda (v)
                             (errorf 'fold-right_proper-list
                                     "not a proper list: ~s"
                                     v))]
                          [(null? (cdr '(reverse_v1d)))
                           (let ([the-error-case (car '(reverse_v1d))])
                             (if (procedure? the-error-case)
                                 the-error-case
                                 (lambda (v)
                                   (errorf the-error-case
                                           "not a proper list: ~s"
                                           v))))]
                          [else
                           (errorf 'fold-right_proper-list
                                   "improper actual parameters: ~s"
                                   '(reverse_v1d))])])
        (lambda (v_init)
          (letrec ([visit (lambda (v)
                            (cond
                              [(null? v)
                               (lambda (ys)
                                 ys)]
                              [(pair? v)
                               ((lambda (x c)
                                  (lambda (ys)
                                    (c (cons x ys))))
                                (car v)
                                (visit (cdr v)))]
                              [else
                               (error-case v)]))])
            (visit v_init))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1d reverse_v1d)
  (printf "(test-reverse 'reverse_v1d reverse_v1d) failed~n"))

;;; Computation of (null? '(reverse_v1e))

(define reverse_v1e
  (lambda (xs)
    (((let ([error-case (cond
                          [#f
                           (lambda (v)
                             (errorf 'fold-right_proper-list
                                     "not a proper list: ~s"
                                     v))]
                          [(null? (cdr '(reverse_v1e)))
                           (let ([the-error-case (car '(reverse_v1e))])
                             (if (procedure? the-error-case)
                                 the-error-case
                                 (lambda (v)
                                   (errorf the-error-case
                                           "not a proper list: ~s"
                                           v))))]
                          [else
                           (errorf 'fold-right_proper-list
                                   "improper actual parameters: ~s"
                                   '(reverse_v1e))])])
        (lambda (v_init)
          (letrec ([visit (lambda (v)
                            (cond
                              [(null? v)
                               (lambda (ys)
                                 ys)]
                              [(pair? v)
                               ((lambda (x c)
                                  (lambda (ys)
                                    (c (cons x ys))))
                                (car v)
                                (visit (cdr v)))]
                              [else
                               (error-case v)]))])
            (visit v_init))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1e reverse_v1e)
  (printf "(test-reverse 'reverse_v1e reverse_v1e) failed~n"))

;;; Elimination of the first cond-clause

(define reverse_v1f
  (lambda (xs)
    (((let ([error-case (cond
                          [(null? (cdr '(reverse_v1f)))
                           (let ([the-error-case (car '(reverse_v1f))])
                             (if (procedure? the-error-case)
                                 the-error-case
                                 (lambda (v)
                                   (errorf the-error-case
                                           "not a proper list: ~s"
                                           v))))]
                          [else
                           (errorf 'fold-right_proper-list
                                   "improper actual parameters: ~s"
                                   '(reverse_v1f))])])
        (lambda (v_init)
          (letrec ([visit (lambda (v)
                            (cond
                              [(null? v)
                               (lambda (ys)
                                 ys)]
                              [(pair? v)
                               ((lambda (x c)
                                  (lambda (ys)
                                    (c (cons x ys))))
                                (car v)
                                (visit (cdr v)))]
                              [else
                               (error-case v)]))])
            (visit v_init))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1f reverse_v1f)
  (printf "(test-reverse 'reverse_v1f reverse_v1f) failed~n"))

;;; Computation of (cdr '(reverse_v1g))

(define reverse_v1g
  (lambda (xs)
    (((let ([error-case (cond
                          [(null? '())
                           (let ([the-error-case (car '(reverse_v1g))])
                             (if (procedure? the-error-case)
                                 the-error-case
                                 (lambda (v)
                                   (errorf the-error-case
                                           "not a proper list: ~s"
                                           v))))]
                          [else
                           (errorf 'fold-right_proper-list
                                   "improper actual parameters: ~s"
                                   '(reverse_v1g))])])
        (lambda (v_init)
          (letrec ([visit (lambda (v)
                            (cond
                              [(null? v)
                               (lambda (ys)
                                 ys)]
                              [(pair? v)
                               ((lambda (x c)
                                  (lambda (ys)
                                    (c (cons x ys))))
                                (car v)
                                (visit (cdr v)))]
                              [else
                               (error-case v)]))])
            (visit v_init))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1g reverse_v1g)
  (printf "(test-reverse 'reverse_v1g reverse_v1g) failed~n"))

;;; Computation of (null? '())

(define reverse_v1h
  (lambda (xs)
    (((let ([error-case (cond
                          [#t
                           (let ([the-error-case (car '(reverse_v1h))])
                             (if (procedure? the-error-case)
                                 the-error-case
                                 (lambda (v)
                                   (errorf the-error-case
                                           "not a proper list: ~s"
                                           v))))]
                          [else
                           (errorf 'fold-right_proper-list
                                   "improper actual parameters: ~s"
                                   '(reverse_v1h))])])
        (lambda (v_init)
          (letrec ([visit (lambda (v)
                            (cond
                              [(null? v)
                               (lambda (ys)
                                 ys)]
                              [(pair? v)
                               ((lambda (x c)
                                  (lambda (ys)
                                    (c (cons x ys))))
                                (car v)
                                (visit (cdr v)))]
                              [else
                               (error-case v)]))])
            (visit v_init))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1h reverse_v1h)
  (printf "(test-reverse 'reverse_v1h reverse_v1h) failed~n"))

;;; Selection of the first cond-clause

(define reverse_v1i
  (lambda (xs)
    (((let ([error-case (let ([the-error-case (car '(reverse_v1i))])
                          (if (procedure? the-error-case)
                              the-error-case
                              (lambda (v)
                                (errorf the-error-case
                                        "not a proper list: ~s"
                                        v))))])
        (lambda (v_init)
          (letrec ([visit (lambda (v)
                            (cond
                              [(null? v)
                               (lambda (ys)
                                 ys)]
                              [(pair? v)
                               ((lambda (x c)
                                  (lambda (ys)
                                    (c (cons x ys))))
                                (car v)
                                (visit (cdr v)))]
                              [else
                               (error-case v)]))])
            (visit v_init))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1i reverse_v1i)
  (printf "(test-reverse 'reverse_v1i reverse_v1i) failed~n"))

;;; Computation of (car '(reverse_v1j))

(define reverse_v1j
  (lambda (xs)
    (((let ([error-case (let ([the-error-case 'reverse_v1j])
                          (if (procedure? the-error-case)
                              the-error-case
                              (lambda (v)
                                (errorf the-error-case
                                        "not a proper list: ~s"
                                        v))))])
        (lambda (v_init)
          (letrec ([visit (lambda (v)
                            (cond
                              [(null? v)
                               (lambda (ys)
                                 ys)]
                              [(pair? v)
                               ((lambda (x c)
                                  (lambda (ys)
                                    (c (cons x ys))))
                                (car v)
                                (visit (cdr v)))]
                              [else
                               (error-case v)]))])
            (visit v_init))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1j reverse_v1j)
  (printf "(test-reverse 'reverse_v1j reverse_v1j) failed~n"))

;;; Unfolding of the let-expression declaring the-error-case

(define reverse_v1k
  (lambda (xs)
    (((let ([error-case (if (procedure? 'reverse_v1k)
                            'reverse_v1k
                            (lambda (v)
                              (errorf 'reverse_v1k
                                      "not a proper list: ~s"
                                      v)))])
        (lambda (v_init)
          (letrec ([visit (lambda (v)
                            (cond
                              [(null? v)
                               (lambda (ys)
                                 ys)]
                              [(pair? v)
                               ((lambda (x c)
                                  (lambda (ys)
                                    (c (cons x ys))))
                                (car v)
                                (visit (cdr v)))]
                              [else
                               (error-case v)]))])
            (visit v_init))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1k reverse_v1k)
  (printf "(test-reverse 'reverse_v1k reverse_v1k) failed~n"))

;;; Computation of (procedure? 'append2_v1)

(define reverse_v1l
  (lambda (xs)
    (((let ([error-case (if #f
                            'reverse_v1l
                            (lambda (v)
                              (errorf 'reverse_v1l
                                      "not a proper list: ~s"
                                      v)))])
        (lambda (v_init)
          (letrec ([visit (lambda (v)
                            (cond
                              [(null? v)
                               (lambda (ys)
                                 ys)]
                              [(pair? v)
                               ((lambda (x c)
                                  (lambda (ys)
                                    (c (cons x ys))))
                                (car v)
                                (visit (cdr v)))]
                              [else
                               (error-case v)]))])
            (visit v_init))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1l reverse_v1l)
  (printf "(test-reverse 'reverse_v1l reverse_v1l) failed~n"))

;;; Selection of the alternative

(define reverse_v1m
  (lambda (xs)
    (((let ([error-case (lambda (v)
                          (errorf 'reverse_v1m
                                  "not a proper list: ~s"
                                  v))])
        (lambda (v_init)
          (letrec ([visit (lambda (v)
                            (cond
                              [(null? v)
                               (lambda (ys)
                                 ys)]
                              [(pair? v)
                               ((lambda (x c)
                                  (lambda (ys)
                                    (c (cons x ys))))
                                (car v)
                                (visit (cdr v)))]
                              [else
                               (error-case v)]))])
            (visit v_init))))
      xs)
     '())))

(unless (test-reverse 'reverse_v1m reverse_v1m)
  (printf "(test-reverse 'reverse_v1m reverse_v1m) failed~n"))

;;; Unfolding of the let-expression declaring error-case

(define reverse_v1n
  (lambda (xs)
    (((lambda (v_init)
        (letrec ([visit (lambda (v)
                          (cond
                            [(null? v)
                             (lambda (ys)
                               ys)]
                            [(pair? v)
                             ((lambda (x c)
                                (lambda (ys)
                                  (c (cons x ys))))
                              (car v)
                              (visit (cdr v)))]
                            [else
                             ((lambda (v)
                                (errorf 'reverse_v1n
                                        "not a proper list: ~s"
                                        v))
                              v)]))])
          (visit v_init)))
      xs)
     '())))

(unless (test-reverse 'reverse_v1n reverse_v1n)
  (printf "(test-reverse 'reverse_v1n reverse_v1n) failed~n"))

;;; Renaming of the inner (lambda (v) ...) into (lambda (w) ...)
;;; for hygiene:

(define reverse_v1o
  (lambda (xs)
    (((lambda (v_init)
        (letrec ([visit (lambda (v)
                          (cond
                            [(null? v)
                             (lambda (ys)
                               ys)]
                            [(pair? v)
                             ((lambda (x c)
                                (lambda (ys)
                                  (c (cons x ys))))
                              (car v)
                              (visit (cdr v)))]
                            [else
                             ((lambda (w)
                                (errorf 'reverse_v1o
                                        "not a proper list: ~s"
                                        w))
                              v)]))])
          (visit v_init)))
      xs)
     '())))

(unless (test-reverse 'reverse_v1o reverse_v1o)
  (printf "(test-reverse 'reverse_v1o reverse_v1o) failed~n"))

;;; Application of (lambda (v_init) ...) to xs

(define reverse_v1p
  (lambda (xs)
    ((let ([v_init xs])
       (letrec ([visit (lambda (v)
                         (cond
                           [(null? v)
                            (lambda (ys)
                              ys)]
                           [(pair? v)
                            ((lambda (x c)
                               (lambda (ys)
                                 (c (cons x ys))))
                             (car v)
                             (visit (cdr v)))]
                           [else
                            ((lambda (w)
                               (errorf 'reverse_v1p
                                       "not a proper list: ~s"
                                       w))
                             v)]))])
         (visit v_init)))
     '())))

(unless (test-reverse 'reverse_v1p reverse_v1p)
  (printf "(test-reverse 'reverse_v1p reverse_v1p) failed~n"))

;;; Unfolding of the let-expression declaring v_init

(define reverse_v1q
  (lambda (xs)
    ((letrec ([visit (lambda (v)
                       (cond
                         [(null? v)
                          (lambda (ys)
                            ys)]
                         [(pair? v)
                          ((lambda (x c)
                             (lambda (ys)
                               (c (cons x ys))))
                           (car v)
                           (visit (cdr v)))]
                         [else
                          ((lambda (w)
                             (errorf 'reverse_v1q
                                     "not a proper list: ~s"
                                     w))
                           v)]))])
       (visit xs))
     '())))

(unless (test-reverse 'reverse_v1q reverse_v1q)
  (printf "(test-reverse 'reverse_v1q reverse_v1q) failed~n"))

;;; Application of (lambda (w) ...) to v

(define reverse_v1r
  (lambda (xs)
    ((letrec ([visit (lambda (v)
                       (cond
                         [(null? v)
                          (lambda (ys)
                            ys)]
                         [(pair? v)
                          ((lambda (x c)
                             (lambda (ys)
                               (c (cons x ys))))
                           (car v)
                           (visit (cdr v)))]
                         [else
                          (errorf 'reverse_v1r
                                  "not a proper list: ~s"
                                  v)]))])
       (visit xs))
     '())))

(unless (test-reverse 'reverse_v1r reverse_v1r)
  (printf "(test-reverse 'reverse_v1r reverse_v1r) failed~n"))

;;; Application of (lambda (x c) ...) to (car v), (visit ...)

(define reverse_v1s
  (lambda (xs)
    ((letrec ([visit (lambda (v)
                       (cond
                         [(null? v)
                          (lambda (ys)
                            ys)]
                         [(pair? v)
                          (let ([x (car v)]
                                [c (visit (cdr v))])
                            (lambda (ys)
                              (c (cons x ys))))]
                         [else
                          (errorf 'reverse_v1s
                                  "not a proper list: ~s"
                                  v)]))])
       (visit xs))
     '())))

(unless (test-reverse 'reverse_v1s reverse_v1s)
  (printf "(test-reverse 'reverse_v1s reverse_v1s) failed~n"))

;;;  Unfolding of the let-expression declaring x, c

(define reverse_v1
  (lambda (xs)
    ((letrec ([visit (lambda (v)
                       (cond
                         [(null? v)
                          (lambda (ys)
                            ys)]
                         [(pair? v)
                          (lambda (ys)
                            ((visit (cdr v))
                             (cons (car v) ys)))]
                         [else
                          (errorf 'reverse_v1
                                  "not a proper list: ~s"
                                  v)]))])
       (visit xs))
     '())))

(unless (test-reverse 'reverse_v1 reverse_v1)
  (printf "(test-reverse 'reverse_v1 reverse_v1) failed~n"))

;;;;;;;;;;;;;;;;;;;;

(define fold-left_proper-list
  (lambda (nil-case cons-case . rest)
    (let ([error-case (cond
                        [(null? rest)
                         (lambda (v)
                           (errorf 'fold-left_proper-list
                                   "not a proper list: ~s"
                                   v))]
                        [(null? (cdr rest))
                         (let ([the-error-case (car rest)])
                           (if (procedure? the-error-case)
                               the-error-case
                               (lambda (v)
                                 (errorf the-error-case
                                         "not a proper list: ~s"
                                         v))))]
                        [else
                         (errorf 'fold-left_proper-list
                                 "improper actual parameters: ~s"
                                 rest)])])
      (lambda (v_init)
        (letrec ([visit (lambda (v a)
                          (cond
                            [(null? v)
                             a]
                            [(pair? v)
                             (visit (cdr v)
                                    (cons-case (car v) a))]
                            [else
                             (error-case v)]))])
          (visit v_init nil-case))))))

;;; Other generic definition of proper-list reversal:

(define other-reverse_v0
  (lambda (xs)
    ((fold-left_proper-list '() cons 'other-reverse_v0) xs)))

(unless (test-reverse 'other-reverse_v0 other-reverse_v0)
  (printf "(test-reverse 'other-reverse_v0 other-reverse_v0) failed~n"))

;;; Exercise: inline the definition of fold-left_proper-list in other-reverse_v0,
;;; all the way.

;;; Inline fold-left_proper-list

(define other-reverse_v1a
  (lambda (xs)
    (((lambda (nil-case cons-case . rest)
        (let ([error-case (cond
                            [(null? rest)
                             (lambda (v)
                               (errorf 'fold-left_proper-list
                                       "not a proper list: ~s"
                                       v))]
                            [(null? (cdr rest))
                             (let ([the-error-case (car rest)])
                               (if (procedure? the-error-case)
                                   the-error-case
                                   (lambda (v)
                                     (errorf the-error-case
                                             "not a proper list: ~s"
                                             v))))]
                            [else
                             (errorf 'fold-left_proper-list
                                     "improper actual parameters: ~s"
                                     rest)])])
          (lambda (v_init)
            (letrec ([visit (lambda (v a)
                              (cond
                                [(null? v)
                                 a]
                                [(pair? v)
                                 (visit (cdr v)
                                        (cons-case (car v) a))]
                                [else
                                 (error-case v)]))])
              (visit v_init nil-case)))))
      '()
      cons
      'other-reverse_v1a)
     xs)))

(unless (test-reverse 'other-reverse_v1a other-reverse_v1a)
  (printf "(test-reverse 'other-reverse_v1a other-reverse_v1a) failed~n"))

;;; Application of (lambda (nil-case cons-case . rest) ...) to
;;; '(), cons, 'other-reverse_v1b

(define other-reverse_v1b
  (lambda (xs)
    ((let ([nil-case '()]
           [cons-case cons]
           [rest (list 'other-reverse_v1b)])
       (let ([error-case (cond
                           [(null? rest)
                            (lambda (v)
                              (errorf 'fold-left_proper-list
                                      "not a proper list: ~s"
                                      v))]
                           [(null? (cdr rest))
                            (let ([the-error-case (car rest)])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a proper list: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-left_proper-list
                                    "improper actual parameters: ~s"
                                    rest)])])
         (lambda (v_init)
           (letrec ([visit (lambda (v a)
                             (cond
                               [(null? v)
                                a]
                               [(pair? v)
                                (visit (cdr v)
                                       (cons-case (car v) a))]
                               [else
                                (error-case v)]))])
             (visit v_init nil-case)))))
     xs)))

(unless (test-reverse 'other-reverse_v1b other-reverse_v1b)
  (printf "(test-reverse 'other-reverse_v1b other-reverse_v1b) failed~n"))

;;; Compute (list 'other-reverse_v1c)

(define other-reverse_v1c
  (lambda (xs)
    ((let ([nil-case '()]
           [cons-case cons]
           [rest '(other-reverse_v1c)])
       (let ([error-case (cond
                           [(null? rest)
                            (lambda (v)
                              (errorf 'fold-left_proper-list
                                      "not a proper list: ~s"
                                      v))]
                           [(null? (cdr rest))
                            (let ([the-error-case (car rest)])
                              (if (procedure? the-error-case)
                                  the-error-case
                                  (lambda (v)
                                    (errorf the-error-case
                                            "not a proper list: ~s"
                                            v))))]
                           [else
                            (errorf 'fold-left_proper-list
                                    "improper actual parameters: ~s"
                                    rest)])])
         (lambda (v_init)
           (letrec ([visit (lambda (v a)
                             (cond
                               [(null? v)
                                a]
                               [(pair? v)
                                (visit (cdr v)
                                       (cons-case (car v) a))]
                               [else
                                (error-case v)]))])
             (visit v_init nil-case)))))
     xs)))

(unless (test-reverse 'other-reverse_v1c other-reverse_v1c)
  (printf "(test-reverse 'other-reverse_v1c other-reverse_v1c) failed~n"))

;;; Unfolding of the let-expression declaring nil-case, cons-case, and rest

(define other-reverse_v1d
  (lambda (xs)
    ((let ([error-case (cond
                         [(null? '(other-reverse_v1d))
                          (lambda (v)
                            (errorf 'fold-left_proper-list
                                    "not a proper list: ~s"
                                    v))]
                         [(null? (cdr '(other-reverse_v1d)))
                          (let ([the-error-case (car '(other-reverse_v1d))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a proper list: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-left_proper-list
                                  "improper actual parameters: ~s"
                                  '(other-reverse_v1d))])])
       (lambda (v_init)
         (letrec ([visit (lambda (v a)
                           (cond
                             [(null? v)
                              a]
                             [(pair? v)
                              (visit (cdr v)
                                     (cons (car v) a))]
                             [else
                              (error-case v)]))])
           (visit v_init '()))))
     xs)))

(unless (test-reverse 'other-reverse_v1d other-reverse_v1d)
  (printf "(test-reverse 'other-reverse_v1d other-reverse_v1d) failed~n"))

;;; Compute (null? '(other-reverse_v1e))

(define other-reverse_v1e
  (lambda (xs)
    ((let ([error-case (cond
                         [#f
                          (lambda (v)
                            (errorf 'fold-left_proper-list
                                    "not a proper list: ~s"
                                    v))]
                         [(null? (cdr '(other-reverse_v1e)))
                          (let ([the-error-case (car '(other-reverse_v1e))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a proper list: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-left_proper-list
                                  "improper actual parameters: ~s"
                                  '(other-reverse_v1e))])])
       (lambda (v_init)
         (letrec ([visit (lambda (v a)
                           (cond
                             [(null? v)
                              a]
                             [(pair? v)
                              (visit (cdr v)
                                     (cons (car v) a))]
                             [else
                              (error-case v)]))])
           (visit v_init '()))))
     xs)))

(unless (test-reverse 'other-reverse_v1e other-reverse_v1e)
  (printf "(test-reverse 'other-reverse_v1e other-reverse_v1e) failed~n"))

;;; Elimination of the first cond-clause

(define other-reverse_v1f
  (lambda (xs)
    ((let ([error-case (cond
                         [(null? (cdr '(other-reverse_v1f)))
                          (let ([the-error-case (car '(other-reverse_v1f))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a proper list: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-left_proper-list
                                  "improper actual parameters: ~s"
                                  '(other-reverse_v1f))])])
       (lambda (v_init)
         (letrec ([visit (lambda (v a)
                           (cond
                             [(null? v)
                              a]
                             [(pair? v)
                              (visit (cdr v)
                                     (cons (car v) a))]
                             [else
                              (error-case v)]))])
           (visit v_init '()))))
     xs)))

(unless (test-reverse 'other-reverse_v1f other-reverse_v1f)
  (printf "(test-reverse 'other-reverse_v1f other-reverse_v1f) failed~n"))

;;; Compute (cdr '(other-reverse_v1g))

(define other-reverse_v1g
  (lambda (xs)
    ((let ([error-case (cond
                         [(null? '())
                          (let ([the-error-case (car '(other-reverse_v1g))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a proper list: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-left_proper-list
                                  "improper actual parameters: ~s"
                                  '(other-reverse_v1g))])])
       (lambda (v_init)
         (letrec ([visit (lambda (v a)
                           (cond
                             [(null? v)
                              a]
                             [(pair? v)
                              (visit (cdr v)
                                     (cons (car v) a))]
                             [else
                              (error-case v)]))])
           (visit v_init '()))))
     xs)))

(unless (test-reverse 'other-reverse_v1g other-reverse_v1g)
  (printf "(test-reverse 'other-reverse_v1g other-reverse_v1g) failed~n"))

;;; Compute (null? '())

(define other-reverse_v1h
  (lambda (xs)
    ((let ([error-case (cond
                         [#t
                          (let ([the-error-case (car '(other-reverse_v1h))])
                            (if (procedure? the-error-case)
                                the-error-case
                                (lambda (v)
                                  (errorf the-error-case
                                          "not a proper list: ~s"
                                          v))))]
                         [else
                          (errorf 'fold-left_proper-list
                                  "improper actual parameters: ~s"
                                  '(other-reverse_v1h))])])
       (lambda (v_init)
         (letrec ([visit (lambda (v a)
                           (cond
                             [(null? v)
                              a]
                             [(pair? v)
                              (visit (cdr v)
                                     (cons (car v) a))]
                             [else
                              (error-case v)]))])
           (visit v_init '()))))
     xs)))

(unless (test-reverse 'other-reverse_v1h other-reverse_v1h)
  (printf "(test-reverse 'other-reverse_v1h other-reverse_v1h) failed~n"))

;;; Selection of the first cond-clause

(define other-reverse_v1i
  (lambda (xs)
    ((let ([error-case (let ([the-error-case (car '(other-reverse_v1i))])
                         (if (procedure? the-error-case)
                             the-error-case
                             (lambda (v)
                               (errorf the-error-case
                                       "not a proper list: ~s"
                                       v))))])
       (lambda (v_init)
         (letrec ([visit (lambda (v a)
                           (cond
                             [(null? v)
                              a]
                             [(pair? v)
                              (visit (cdr v)
                                     (cons (car v) a))]
                             [else
                              (error-case v)]))])
           (visit v_init '()))))
     xs)))

(unless (test-reverse 'other-reverse_v1i other-reverse_v1i)
  (printf "(test-reverse 'other-reverse_v1i other-reverse_v1i) failed~n"))

;;; Compute (car '(other-reverse_v1j))

(define other-reverse_v1j
  (lambda (xs)
    ((let ([error-case (let ([the-error-case 'other-reverse_v1j])
                         (if (procedure? the-error-case)
                             the-error-case
                             (lambda (v)
                               (errorf the-error-case
                                       "not a proper list: ~s"
                                       v))))])
       (lambda (v_init)
         (letrec ([visit (lambda (v a)
                           (cond
                             [(null? v)
                              a]
                             [(pair? v)
                              (visit (cdr v)
                                     (cons (car v) a))]
                             [else
                              (error-case v)]))])
           (visit v_init '()))))
     xs)))

(unless (test-reverse 'other-reverse_v1j other-reverse_v1j)
  (printf "(test-reverse 'other-reverse_v1j other-reverse_v1j) failed~n"))

;;; Unfolding of the let-expression declaring the-error-case

(define other-reverse_v1k
  (lambda (xs)
    ((let ([error-case (if (procedure? 'other-reverse_v1k)
                           the-error-case
                           (lambda (v)
                             (errorf 'other-reverse_v1k
                                     "not a proper list: ~s"
                                     v)))])
       (lambda (v_init)
         (letrec ([visit (lambda (v a)
                           (cond
                             [(null? v)
                              a]
                             [(pair? v)
                              (visit (cdr v)
                                     (cons (car v) a))]
                             [else
                              (error-case v)]))])
           (visit v_init '()))))
     xs)))

(unless (test-reverse 'other-reverse_v1k other-reverse_v1k)
  (printf "(test-reverse 'other-reverse_v1k other-reverse_v1k) failed~n"))

;;; Compute (procedure? 'other-reverse_v1k)

(define other-reverse_v1l
  (lambda (xs)
    ((let ([error-case (if #f
                           'other-reverse_v1l
                           (lambda (v)
                             (errorf 'other-reverse_v1l
                                     "not a proper list: ~s"
                                     v)))])
       (lambda (v_init)
         (letrec ([visit (lambda (v a)
                           (cond
                             [(null? v)
                              a]
                             [(pair? v)
                              (visit (cdr v)
                                     (cons (car v) a))]
                             [else
                              (error-case v)]))])
           (visit v_init '()))))
     xs)))

(unless (test-reverse 'other-reverse_v1l other-reverse_v1l)
  (printf "(test-reverse 'other-reverse_v1l other-reverse_v1l) failed~n"))

;;; Selection of the alternative

(define other-reverse_v1m
  (lambda (xs)
    ((let ([error-case (lambda (v)
                         (errorf 'other-reverse_v1m
                                 "not a proper list: ~s"
                                 v))])
       (lambda (v_init)
         (letrec ([visit (lambda (v a)
                           (cond
                             [(null? v)
                              a]
                             [(pair? v)
                              (visit (cdr v)
                                     (cons (car v) a))]
                             [else
                              (error-case v)]))])
           (visit v_init '()))))
     xs)))

(unless (test-reverse 'other-reverse_v1m other-reverse_v1m)
  (printf "(test-reverse 'other-reverse_v1m other-reverse_v1m) failed~n"))

;;; Unfolding of the let-expression declaring error-case

(define other-reverse_v1n
  (lambda (xs)
    ((lambda (v_init)
       (letrec ([visit (lambda (v a)
                         (cond
                           [(null? v)
                            a]
                           [(pair? v)
                            (visit (cdr v)
                                   (cons (car v) a))]
                           [else
                            ((lambda (v)
                               (errorf 'other-reverse_v1n
                                       "not a proper list: ~s"
                                       v))
                             v)]))])
         (visit v_init '())))
     xs)))

(unless (test-reverse 'other-reverse_v1n other-reverse_v1n)
  (printf "(test-reverse 'other-reverse_v1n other-reverse_v1n) failed~n"))

;;; Renaming of the inner (lambda (v) ...) into (lambda (w) ...)
;;; for hygiene:

(define other-reverse_v1o
  (lambda (xs)
    ((lambda (v_init)
       (letrec ([visit (lambda (v a)
                         (cond
                           [(null? v)
                            a]
                           [(pair? v)
                            (visit (cdr v)
                                   (cons (car v) a))]
                           [else
                            ((lambda (w)
                               (errorf 'other-reverse_v1o
                                       "not a proper list: ~s"
                                       w))
                             v)]))])
         (visit v_init '())))
     xs)))

(unless (test-reverse 'other-reverse_v1o other-reverse_v1o)
  (printf "(test-reverse 'other-reverse_v1o other-reverse_v1o) failed~n"))

;;; Application of (lambda (v_init) ...) to xs

(define other-reverse_v1p
  (lambda (xs)
    (letrec ([visit (lambda (v a)
                      (cond
                        [(null? v)
                         a]
                        [(pair? v)
                         (visit (cdr v)
                                (cons (car v) a))]
                        [else
                         ((lambda (w)
                            (errorf 'other-reverse_v1p
                                    "not a proper list: ~s"
                                    w))
                          v)]))])
      (visit xs '()))))

(unless (test-reverse 'other-reverse_v1p other-reverse_v1p)
  (printf "(test-reverse 'other-reverse_v1p other-reverse_v1p) failed~n"))

;;; Application of (lambda (w) ...) to v

(define other-reverse_v1
  (lambda (xs)
    (letrec ([visit (lambda (v a)
                      (cond
                        [(null? v)
                         a]
                        [(pair? v)
                         (visit (cdr v)
                                (cons (car v) a))]
                        [else
                         (errorf 'other-reverse_v1
                                 "not a proper list: ~s"
                                 v)]))])
      (visit xs '()))))

(unless (test-reverse 'other-reverse_v1 other-reverse_v1)
  (printf "(test-reverse 'other-reverse_v1 other-reverse_v1) failed~n"))

;;;;;;;;;;;;;;;;;;;;

;;; end of out-of-the-fold.scm

"out-of-the-fold.scm"

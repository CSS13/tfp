;;; The goal of this exercise is to write a structurally recursive procedure that computes the run-length encoding of a proper list of symbols.
;;; Given a proper list of symbols, the procedure segments it into a list of pairs of symbols and non-negative integers, mapping each longest sequence xs of consecutive identical symbols x into a pair (x . n) where n is the length of xs. 
;;;
;;; The procedure should pass the following unit test:

(define test-run-length
  (lambda (candidate)
    (and (equal? (candidate
                 '())
                 '())
         (equal? (candidate
                 '(a))
                 '((a . 1)))
         (equal? (candidate
                 '(a a))
                 '((a . 2)))
         (equal? (candidate
                 '(a b))
                 '((a . 1) (b . 1)))
         (equal? (candidate
                 '(a b b))
                 '((a . 1) (b . 2)))
         (equal? (candidate
                 '(a a b))
                 '((a . 2) (b . 1)))
         (equal? (candidate
                 '(a b a))
                 '((a . 1) (b . 1) (a . 1)))
         (equal? (candidate
                 '(a b a b))
                 '((a . 1) (b . 1) (a . 1) (b . 1)))
         (equal? (candidate
                 '(a b b a))
                 '((a . 1) (b . 2) (a . 1)))
         (equal? (candidate
                 '(a b b c c c))
                 '((a . 1) (b . 2) (c . 3)))
         (equal? (candidate
                 '(a b b c c c a a a a a a))
                 '((a . 1) (b . 2) (c . 3) (a . 6)))
         ;;; etc.
         )))

;;; Given something else than a proper list of symbols, the procedure should raise an error.
;;;
;;; Please
;;; * extend the unit-test procedure with your own examples
;;; * make sure your procedure is written in a structurally recursive way
;;; * write a version that uses the fold-right procedure for proper lists
;;; * write a version that is maximally lambda-dropped.

;; Naive
(define run-length_naive
  (lambda (v_init)
    (letrec ([visit (lambda (xs)
                      (cond
                        [(null? xs)
                         '()]
                        [(pair? xs)
                         (let ([ symbol_case (lambda (xs)
                                               (cons xs 1))])
                           (let ([ lhs (symbol_case (car xs))]
                                 [ rhs (visit (cdr xs))])
                             (if (and (pair? lhs)
                                      (pair? rhs)
                                      (eq? (car lhs) (car (car rhs))))
                               (cons (cons (car lhs)
                                           (+ (cdr lhs) (cdr (car rhs))))
                                     (cdr rhs))
                               (cons lhs rhs))))]
                        [else
                          (errorf 'run-length_naive
                                  "not a valid list: ˝s"
                                  xs)]))])
      (visit v_init))))

;; Accumulator
(define run-length_accum
  (lambda (v_init)
    (letrec ([visit (lambda (xs acc)
                      (cond
                        [(null? xs)
                         acc]
                        [(pair? xs)
                         (let ([ symbol_case (lambda (xs acc)
                                               (if (and (pair? acc)
                                                        (eq? xs (car (car acc))))
                                                 (cons (cons xs
                                                             (1+ (cdr (car acc))))
                                                       (cdr acc))
                                                 (cons (cons xs 1) acc)))])
                           (let ([ rhs (visit (cdr xs) acc)])
                             (symbol_case (car xs) rhs)))]
                        [else
                          (errorf 'run-length_accum
                                  "not a valid list: ˝s"
                                  xs)]))])
      (visit v_init '()))))

;; Right fold list
(define fold-right_list
  (lambda (nil-case cons-case err)
    (lambda (vs)
      (letrec ([visit (lambda (ws)
                        (cond
                          [(null? ws)
                           (nil-case)]
                          [(pair? ws)
                           (cons-case (car ws)
                                      (visit (cdr ws)))]
                          [else
                            (err ws)]))])
        (visit vs)))))

;; Right fold naive
(define run-length_naive_fold
  (lambda (xs)
    (((fold-right_list
        (lambda ()
          (lambda ()
            '()))
        (lambda (ca ys)
          (lambda ()
            (let ([ symbol_case (lambda (xs)
                                  (cons xs 1))])
              (let ([ lhs (symbol_case ca)]
                    [ rhs (ys)])
                (if (and (pair? lhs)
                         (pair? rhs)
                         (eq? (car lhs) (car (car rhs))))
                  (cons (cons (car lhs)
                              (+ (cdr lhs) (cdr (car rhs))))
                        (cdr rhs))
                  (cons lhs rhs))))))
          (lambda (xs)
            (lambda ()
              (errorf 'run-length_naive_fold
                      "not a valid list: ~s"
                      xs))))
      xs))))

;; Right fold accumulator
(define run-length_accum_fold
  (lambda (xs)
    (((fold-right_list
        (lambda ()
          (lambda (acc)
            acc))
        (lambda (ca ys)
          (lambda (acc)
            (let ([ symbol_case (lambda (xs)
                                  (lambda (acc)
                                    (if (and (pair? acc)
                                             (eq? xs (car (car acc))))
                                      (cons (cons xs
                                                  (1+ (cdr (car acc))))
                                            (cdr acc))
                                      (cons (cons xs 1) acc))))])
              (let ([ rhs (ys acc)])
                ((symbol_case ca) rhs)))))
        (lambda (xs)
          (lambda (acc)
            (errorf 'run-length_naive_fold
                    "not a valid list: ~s"
                    xs))))
      xs) '())))

;; Right fold continuation passing style
(define run-length_cps_fold
  (lambda (xs)
    (((fold-right_list
        (lambda ()
          (lambda (k)
            (k '())))
        (lambda (ca ys)
          (lambda (k)
            (let ([ symbol_case (lambda (xs k)
                                    (k (cons xs 1)))])
              (ys (lambda (rhs)
                    (symbol_case ca (lambda (lhs)
                                        (if (and (pair? lhs)
                                                 (pair? rhs)
                                                 (eq? (car lhs) (car (car rhs))))
                                          (k (cons (cons (car lhs)
                                                         (+ (cdr lhs) (cdr (car rhs))))
                                                   (cdr rhs)))
                                          (k (cons lhs rhs))))))))))
        (lambda (xs)
          (lambda (k)
            (errorf 'run-length_naive_fold
                    "not a valid list: ~s"
                    xs))))
      xs) (lambda (x) x))))

;(define run-length
;  (lambda (v_init)
;    ;(run-length_naive v_init)
;    ;(run-length_accum v_init)
;    ;(run-length_naive_fold v_init)
;    ;(run-length_accum_fold v_init)
;    (run-length_cps_fold v_init)
;))

; (run-length
;  '())
; 
; (run-length
;  '(a))
;
; (run-length
;  '(a b))
;
; (run-length
;  '(a b b))
;
; (run-length
;  '(a b a))
;
; (run-length
;  '(a b b c c c))
;
; (run-length
;  '(a b b c c c a a a a a a))
;                
; (test-run-length run-length)

(unless (test-run-length run-length_naive)
  (printf "(test-run-length run-length_naive) failed~n"))

(unless (test-run-length run-length_accum)
  (printf "(test-run-length run-length_accum) failed~n"))

(unless (test-run-length run-length_naive_fold)
  (printf "(test-run-length run-length_naive_fold) failed~n"))

(unless (test-run-length run-length_accum_fold)
  (printf "(test-run-length run-length_accum_fold) failed~n"))

(unless (test-run-length run-length_cps_fold)
  (printf "(test-run-length run-length_cps_fold) failed~n"))


;;; TODO: More unit tests
;;; TODO: Write a version that is maximally lambda-dropped.
;;; TODO: Non folded cps version

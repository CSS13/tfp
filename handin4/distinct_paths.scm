;;; This exercise is a continuation of the lecture note about binary trees.
;;;
;;; Given a binary tree, the following procedure lists the lengths of all the distinct paths from the root of the binary tree to any subtree:

;;; Please
;;;
;;; * write a unit-test procedure for lengths-of-all-distinct-paths
;;; * write a version of lengths-of-all-distinct-paths that uses an accumulator instead of append.

(define test-lengths-of-all-distinct-paths
  (lambda (candidate)
    (and ; Base case test
         (equal? (candidate
                 '())
                 '(0))
         (equal? (candidate
                 '(() . ()))
                 '(0 1 1))
         (equal? (candidate
                 '((() . ()) . ()))
                 '(0 1 2 2 1))
         (equal? (candidate
                 '(() . (() . ())))
                 '(0 1 1 2 2))
         (equal? (candidate
                 '((() . ()) . (() . ())))
                 '(0 1 2 2 1 2 2))
         (equal? (candidate
                 '(() . ((() . ()) . ())))
                 '(0 1 1 2 3 3 2))
         ;;;
         )))

(define is-leaf?
  (lambda (v)
    (null? v)))

(define is-node?
  (lambda (v)
    (pair? v)))

(define node-1
  (lambda (v)
    (car v)))

(define node-2
  (lambda (v)
    (cdr v)))

;; Naive
(define lengths-of-all-distinct-paths_naive
  (lambda (v_init)
    (letrec ([visit (lambda (v n)
                      (cons n
                            (cond
                              [(is-leaf? v)
                               '()]
                              [(is-node? v)
                               (append (visit (node-1 v) (1+ n))
                                       (visit (node-2 v) (1+ n)))]
                              [else
                               (errorf 'lengths-of-all-distinct-paths
                                       "not a binary tree: ~s"
                                       v)])))])
      (visit v_init 0))))

;; Accumulator
(define lengths-of-all-distinct-paths_accum
  (lambda (v_init)
    (letrec ([visit (lambda (v n acc)
                            (cond
                              [(is-leaf? v)
                               (cons n acc)]
                              [(is-node? v)
                               (let ([lhs (visit (node-2 v) (1+ n) acc)])
                                          (cons n (visit (node-1 v) (1+ n) lhs)))]
                              [else
                               (errorf 'lengths-of-all-distinct-paths
                                       "not a binary tree: ~s"
                                       v)]))])
      (visit v_init 0 '()))))

;; Fold
(define fold-right_binary-tree
  (lambda (lea nod err)
    (lambda (v_init)
      (letrec ([visit (lambda (v)
                        (cond
                          [(is-leaf? v)
                           (lea v)]
                          [(is-node? v)
                           (nod (visit (node-1 v))
                                (visit (node-2 v)))]
                          [else
                           (err v)]))])
        (visit v_init)))))

;; Right fold naive
(define lengths-of-all-distinct-paths_naive_fold
  (lambda (v_init)
    (((fold-right_binary-tree
        (lambda (v)
          (lambda (n)
            (list n)))
        (lambda (v1 v2)
          (lambda (n)
            (cons n
                  (append (v1 (1+ n))
                          (v2 (1+ n))))))
        (lambda (v)
          (lambda (n)
           (errorf 'lengths-of-all-distinct-paths
                               "not a binary tree: ~s"
                               v))))
        v_init) 0)))


;; Right fold accumulator
(define lengths-of-all-distinct-paths_accum_fold
  (lambda (v_init)
    (((fold-right_binary-tree
        (lambda (v)
          (lambda (n acc)
            (cons n acc)))
        (lambda (v1 v2)
          (lambda (n acc)
            (let ([lhs (v2 (1+ n) acc)])
               (cons n (v1 (1+ n) lhs)))))
        (lambda (v)
          (lambda (n acc)
            (errorf 'lengths-of-all-distinct-paths
                    "not a binary tree: ~s"
                    v))))
      v_init) 0 '())))

;; Right fold continuation passing style
(define lengths-of-all-distinct-paths_cps_fold
  (lambda (v_init)
    (((fold-right_binary-tree
        (lambda (v)
          (lambda (n k)
            (k (list n))))
        (lambda (v1 v2)
          (lambda (n k)
            (v1 (1+ n) (lambda (x)
                         (v2 (1+ n) (lambda (y)
                             (k (cons n (append x
                                                y)))))))))
        (lambda (v)
          (lambda (n k)
            (errorf 'lengths-of-all-distinct-paths
                    "not a binary tree: ~s"
                    v))))
      v_init) 0 (lambda (x) x))))

;(define lengths-of-all-distinct-paths
;  (lambda (v_init)
;    ;(lengths-of-all-distinct-paths_naive v_init)
;    ;(lengths-of-all-distinct-paths_accum v_init)
;    ;(lengths-of-all-distinct-paths_naive_fold v_init)
;    ;(lengths-of-all-distinct-paths_accum_fold v_init)
;    (lengths-of-all-distinct-paths_cps_fold v_init)
;))

;(lengths-of-all-distinct-paths
;   '())
;
;(lengths-of-all-distinct-paths
;   '(() . ()))
;
; (lengths-of-all-distinct-paths
;   '(() . ((() . ()) . ())))
; '(0 1 1 2 3 3 2))
; 
; (test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths)


(unless (test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths_naive)
  (printf "(test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths_naive) failed~n"))

(unless (test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths_accum)
  (printf "(test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths_accum) failed~n"))

(unless (test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths_naive_fold)
  (printf "(test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths_naive_fold) failed~n"))

(unless (test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths_accum_fold)
  (printf "(test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths_accum_fold) failed~n"))

(unless (test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths_cps_fold)
  (printf "(test-lengths-of-all-distinct-paths lengths-of-all-distinct-paths_cps_fold) failed~n"))

;;; TODO: More unit tests
;;; TODO: Non folded cps version

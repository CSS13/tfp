Exercise 1 -- dutch_flag.scm - dutch-flag (DF) problem.

Definition:
  - A DF has the following structure:
    - (left counter right)
    - Where:
      - left, right: are lists of integers.
      - count: is a positive integer.
  1) All elements less than the pivot are added to the left list.
  2) All elements grater than the pivot are added to the right list.
  3) All elements equal to the pivot increments the counter by one.

Inductive characterization:
  - A DF is defined for the empty list on any pivot as: (() 0 ()).
  - A DF is defined for the list of size greater than the empty list by three
    production rules based on the definitions seen above.
  - A DF is therefore inductively defined on a list with a given a pivot.
  - A DF could then be computed through structural recursion on an input list.

Invariants:
  1) All elements from the input list, not equal to the pivot, can be found in
     either the left or the right list.
  2) All elements in the left list are all lesser than the pivot.
  3) All elements in the right list are all greater than the pivot.
  4) The counter is equal to the number of elements from the input list that are
     equal to the pivot

Justify our implementations:
  - All are structurally recursive on the input list with a given pivot, and
    only input elements equal to the pivot are not added the lists, therefore
    invariant 1) is fulfilled.
    - Elements lesser than the pivot is added to the left list, thus 2).
    - Elements greater than the pivot is added to the right list, thus 3).
    - Elements equal to the pivot increments the counter by one, thus 4).

Exercise 2 -- distinct_paths.scm

lengths-of-all-distinct-paths_accum uses an accumulator and does not use append.

Exercise 3 -- run_length.scm

We couldnt see how this exercise should be written in a "maximally lambda-dropped" version,
without making it perform in squared time, which we did not think was what was meant.
(ie by using list-ref on the initial list instead of recursing on it)

Exercise 4 -- well_balanced.scm

Exercise 5 -- surprising_magritte_cont_pass.scm

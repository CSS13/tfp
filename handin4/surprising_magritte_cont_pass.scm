(load "surprising.scm")

(define interpret-arithmetic-expression_Magritte_surprising_naive
  (lambda (e_init)
    (letrec ([visit (lambda (e k)
                      (cond
                        [(is-literal? e) (k e)]
                        [(is-plus? e)
                         (k (visit
                              (plus-1 e)
                              (lambda (e1)
                                (visit
                                  (plus-2 e)
                                  (lambda (e2)
                                    (if (and (is-literal? e1)
                                             (eqv? (literal-1 e1) 0))
                                        e2
                                        (if (and (is-literal? e2)
                                                 (eqv? (literal-1 e2) 0))
                                            e1
                                            (make-plus e1 e2))))))))]
                        [(is-times? e)
                         (k (visit
                              (times-1 e)
                              (lambda (e1)
                                (if (and (is-literal? e1)
                                         (eqv? (literal-1 e1) 0))
                                    (make-literal 0)
                                    (visit
                                      (times-2 e)
                                      (lambda (e2)
                                        (cond
                                          [(and (is-literal? e2)
                                                (eqv? (literal-1 e2) 0))
                                           (make-literal 0)]
                                          [(and (is-literal? e1)
                                                (eqv? (literal-1 e1) 1))
                                           e2]
                                          [(and (is-literal? e2)
                                                (eqv? (literal-1 e2) 1))
                                           e1]
                                          [else (make-times e1 e2)])))))))]
                        [else
                         (errorf 'interpret-arithmetic-expression_Magritte_curious
                           "not a arithmetic expression: ~s"
                           e)]))])
      (visit e_init (lambda (e) e)))))

(define surprising_fold_right
  (lambda (literal plus times err)
    (lambda (e_init)
      (letrec ([visit (lambda (e)
                        (cond
                          [(is-literal? e) (literal e)]
                          [(is-plus? e)
                           (plus (visit (plus-1 e)) (visit (plus-2 e)))]
                          [(is-times? e)
                           (times (visit (times-1 e)) (visit (times-2 e)))]
                          [else (err e)]))])
        (visit e_init)))))

(define interpret-arithmetic-expression_Magritte_surprising_fold
  (lambda (e_init)
    (((surprising_fold_right
        (lambda (e)
          (lambda (k0 k1 k)
            (cond
              [(eqv? (literal-1 e) 0)
               (k0 e)]
              [(eqv? (literal-1 e) 1)
               (k1 e)]
              [else
               (k e)])))
        (lambda (xe ye)
          (lambda (k0 k1 k)
            (xe (lambda (e1)
                  (ye (lambda (e2)
                        (k0 (make-literal 0)))
                      (lambda (e2)
                        (k1 (make-literal 1)))
                      (lambda (e2)
                        (k e2))))
                (lambda (e1)
                  (ye (lambda (e2)
                        (k1 (make-literal 1)))
                      (lambda (e2)
                        ;(k (make-plus (make-literal 1) (make-literal 1))))
                        (k (make-plus e1 e2)))
                      (lambda (e2)
                        ;(k (make-plus (make-literal 1) e2)))))
                        (k (make-plus e1 e2)))))
                (lambda (e1)
                  (ye (lambda (e2)
                        (k e1))
                      (lambda (e2)
                        ;(k (make-plus e1 (make-literal 1))))
                        (k (make-plus e1 e2)))
                      (lambda (e2)
                        (k (make-plus e1 e2))))))))
        (lambda (xe ye)
          (lambda (k0 k1 k)
            (xe (lambda (e1)
                  (k0 (make-literal 0)))
                (lambda (e1)
                  (ye (lambda (e2)
                        (k0 (make-literal 0)))
                      (lambda (e2)
                        (k1 (make-literal 1)))
                      (lambda (e2)
                        (k e2))))
                (lambda (e1)
                  (ye (lambda (e2)
                        (k0 (make-literal 0)))
                      (lambda (e2)
                        (k e1))
                      (lambda (e2)
                        (k (make-times e1 e2))))))))
        (lambda (e)
          (lambda ()
            errorf
            'interpret-arithmetic-expression_Magritte_curious
            "not a arithmetic expression: ~s"
            ex)))
      e_init)
     (lambda (e) e)
     (lambda (e) e)
     (lambda (e) e)
     )))

(define interpret-arithmetic-expression_Magritte_surprising
  (lambda (e_init)
    ;(interpret-arithmetic-expression_Magritte_surprising_naive e_init)
    (interpret-arithmetic-expression_Magritte_surprising_fold e_init)
    ))
 
(define experimenter
  (lambda (candidate)
    (lambda (e)
      (unparse-arithmetic-expression
        (candidate (parse-arithmetic-expression e))))))

(define different
  (lambda (e)
    (equal?
      ((experimenter interpret-arithmetic-expression_Magritte_surprising_naive)
       e)
      ((experimenter interpret-arithmetic-expression_Magritte_surprising_fold)
       e))))

(different source-ae0)

(different source-ae1)

(different source-ae6)

; 1
((experimenter
   interpret-arithmetic-expression_Magritte_surprising)
 '(+ 1 0))

; 1
((experimenter
   interpret-arithmetic-expression_Magritte_surprising)
 '(+ 0 1))

; (+ 1 1)
((experimenter 
   interpret-arithmetic-expression_Magritte_surprising) 
 '(+ 1 1))

; 0
((experimenter 
   interpret-arithmetic-expression_Magritte_surprising) 
 '(* 0 1))

; 1
((experimenter 
   interpret-arithmetic-expression_Magritte_surprising) 
 '(* 1 1))

; 0
((experimenter 
   interpret-arithmetic-expression_Magritte_surprising) 
 '(* 1 0))

; 5
((experimenter 
   interpret-arithmetic-expression_Magritte_surprising) 
 '(* 1 5))

; 5
((experimenter 
   interpret-arithmetic-expression_Magritte_surprising) 
 '(* 5 1))

; 0
((experimenter 
   interpret-arithmetic-expression_Magritte_surprising) 
 '(* 0 5))

; 0
((experimenter 
   interpret-arithmetic-expression_Magritte_surprising) 
 '(* 5 0))

(define does_interpret-arithmetic-expression_Magritte_surprising_make_the_diagram_commute?
  (lambda (source-ae)
    (let ([ae (parse-arithmetic-expression source-ae)])
      (equal?
        (interpret-arithmetic-expression_Magritte_surprising ae)
        (compile-and-run-arithmetic-expression_Magritte_surprising
          ae)))))

(define test_does_interpret-arithmetic-expression_Magritte_surprising_make_the_diagram_commute?
  (lambda ()
    (andmap
      does_interpret-arithmetic-expression_Magritte_surprising_make_the_diagram_commute?
      sample-of-arithmetic-expressions)))

;;; Uncomment the following lines to test your implementation when loading this file:
(unless (test_does_interpret-arithmetic-expression_Magritte_surprising_make_the_diagram_commute?)
  (printf "(test_does_interpret-arithmetic-expression_Magritte_surprising_make_the_diagram_commute?) failed~n"))

;;; In plain English, which surprising program transformation is performed?
(define is_interpret-arithmetic-expression_Magritte_surprising_idempotent?
  (lambda (source-ae)
    (let* ([ae (parse-arithmetic-expression source-ae)]
           [ae_optimized (interpret-arithmetic-expression_Magritte_surprising
                           ae)])
      (equal?
        ae_optimized
        (interpret-arithmetic-expression_Magritte_surprising
          ae_optimized)))))

(define test_is_interpret-arithmetic-expression_Magritte_surprising_idempotent?
  (lambda ()
    (andmap
      is_interpret-arithmetic-expression_Magritte_surprising_idempotent?
      sample-of-arithmetic-expressions)))

;;; Uncomment the following lines to test your implementation when loading this file:
(unless (test_is_interpret-arithmetic-expression_Magritte_surprising_idempotent?)
  (printf "(test_is_interpret-arithmetic-expression_Magritte_surprising_idempotent?) failed~n"))

;;;;;;;;;;



;;; Revisit the well-balanced procedures from the lecture notes about binary trees and write it in continuation-passing style.
;;; Only apply the current continuation if the current subtree is well balanced. 
;;;     If a subtree is unbalanced, don’t apply the current continuation and return #f straight away.
;;;
;;; Your procedure should be structurally recursive, and to witness that you should also write it using the fold-right procedure for binary trees.

(define test-well-balanced?
  (lambda (candidate)
    (and (equal? (candidate
                  1)
                  #t)
         (equal? (candidate
                 '(1 . 1))
                 #t)
         (equal? (candidate
                 '(2 . (1 . 1)))
                 #t)
         (equal? (candidate
                 '(4 . (2 . (1 . 1))))
                 #t)
         (equal? (candidate
                 '(8 . (4 . (2 . (1 . 1)))))
                 #t)
         (equal? (candidate
                 '(2 . (2 . (1 . 1))))
                 #f)
         (equal? (candidate
                 '(1 . (2 . (1 . 1))))
                 #f)
         (equal? (candidate
                 '(1 . (1 . 1)))
                 #f)
         (equal? (candidate
                 '((1 . 1) . (1 . 1)))
                 #t)
             ;;;
             )))

;; Naive
(define well-balanced?_naive
  (lambda (init_v)
    (letrec ([visit (lambda (v)
                      (cond
                        [(number? v)
                         (cons (positive? v) v)]
                        [(pair? v)
                         (let ([lhs (visit (car v))])
                           (let ([rhs (visit (cdr v))])
                             (cons (and (car lhs)
                                        (car rhs)
                                        (= (cdr lhs)
                                           (cdr rhs)))
                                   (+ (cdr lhs)
                                      (cdr rhs)))))]
                        [else
                         (errorf 'well-balanced?_naive
                                 "not a proper binary tree: ~s"
                                 v)]))])
      (car (visit init_v)))))

;; Right fold binary-tree
(define fold-right_binary-tree
  (lambda (lea nod err)
    (lambda (v_init)
      (letrec ([visit (lambda (v)
                        (cond
                          [(number? v)
                           (lea v)]
                          [(pair? v)
                           (nod (visit (car v))
                                (visit (cdr v)))]
                          [else
                           (err v)]))])
        (visit v_init)))))

;; Right fold naive
(define well-balanced?_naive_fold
  (lambda (init_v)
    (car
      (((fold-right_binary-tree (lambda (n)
                                 (lambda ()
                                   (cons (positive? n) n)))
                               (lambda (r1 r2)
                                 (lambda ()
                                   (let ([lhs (r1)])
                                     (let ([rhs (r2)])
                                       (cons (and (car lhs)
                                                  (car rhs)
                                                  (= (cdr lhs)
                                                     (cdr rhs)))
                                             (+ (cdr lhs)
                                                (cdr rhs)))))))
                               (lambda (v)
                                 (lambda ()
                                   (errorf 'well-balanced?_naive_fold
                                           "not a proper binary tree: ~s"
                                           (v)))))
       init_v)))))

;; Right fold continuation passing style
(define well-balanced?_cps_fold
  (lambda (init_v)
    (((fold-right_binary-tree (lambda (n)
                                (lambda (k)
                                  (k n)))
                              (lambda (r1 r2)
                                (lambda (k)
                                  (r1 (lambda (lhs)
                                        (r2 (lambda (rhs)
                                              (if (= lhs rhs)
                                                (k (+ lhs rhs))
                                                #f)))))))
                              (lambda (v)
                                (lambda ()
                                  (errorf 'well-balanced?_cps_fold
                                          "not a proper binary tree: ~s"
                                          (v)))))
      init_v) (lambda (x) #t))))

(define well-balanced?
  (lambda (v_init)
    ;(well-balanced?_naive v_init)
    ;(well-balanced?_naive_fold v_init)
    (well-balanced?_cps_fold v_init)
))

(well-balanced? 
  1)

(well-balanced? 
  '(1 . 1))

(well-balanced? 
  '(2 . 1))

(test-well-balanced? well-balanced?)

;;; TODO: More unit tests

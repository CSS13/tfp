;;; The goal of this exercise is to write a structurally recursive procedure that solves the Dutch-flag problem for lists: given a proper list of integers xs and an integer pivot, the procedure should return a proper list of length 3:
;;;
;;; the first element of the resulting list should be a proper list containing the integers from xs that are strictly less than pivot; these integers should occur in the same order as in xs;
;;; the second element of the resulting list should be the number of integers from xs that are equal to pivot (i.e., the number of occurrences of pivot in xs); and
;;; the third element of the resulting list should be a proper list containing the integers from xs that are strictly more than pivot; these integers should occur in the same order as in xs.
;;; Given an improper list or given a list containing something else than an integer, the procedure should raise an error.
;;;
;;; The procedure should pass the following unit test:

(define test-Dutch-flag
  (lambda (candidate)
    (and ; Base case test
         (equal? (candidate '() 10)
                 '(() 0 ()))
         ; Basic placement tests
         (equal? (candidate '(1) 10)
                 '((1) 0 ()))
         (equal? (candidate '(10) 10)
                 '(() 1 ()))
         (equal? (candidate '(11) 10)
                 '(() 0 (11)))
         ; Basic list tests
         (equal? (candidate '(1 2) 10)
                 '((1 2) 0 ()))
         (equal? (candidate '(11 12) 10)
                 '(() 0 (11 12)))
         (equal? (candidate '(1 2 3) 10)
                 '((1 2 3) 0 ()))
         (equal? (candidate '(11 12 13) 10)
                 '(() 0 (11 12 13)))
         ; Reverse order list tests
         (equal? (candidate '(2 1) 10)
                 '((2 1) 0 ()))
         (equal? (candidate '(12 11) 10)
                 '(() 0 (12 11)))
         (equal? (candidate '(3 2 1) 10)
                 '((3 2 1) 0 ()))
         (equal? (candidate '(13 12 11) 10)
                 '(() 0 (13 12 11)))
         ; Mixed order list tests
         (equal? (candidate '(3 1 2) 10)
                 '((3 1 2) 0 ()))
         (equal? (candidate '(13 11 12) 10)
                 '(() 0 (13 11 12)))
         (equal? (candidate '(2 1 3) 10)
                 '((2 1 3) 0 ()))
         (equal? (candidate '(12 11 13) 10)
                 '(() 0 (12 11 13)))
         ; Duplicates tests (pivot)
         (equal? (candidate '(10 10) 10)
                 '(() 2 ()))
         (equal? (candidate '(10 10 10) 10)
                 '(() 3 ()))
         ; Duplicates tests (lists)
         (equal? (candidate '(1 1) 10)
                 '((1 1) 0 ()))
         (equal? (candidate '(11 11) 10)
                 '(() 0 (11 11)))
         (equal? (candidate '(1 1 1) 10)
                 '((1 1 1) 0 ()))
         (equal? (candidate '(11 11 11) 10)
                 '(() 0 (11 11 11)))
         ; Basic sorting tests
         (equal? (candidate '(1 11) 10)
                 '((1) 0 (11)))
         (equal? (candidate '(11 1) 10)
                 '((1) 0 (11)))
         ; Basic sorting tests + pivot
         (equal? (candidate '(10 1 11) 10)
                 '((1) 1 (11)))
         (equal? (candidate '(11 10 1) 10)
                 '((1) 1 (11)))
         (equal? (candidate '(11 1 10) 10)
                 '((1) 1 (11)))
         ; Basic sorting tests + pivots
         (equal? (candidate '(10 10 1 11) 10)
                 '((1) 2 (11)))
         (equal? (candidate '(10 1 10 11) 10)
                 '((1) 2 (11)))
         (equal? (candidate '(10 1 11 10) 10)
                 '((1) 2 (11)))
         (equal? (candidate '(10 10 11 1) 10)
                 '((1) 2 (11)))
         (equal? (candidate '(10 11 10 1) 10)
                 '((1) 2 (11)))
         (equal? (candidate '(10 11 1 10) 10)
                 '((1) 2 (11)))
         ; Big tests
         (equal? (candidate '(1 2 3 10 100 200 300) 10)
                 '((1 2 3) 1 (100 200 300)))
         (equal? (candidate '(10 1 300 3 10 2 100 10 200 1 10) 10)
                 '((1 3 2 1) 4 (300 100 200)))
         )))

;;; Please:
;;; * make sure you extend the unit-test procedure with your own tests
;;; * justify your design with invariants and/or an inductive characterization
;;; * write several versions, one of which using the fold-right procedure for proper lists.
;;;
;;; If you are tempted to use the reverse procedure, by all means yield to it.
;;; However, can you think of a structurally recursive procedure that solves the Dutch-flag problem without using reverse (and without using append either)?
;;;
;;; --> reverse is used if accumulation is done on call time, rather than at return time.

;; Naive
(define dutch-flag_naive
  (lambda (xs pivot)
    (letrec ([visit (lambda (xs pivot)
                      (cond
                        [(null? xs)
                         '(() 0 ())]
                        [(pair? xs)
                         (let ([ num_case (lambda (xs pivot)
                                            (cond
                                              [(= xs pivot)
                                               '(() 1 ())]
                                              [(> xs pivot)
                                               (list '() 0 (list xs))]
                                              [else
                                                (list (list xs) 0 '())]))])
                           (let ([ lhs (num_case (car xs) pivot)]
                                 [ rhs (visit (cdr xs) pivot)])
                             (list
                               (append (list-ref lhs 0) (list-ref rhs 0))
                               (+ (list-ref lhs 1) (list-ref rhs 1))
                               (append (list-ref lhs 2) (list-ref rhs 2)))
                             ))]
                        [else
                          (errorf 'dutch-flag
                                  "not a valid list: ˝s"
                                  xs)]))])
      (visit xs pivot))))

;; Accumulator
(define dutch-flag_accum
  (lambda (xs pivot)
    (letrec ([visit (lambda (xs pivot a_l a_n a_h)
                      (cond
                        [(null? xs)
                         (list a_l a_n a_h)]
                        [(pair? xs)
                         (let ([ num_case (lambda (xs pivot a_l a_n a_h)
                                            (cond
                                              [(= xs pivot)
                                               (list a_l (1+ a_n) a_h)]
                                              [(> xs pivot)
                                               (list a_l a_n (cons xs a_h))]
                                              [else
                                                (list (cons xs a_l) a_n a_h)]))])
                           (let ([ rhs (visit (cdr xs) pivot a_l a_n a_h)])
                             (num_case (car xs) pivot 
                                                  (list-ref rhs 0)
                                                  (list-ref rhs 1)
                                                  (list-ref rhs 2))))]
                        [else
                          (errorf 'dutch-flag
                                  "not a valid list: ˝s"
                                  xs)]))])
      (visit xs pivot '() 0 '()))))

;; Right fold list
(define fold-right_list
  (lambda (nil-case cons-case err)
    (lambda (vs)
      (letrec ([visit (lambda (ws)
                        (cond
                          [(null? ws)
                           (nil-case)]
                          [(pair? ws)
                           (cons-case (car ws)
                                      (visit (cdr ws)))]
                          [else
                            (err ws)]))])
        (visit vs)))))

;; Right fold naive
(define dutch-flag_naive_fold
  (lambda (xs pivot)
    (((fold-right_list
        (lambda ()
          (lambda ()
            '(() 0 ())))
        (lambda (ca ys)
          (lambda ()
            (let ([ num_case (lambda (xs)
              (cond
                [(= xs pivot)
                 '(() 1 ())]
                [(> xs pivot)
                 (list '() 0 (list xs))]
                [else
                 (list (list xs) 0 '())]))])
            (let ([ lhs (num_case ca)]
                  [ rhs (ys)])
              (list
                (append (list-ref lhs 0) (list-ref rhs 0))
                (+ (list-ref lhs 1) (list-ref rhs 1))
                (append (list-ref lhs 2) (list-ref rhs 2)))))))
        (lambda (xs)
          (lambda ()
            (errorf 'dutch-flag_naive_fold
                    "not a valid list: ˝s"
                    xs))))
      xs))))

;; Right fold accumulator
(define dutch-flag_accum_fold
  (lambda (xs pivot)
    (((fold-right_list
        (lambda ()
          (lambda (a_l a_n a_h)
            (list a_l a_n a_h)))
        (lambda (ca ys)
          (lambda (a_l a_n a_h)
            (let ([ num_case (lambda (xs)
                               (lambda (a_l a_n a_h)
                                 (cond
                                   [(= xs pivot)
                                    (list a_l (1+ a_n) a_h)]
                                   [(> xs pivot)
                                    (list a_l a_n (cons xs a_h))]
                                   [else
                                    (list (cons xs a_l) a_n a_h)])))])
              (let ([ rhs (ys a_l a_n a_h)])
                ((num_case ca) (list-ref rhs 0)
                               (list-ref rhs 1)
                               (list-ref rhs 2))))))
        (lambda (xs)
          (lambda ()
            (errorf 'dutch-flag_naive_fold
                    "not a valid list: ˝s"
                    xs))))
      xs) '() 0 '())))

;; Proper list
(define proper-list-of-given-length?
  (lambda (v n)
    (or (and (null? v)
             (= n 0))
        (and (pair? v)
             (> n 0)
             (proper-list-of-given-length? (cdr v)
                                           (- n 1))))))

;; Right fold continution passing style
(define dutch-flag_cps_fold
  (lambda (xs pivot)
    (((fold-right_list
        (lambda ()
          (lambda (k)
            (k '() 0 '())))
         (lambda (ca ys)
           (lambda (k)
             (let ([ num_case (lambda (xs)
                                (lambda (k)
                                  (cond
                                    [(= xs pivot)
                                     (k '() 1 '())]
                                    [(> xs pivot)
                                     (k '() 0 (list xs))]
                                    [else
                                     (k (list xs) 0 '())])))])
               (ys (lambda (ya_l ya_n ya_h)
                     ((num_case ca) (lambda (xa_l xa_n xa_h)
                           ; xa_l and xa_h are either '() or '(x) --> (size 1)
                           ;(if (not (null? xa_l))
                           ;  (assert (proper-list-of-given-length? xa_l 1)))
                           ;(if (not (null? xa_h))
                           ;  (assert (proper-list-of-given-length? xa_h 1)))
                           (k (append xa_l ya_l) (+ ya_n xa_n) (append xa_h ya_h)))))))))
        (lambda (xs)
          (lambda ()
            (errorf 'dutch-flag_naive_fold
                    "not a valid list: ˝s"
                    xs))))
      xs) (lambda (a_l a_n a_h) (list a_l a_n a_h)))))

;(define dutch-flag
;  (lambda (xs pivot)
;    ;(dutch-flag_naive xs pivot)
;    ;(dutch-flag_accum xs pivot)
;    ;(dutch-flag_naive_fold xs pivot)
;    ;(dutch-flag_accum_fold xs pivot)
;    (dutch-flag_cps_fold xs pivot)
;))

;; (dutch-flag '() 10)
;;
;; (dutch-flag '(1) 10)
;;
;; (dutch-flag '(10) 10)
;;
;; (dutch-flag '(20) 10)
;;
;; (dutch-flag '(1 2) 10)
;;
;; (dutch-flag '(10 1 300 3 10 2 100 10 200 1 10) 10)
;;
;;;; '((1 3 2 1) 4 (300 100 200)))

; (test-Dutch-flag dutch-flag)

(unless (test-Dutch-flag dutch-flag_naive)
  (printf "(test-Dutch-flag dutch-flag_naive) failed~n"))

(unless (test-Dutch-flag dutch-flag_accum)
  (printf "(test-Dutch-flag dutch-flag_accum) failed~n"))

(unless (test-Dutch-flag dutch-flag_naive_fold)
  (printf "(test-Dutch-flag dutch-flag_naive_fold) failed~n"))

(unless (test-Dutch-flag dutch-flag_accum_fold)
  (printf "(test-Dutch-flag dutch-flag_accum_fold) failed~n"))

(unless (test-Dutch-flag dutch-flag_cps_fold)
  (printf "(test-Dutch-flag dutch-flag_cps_fold) failed~n"))


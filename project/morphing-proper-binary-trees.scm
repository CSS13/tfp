;;; morphing-proper-binary-trees.scm
;;; TFP 2015-2016, Q3
;;; Olivier Danvy <danvy@cs.au.dk>
;;; Version of 14 Mar 2016

;;; Accompanying material for the lecture note at
;;;   http://users-cs.au.dk/danvy/TFP/Lecture-notes/possible-term-projects.html

;;;;;;;;;;;;;;;;;;;;

;;; BNF of proper binary trees and associated fold-right procedure:

;;; <proper-binary-tree> ::= <number>
;;;                        | (<proper-binary-tree> . ())
;;;                        | (<proper-binary-tree> . <proper-binary-tree>)

(define fold-right_proper-binary-tree
  (lambda (case-numb case-last case-pair case-else)
    (trace-lambda init (v_init)
      (letrec ([visit
                (trace-lambda step(v)
                  (cond
                    [(number? v)
                     (case-numb v)]
                    [(pair? v)
                     (let ([v1 (car v)]
                           [v2 (cdr v)])
                       (if (null? v2)
                           (case-last (visit v1))
                           (case-pair (visit v1) (visit v2))))]
                    [else
                     (case-else v)]))])
        (visit v_init)))))

(define check-proper-binary-tree
  (fold-right_proper-binary-tree
   number?
   (lambda (b) b)
   (lambda (b1 b2)
     (and b1 b2))
   (lambda (v) #f)))

;; **********************
;; ** PART 1 : Morpher **
;; **********************

(define test-proper-binary-tree-morpher
  (lambda (candidate)
    (and (equal? ((candidate '3) 4)
                 '4)
         (equal? ((candidate '(1 (2) 3)) 4)
                 '(4 (4) 4))
         (equal? ((candidate '(3 . 3)) 5)
                 '(5 . 5))
         (equal? ((candidate '((1 . 1) . (2 . 2))) 5)
                 '((5 . 5) . (5 . 5)))
         (equal? ((candidate '((1 . 0) . 0)) 1)
                 '((1 . 1) . 1))
         (equal? ((candidate '(0 . (1 . 0))) 1)
                 '(1 . (1 . 1)))
         ;;; etc.
         )))
;;
;; 2n runnig time
;;
;; Linear on input with right-fold,
;; created function has identical size to input.
;; 
(define proper-binary-tree-morpher_v1
  (lambda (bt)
    ((fold-right_proper-binary-tree
      (lambda (v)
        (lambda (n)
          n))
      (lambda (L)
        (lambda (n)
          (cons (L n) '())))
      (lambda (L R)
        (lambda (n)
          (cons (L n) (R n))))
      (lambda (v)
        (errorf 'proper-binary-tree-morpher_v1
                "not a proper binary tree: ~s"
                v)))
     bt)))

(unless (test-proper-binary-tree-morpher proper-binary-tree-morpher_v1)
   (printf "(test-proper-binary-tree-morpher proper-binary-tree-morpher_v1) failed~n"))

;;
;; n running time
;;
;; Linear on input with right-fold.
;;
(define proper-binary-tree-morpher_v2
  (lambda (bt)
    (lambda (n)
      ((fold-right_proper-binary-tree
         (lambda (b)
           n)
         (lambda (L)
           (cons L '()))
         (lambda (L R) 
           (cons L R))
         (lambda (s)
           (errorf 'proper-binary-tree-morpher_v2
                   "Not a proper binary tree. ~s" 
                   s)))
       bt))))

(unless (test-proper-binary-tree-morpher proper-binary-tree-morpher_v2)
   (printf "(test-proper-binary-tree-morpher proper-binary-tree-morpher_v2) failed~n"))

;; *************************
;; ** part 2 : Magrittify **
;; *************************
;;
;; n running time
;;
;; Linear on input with right-fold
;;
(define magritte_proper-binary-tree-morpher_v2
  (lambda (bt)
    (lambda (n)
      ((fold-right_proper-binary-tree
         (lambda (b)
           n)
         (lambda (L)
           `(cons ,L '()))
         (lambda (L R) 
           `(cons ,L ,R))
         (lambda (s)
           (errorf 'proper-binary-tree-morpher_v2
                   "Not a proper binary tree. ~s" 
                   s)))
       bt))))

;; Example:
;;   ((magritte_proper-binary-tree-morpher_v2 '((1 . 2) (3) . 4)) 2)
;;     => (cons (cons 2 2) (cons (cons 2 '()) 2))
;;
;;   (eval ((magritte_proper-binary-tree-morpher_v2 '((1 . 2) (3) . 4)) 2))
;;     => ((2 . 2) (2) . 2)

;; **************************
;; ** PART 3 : Min morpher **
;; **************************

(define test-proper-binary-tree-minmorpher
  (lambda (candidate)
    (and (equal? (candidate '3)
                 '3)
         (equal? (candidate '(1 (2) 3))
                 '(1 (1) 1))
         (equal? (candidate '(0 . 5))
                 '(0 . 0))
         (equal? (candidate '((1 . 0) . 0))
                 '((0 . 0) . 0))
         (equal? (candidate '(0 . (1 . 0)))
                 '(0 . (0 . 0)))
         (equal? (candidate '((0 . 2) . (1 . 2)))
                 '((0 . 0) . (0 . 0)))
         ;;; etc.
         )))

;;
;; 2n running time.
;;
;; Linear on input with right-fold,
;; created function has identical size to input
;;
(define proper-binary-tree-minmorpher-eval
  (lambda (bt)
    (let ([result
            ((fold-right_proper-binary-tree
               (lambda (b)
                 (cons b 'x))
               (lambda (last)
                 (cons (car last)
                       `(cons ,(cdr last) '())))
               (lambda (left right)
                 (cons (min (car left) 
                            (car right)) 
                       `(cons ,(cdr left) 
                              ,(cdr right))))
               (lambda (s)
                 `(errorf 'proper-binary-tree-morpher
                          "Not a proper binary tree. ~s"
                          s)))
             bt)])
      (eval `((lambda (x) ,(cdr result))
        ,(car result))))))

;;
;; 2n running time
;; 
;; Linear on input with right-fold,
;; created function has identical size to input
;;
(define proper-binary-tree-minmorpher_v1
  (lambda (bt)
    (let ([result ((fold-right_proper-binary-tree
                    (lambda (v)
                      (cons v (lambda (n) n)))
                    (lambda (L)
                      (let ([p (cdr L)])
                        (cons (car L)
                              (lambda (n)
                                (cons (p n) '())))))
                    (lambda (L R)
                      (let ([pl (cdr L)]
                            [pr (cdr R)])
                        (cons (min (car L)
                                   (car R))
                              (lambda (n)
                                (cons (pl n)
                                      (pr n))))))
                    (lambda (v)
                      (errorf 'proper-binary-tree-minmorpher_v1
                              "not a proper binary tree: ~s"
                              v)))
                   bt)])
      ((cdr result) (car result)))))

(unless (test-proper-binary-tree-minmorpher proper-binary-tree-minmorpher_v1)
  (printf "(test-proper-binary-tree-minmorpher proper-binary-tree-minmorpher_v1) failed~n"))

;;
;; 3n running time
;;
;; Linear on input with right-fold where structure is built
;; Linear pass to find smallest number
;; created function has identical size to input
;;
(define proper-binary-tree-minmorpher_v2
  (lambda (bt)
    (((fold-right_proper-binary-tree
       (lambda (v)
         (lambda (k)
           (k v (lambda (n) n))))
       (lambda (L)
         (lambda (k)
           (L (lambda (nl l)
                (k nl
                   (lambda (n)
                     (cons (l n) '())))))))
       (lambda (L R)
         (lambda (k)
           (L (lambda (nl l)
                (R (lambda (nr r)
                     (k (min nl nr)
                        (lambda (n)
                          (cons (l n) (r n))))))))))
       (lambda (v)
         (errorf 'proper-binary-tree-minmorpher_v2
                 "not a proper binary tree: ~s"
                 v)))
      bt)
     (lambda (n f)
       (f n)))))

(unless (test-proper-binary-tree-minmorpher proper-binary-tree-minmorpher_v2)
  (printf "(test-proper-binary-tree-minmorpher proper-binary-tree-minmorpher_v2) failed~n"))

;; **************************
;; ** PART 4 : Max morpher **
;; **************************
(define test-proper-binary-tree-maxmorpher
  (lambda (candidate)
    (and (equal? (candidate '3)
                 '3)
         (equal? (candidate '(1 (2) 3))
                 '(3 (3) 3))
         (equal? (candidate '((1 . 0) . 0))
                 '((1 . 1) . 1))
         (equal? (candidate '(0 . (1 . 0)))
                 '(1 . (1 . 1)))
         (equal? (candidate '(1 . 5))
                 '(5 . 5))
         (equal? (candidate '((3 . 2) . (1 . 5)))
                 '((5 . 5) . (5 . 5)))
         ;;; etc.
         )))

;;
;; 2n running time.
;;
;; Linear on input with right-fold,
;; created function has identical size to input
;;
(define proper-binary-tree-maxmorpher-eval
  (lambda (bt)
    (let ([result
            ((fold-right_proper-binary-tree
               (lambda (b)
                 (cons b 'x))
               (lambda (last)
                 (cons (car last)
                       `(cons ,(cdr last) '())))
               (lambda (left right)
                 (cons (max (car left) 
                            (car right)) 
                       `(cons ,(cdr left) 
                              ,(cdr right))))
               (lambda (s)
                 `(errorf 'proper-binary-tree-morpher
                          "Not a proper binary tree. ~s"
                          s)))
             bt)])
      (eval `((lambda (x) ,(cdr result))
        ,(car result))))))

;;
;; 2n running time
;; 
;; Linear on input with right-fold,
;; created function has identical size to input
;;
(define proper-binary-tree-maxmorpher_v1
  (lambda (bt)
	(let ([result ((fold-right_proper-binary-tree
					 (lambda (v)
					   (cons v (lambda (n) n)))
					 (lambda (L)
					   (let ([p (cdr L)])
						 (cons (car L)
							   (lambda (n)
								 (cons (p n) '())))))
					 (lambda (L R)
					   (let ([pl (cdr L)]
							 [pr (cdr R)])
						 (cons (max (car L)
									(car R))
							   (lambda (n)
								 (cons (pl n)
									   (pr n))))))
					 (lambda (v)
					   (errorf 'proper-binary-tree-minmorpher_v1
							   "not a proper binary tree: ~s"
							   v)))
				   bt)])
	  ((cdr result) (car result)))))

(unless (test-proper-binary-tree-maxmorpher proper-binary-tree-maxmorpher_v1)
  (printf "(test-proper-binary-tree-maxmorpher proper-binary-tree-maxmorpher_v1) failed~n"))

;;
;; 3n running time
;;
;; Linear on input with right-fold where structure is built
;; Linear pass to find smallest number
;; created function has identical size to input
;;
(define proper-binary-tree-maxmorpher_v2
  (lambda (bt)
    (((fold-right_proper-binary-tree
       (lambda (v)
         (lambda (k)
           (k v (lambda (n) n))))
       (lambda (L)
         (lambda (k)
           (L (lambda (nl l)
                (k nl
                   (lambda (n)
                     (cons (l n) '())))))))
       (lambda (L R)
         (lambda (k)
           (L (lambda (nl l)
                (R (lambda (nr r)
                     (k (max nl nr)
                        (lambda (n)
                          (cons (l n) (r n))))))))))
       (lambda (v)
         (errorf 'proper-binary-tree-maxmorpher_v2
                 "not a proper binary tree: ~s"
                 v)))
      bt)
     (lambda (n f)
       (f n)))))

(unless (test-proper-binary-tree-maxmorpher proper-binary-tree-maxmorpher_v2)
  (printf "(test-proper-binary-tree-maxmorpher proper-binary-tree-maxmorpher_v2) failed~n"))

;; **********************
;; ** COMBINED 1, 3, 4 **
;; **********************

(define proper-binary-tree-f-morpher
  (lambda (morph)
    (lambda (bt)
      (((fold-right_proper-binary-tree
         (lambda (v)
           (lambda (k)
             (k v (lambda (n) n))))
         (lambda (L)
           (lambda (k)
             (L (lambda (nl l)
                  (k nl
                     (lambda (n)
                       (cons (l n) '())))))))
         (lambda (L R)
           (lambda (k)
             (L (lambda (nl l)
                  (R (lambda (nr r)
                       (k (morph nl nr)
                          (lambda (n)
                            (cons (l n) (r n))))))))))
         (lambda (v)
           (errorf 'proper-binary-tree-f-morpher
                   "not a proper binary tree: ~s"
                   v)))
        bt)
       (lambda (n f)
         (f n))))))

;; > ((proper-binary-tree-f-morpher (lambda (l r) 6)) '(5 . 3))
;; (6 . 6)
;; > ((proper-binary-tree-f-morpher min) '(5 . 3))
;; (3 . 3)
;; > ((proper-binary-tree-f-morpher max) '(5 . 3))
;; (5 . 5)

;; **********************************
;; ** PART 5 : Depth first morpher **
;; **********************************

(define test-proper-binary-tree-depth-first-morpher
  (lambda (candidate)
    (and (equal? (candidate '3)
                '0)
         (equal? (candidate '(((100))))
                 '(((0))))
         (equal? (candidate '(10 (2 -5) 3))
                 '(0 (1 2) 3))
         (equal? (candidate '(1 . 2))
                 '(0 . 1))
         (equal? (candidate '(2 . (4 . 5)))
                 '(0 . (1 . 2)))
         (equal? (candidate '((3 . 3) . 3))
                 '((0 . 1) . 2))
         (equal? (candidate '((4 . 3) . ( 24 . 4)))
                 '((0 . 1) . (2 . 3)))
         ;;; etc.
         )))

;; 
;; 2n running time
;; 
;; Linear on input with fold-right
;; created function has identical size to input
;; 
(define proper-binary-tree-depth-first-morpher_v1
  (lambda (bt)
    (((fold-right_proper-binary-tree
       (lambda (b)
         (lambda (n k)
           (k (1+ n) n)))
       (lambda (L)
         (lambda (n k)
           (L n (lambda (nl l)
                  (k nl (cons l '()))))))
       (lambda (L R)
         (lambda (n k)
           (L n (lambda (nl l) 
                  (R nl (lambda (nr r)
                          (k nr (cons l r))))))))
       (lambda (s)
         (errorf 'proper-binary-tree-depth-first-morpher_v1
                 "not a proper binary tree: ~s" 
                 s)))
      bt)
     0 (lambda (n v) v))))

(unless (test-proper-binary-tree-depth-first-morpher proper-binary-tree-depth-first-morpher_v1)
  (printf "(test-proper-binary-tree-depth-first-morpher proper-binary-tree-depth-first-morpher_v1) failed~n"))

;; ************************************
;; ** PART 6 : Breadth first morpher **
;; ************************************

(define test-proper-binary-tree-breadth-first-morpher
  (lambda (candidate)
    (and (equal? (candidate '3)
                 '0)
         (equal? (candidate '(((100))))
                 '(((0))))
         (equal? (candidate '(10 (2 -5) 3))
                 '(0 (1 3) 2))

;;; (10 (2 -5) 3))
;;; is constructed as (cons 10 (cons 2 (cons -5 '())) (cons 3 '()))
;;; and represented as

;;; +-----+   +-----+             +-----+
;;; |  | -|-->|  | -|------------>|  | /|
;;; +--+--+   +--+--+             +--+--+
;;;   |         |                   |
;;;   v         v                   v
;;;  10       +-----+   +-----+     3
;;;           |  | -|-->|  | /|
;;;           +--+--+   +--+--+
;;;             |         |
;;;             v         v
;;;             2        -5

;;; i.e.,

;;;    *
;;;   / \
;;; 10   *
;;;     / \
;;;    /   \
;;;   *     *
;;;  / \   / \
;;; 2   * 3  ()
;;;    / \
;;;  -5  ()

;;; which is morphed breadth-first into

;;;    *
;;;   / \
;;;  0   *
;;;     / \
;;;    /   \
;;;   *     *
;;;  / \   / \
;;; 1   * 2  ()
;;;    / \
;;;   3  ()

;;; i.e.,

;;; +-----+   +-----+             +-----+
;;; |  | -|-->|  | -|------------>|  | /|
;;; +--+--+   +--+--+             +--+--+
;;;   |         |                   |
;;;   v         v                   v
;;;   0       +-----+   +-----+     2
;;;           |  | -|-->|  | /|
;;;           +--+--+   +--+--+
;;;             |         |
;;;             v         v
;;;             1         3

;;; i.e.,

;;; (0 (1 3) 2)

         (equal? (candidate '(((((10)))) (2 -5) 3))
                 '(((((3)))) (0 2) 1))
         (equal? (candidate '(3 . 3))
                 '(0 . 1))
         (equal? (candidate '(3 . (3 . 3)))
                 '(0 . (1 . 2)))
         (equal? (candidate '((3 . 3) . 3))
                 '((1 . 2) . 0))
         (equal? (candidate '((3 . 3) . (3 . 3)))
                 '((0 . 1) . (2 . 3)))
         ;;; etc.
         )))
;;
;; O(min(xs ys))*O(p)
;; where p is the given function
;;
(define zip-with
  (lambda (p xs_init ys_init)
    (if (list? xs_init)
        (if (list? ys_init)
            (letrec ([visit
                      (lambda (xs ys)
                        (cond
                          [(null? xs)
                           ys]
                          [(null? ys)
                           xs]
                          [else
                           (cons (p (car xs)
                                    (car ys))
                                 (visit (cdr xs)
                                        (cdr ys)))]))])
              (visit xs_init ys_init))
            (errorf 'zip-with
                    "not a proper list: ~s"
                    ys_init))
        (errorf 'zip-with
                "not a proper list: ~s"
                xs_init))))

;;
;; O(n*(n-1)/2) = O(n^2)
;;
;; Recurses over the input list and
;; calls zip-with with append at each internal node.
;;
(define levels_proper-binary-tree
  (fold-right_proper-binary-tree
   (lambda (v)
     `((,v)))
   (lambda (l)
     (cons '(l) l))
   (lambda (l r)
     (cons '(p) (zip-with append l r)))
   (lambda (v)
     (errorf 'levels_proper-binary-tree
             "not a proper binary tree: ~s"
             v))))

; From week 5: out-of-the-fold.scm
(define fold-right_proper-list
  (lambda (nil-case cons-case . rest)
    (let ([error-case (cond
                        [(null? rest)
                         (lambda (v)
                           (errorf 'fold-right_proper-list
                                   "not a proper list: ~s"
                                   v))]
                        [(null? (cdr rest))
                         (let ([the-error-case (car rest)])
                           (if (procedure? the-error-case)
                               the-error-case
                               (lambda (v)
                                 (errorf the-error-case
                                         "not a proper list: ~s"
                                         v))))]
                        [else
                         (errorf 'fold-right_proper-list
                                 "improper actual parameters: ~s"
                                 rest)])])
      (lambda (v_init)
        (letrec ([visit (lambda (v)
                          (cond
                            [(null? v)
                             nil-case]
                            [(pair? v)
                             (cons-case (car v)
                                        (visit (cdr v)))]
                            [else
                             (error-case v)]))])
          (visit v_init))))))

;;
;; n*m running time
;;
;; Linear in n with fold-right
;; Linear in internal list elements (m) with fold-right
;;
(define relabel-levels
  (lambda (lvls_init)
    (let ([relabel-level
           (lambda (n lvl k)
             (((fold-right_proper-list
                (lambda (n k)
                  (k n '()))
                (lambda (v vs)
                  (lambda (n k)
                    (if (number? v)
                        (vs (1+ n) (lambda (_n _vs)
                                     (k _n (cons n _vs))))
                        (vs n (lambda (_n _vs)
                                (k _n (cons v _vs)))))))
                'relabel-level)
               lvl)
              n k))])
      (((fold-right_proper-list
         (lambda (n k)
           (k n '()))
         (lambda (lvl lvls)
           (lambda (n1 k)
             (relabel-level n1 lvl
                            (lambda (n2 _lvl)
                              (lvls n2 (lambda (n3 _lvls)
                                         (k n3 (cons _lvl _lvls))))))))
         'relabel-levels)
        lvls_init)
       0 (lambda (n lvls) lvls)))))

;;
;; 2n runnign time
;;
;; each element will at most be added to queue once,
;; and removed from the queue once.
;;
(define rebuild_proper-binary-tree
  (lambda (lvls_init)
    (let ([merge-levels
           (lambda (cur pre)
             (((fold-right_proper-list
                (lambda (q1)
                  '())
                (lambda (v vs)
                  (lambda (q1)
                    (if (symbol? v)
                        (if (equal? v 'l)
                            (let ([l (car q1)]
                                  [q2 (cdr q1)])
                              (cons (cons l '()) (vs q2)))
                            (let ([l (car q1)]
                                  [r (cadr q1)]
                                  [q2 (cddr q1)])
                              (cons (cons l r) (vs q2))))
                        (cons v (vs q1)))))
                'merge-levels)
               cur)
              pre))])
      (((fold-right_proper-list
         (lambda (k)
           (k '()))
         (lambda (lvl lvls)
           (lambda (k)
             (lvls (lambda (_lvls)
                     (k (merge-levels lvl _lvls))))))
         'rebuild_proper-binary-tree)
        lvls_init)
       (lambda (lvls)
         (car lvls))))))

;;
;; O(n^2)
;;
;; Due to levels_proper-binary-tree.
;;
(define proper-binary-tree-breadth-first-morpher_v1
  (lambda (bt)
    (rebuild_proper-binary-tree
     (relabel-levels
      (levels_proper-binary-tree bt)))))

(unless (test-proper-binary-tree-breadth-first-morpher proper-binary-tree-breadth-first-morpher_v1)
  (printf "(test-proper-binary-tree-breadth-first-morpher proper-binary-tree-breadth-first-morpher_v1) failed~n"))

;;
;; 3n+d running time
;; where d is the depth of the tree
;;
;; Linear time in recursing over the tree
;; Linear time in reserve; each node reserved at most once
;; Linear time in removing from queue.
;; created function has size of bt depth
;;
(define breadth-first-traversal_proper-binary-tree
  (lambda (v_init)
    (letrec ([visit
              (lambda (fq bq n k)
                (if (null? bq)
                    (if (null? fq)
                        (k '(()))
                        (visit '() (reverse fq) n
                               (lambda (vs)
                                 (k (cons '() vs)))))
                    (let ([v (car bq)]
                          [_bq (cdr bq)])
                      (cond
                        [(number? v)
                         (visit fq _bq (1+ n)
                                (lambda (vs)
                                  (k (cons (cons n (car vs)) (cdr vs)))))]
                        [(pair? v)
                         (let ([v1 (car v)]
                               [v2 (cdr v)])
                           (if (null? v2)
                               (visit (cons v1 fq) _bq n
                                      (lambda (vs)
                                        (k (cons (cons 'l (car vs)) (cdr vs)))))
                               (visit (cons v2 (cons v1 fq)) _bq n
                                      (lambda (vs)
                                        (k (cons (cons 'p (car vs)) (cdr vs)))))))]
                        [else
                         (errorf 'breadth-first-traversal_proper-binary-tree
                                 "not a proper binary tree: ~s"
                                 v)]))))])
      (visit '() (list v_init) 0 (lambda (vs) vs)))))

(define depth-first-traversal_proper-binary-tree
  (lambda (v_init)
    (letrec ([visit
              (lambda (fq bq n k)
                (if (null? bq)
                    (if (null? fq)
                        (k '(()))
                        (visit '() (reverse fq) n
                               (lambda (vs)
                                 (k (cons '() vs)))))
                    (let ([v (car bq)]
                          [_bq (cdr bq)])
                      (cond
                        [(number? v)
                         (visit fq _bq (1+ n)
                                (lambda (vs)
                                  (k (cons (cons n (car vs)) (cdr vs)))))]
                        [(pair? v)
                         (let ([v1 (car v)]
                               [v2 (cdr v)])
                           (if (null? v2)
                               (visit fq (cons v1 _bq) n
                                      (lambda (vs)
                                        (k (cons (cons 'l (car vs)) (cdr vs)))))
                               (visit fq (cons v1 (cons v2 _bq)) n
                                      (lambda (vs)
                                        (k (cons (cons 'p (car vs)) (cdr vs)))))))]
                        [else
                         (errorf 'breadth-first-traversal_proper-binary-tree
                                 "not a proper binary tree: ~s"
                                 v)]))))])
      (visit '() (list v_init) 0 (lambda (vs) vs)))))

(depth-first-traversal_proper-binary-tree '((0 . 1) . (2 . 3)))


;;
;; O(n)
;; 
;; 5n+d due to used functions.
;;
(define proper-binary-tree-breadth-first-morpher_v2
  (lambda (bt)
    (rebuild_proper-binary-tree
     (breadth-first-traversal_proper-binary-tree bt))))

(unless (test-proper-binary-tree-breadth-first-morpher proper-binary-tree-breadth-first-morpher_v2)
  (printf "(test-proper-binary-tree-breadth-first-morpher proper-binary-tree-breadth-first-morpher_v2) failed~n"))

; An attempt at bfs folding
; end-width when changing layers (next row of nodes)
(define fold-bfs_proper-binary-tree
  (lambda (null-case end-width case-numb case-last case-pair case-else)
    (lambda (v_init)
      (letrec ([visit (lambda (fq bq)
                        (if (null? bq)
                          (if (null? fq)
                            (null-case)
                            (end-width (visit '() (reverse fq))))
                          (let ([v (car bq)]
                                [_bq (cdr bq)])
                            (cond
                              [(number? v)
                               (case-numb v (visit fq _bq))]
                              [(pair? v)
                               (let ([v1 (car v)]
                                     [v2 (cdr v)])
                                 (if (null? v2)
                                   (case-last v (visit (cons v1 fq) _bq))
                                   (case-pair v (visit (cons v2 (cons v1 fq)) _bq))))]
                              [else
                                (case-else v)]))))])
        (visit '() (list v_init))))))

(define breadth-first-traversal_proper-binary-tree_fold
  (lambda (bt)
    (((fold-bfs_proper-binary-tree
        ; null-case
        (lambda ()
          (lambda (n k)
            (k '(()))))
        ; end-width
        (lambda (N)
          (lambda (n k)
            (N n (lambda (vs)
                 (k (cons '() vs))))))
        ; case-numb (i.e. leaf-case)
        (lambda (v N)
          (lambda (n k)
            (N (1+ n) (lambda (vs)
                 (k (cons (cons n (car vs)) (cdr vs)))))))
        ; case-last
        (lambda (v N)
          (lambda (n k)
            (N n (lambda (vs)
                   (k (cons (cons 'l (car vs)) (cdr vs)))))))
        ; case-pair
        (lambda (v N)
          (lambda (n k)
            (N n (lambda (vs)
                   (k (cons (cons 'p (car vs)) (cdr vs)))))))
        ; case-else
        (lambda (s)
          (errorf 'breadth-first-traversal_proper-binary-tree
                  "not a proper binary tree: ~s" 
                  s)))
      bt)
     0 (lambda (vs) vs))))

;;
;; O(n)
;; 
;; 5n+d due to used functions.
;;
(define proper-binary-tree-breadth-first-morpher_v3
  (lambda (bt)
    (rebuild_proper-binary-tree
     (breadth-first-traversal_proper-binary-tree_fold bt))))

(unless (test-proper-binary-tree-breadth-first-morpher proper-binary-tree-breadth-first-morpher_v3)
  (printf "(test-proper-binary-tree-breadth-first-morpher proper-binary-tree-breadth-first-morpher_v3) failed~n"))


;;;;;;;;;;;;;;;;;;;;

;;; end of morphing-proper-binary-trees.scm

"morphing-proper-binary-trees.scm"

;;; <proper-binary-tree> ::= <number>
;;;                        | (<proper-binary-tree> . ())
;;;                        | (<proper-binary-tree> . <proper-binary-tree>)


;; **********************
;; ** Helper functions **
;; **********************

(define bt-fold-right-proper
  (lambda (num-case pair-case error-case)
    (lambda (bt-init)
      (letrec ([visit (lambda (bt)
                        (cond
                          [(number? bt)
                           (num-case bt)]
                          [(pair? bt)
                           (pair-case (visit (car bt))
                                      (visit (cdr bt)))]
                          [else
                            (error-case bt)]))])
        (visit bt-init)))))

(define bt-fold-right
  (lambda (case-numb case-last case-pair case-else)
    (lambda (v_init)
      (letrec ([visit
                (lambda (v)
                  (cond
                    [(number? v)
                     (case-numb v)]
                    [(pair? v)
                     (let ([v1 (car v)]
                           [v2 (cdr v)])
                       (if (null? v2)
                           (case-last (visit v1))
                           (case-pair (visit v1) (visit v2))))]
                    [else
                     (case-else v)]))])
        (visit v_init)))))

;; ************
;; ** PART 1 **
;; ************

(define proper-binary-tree-morpher
  (lambda (bt)
    (lambda (x)
      ((bt-fold-right 
         (lambda (b) 
           x)
         (lambda (last)
           (cons last '()))
         (lambda (left right) 
           (cons left right))
         (lambda (s)
           (errorf 'proper-binary-tree-morpher 
                   "Not a proper binary tree. ~s" 
                   s)))
       bt))))


;; ************
;; ** PART 2 **
;; ************

(define proper-binary-tree-morpher-magrittify
  (lambda (bt)
    (lambda (x)
      ((bt-fold-right
         (lambda (b)
           x)
         (lambda (last)
           `(cons ,last '()))
         (lambda (left right)
           `(cons ,left ,right))
         
         (lambda (s)
           `(errorf 'proper-binary-tree-morpher
                    "Not a proper binary tree. ~s"
                    s)))
       bt))))

;; ************
;; ** PART 3 **
;; ************

(define proper-binary-tree-minmorpher-eval
  (lambda (bt)
    (let ([result
            ((bt-fold-right
               (lambda (b)
                 (cons b 'x))
               (lambda (last)
                 (cons (car last)
                       `(cons ,(cdr last) '())))
               (lambda (left right)
                 (cons (min (car left) 
                            (car right)) 
                       `(cons ,(cdr left) 
                              ,(cdr right))))
               (lambda (s)
                 `(errorf 'proper-binary-tree-morpher
                          "Not a proper binary tree. ~s"
                          s)))
             bt)])
      (eval `((lambda (x) ,(cdr result))
        ,(car result))))))

; TODO: make less ugly variant

;; ************
;; ** PART 4 **
;; ************

(define proper-binary-tree-maxmorpher-eval
  (lambda (bt)
    (let ([result
            ((bt-fold-right
               (lambda (b)
                 (cons b 'x))
               (lambda (last)
                 (cons (car last)
                       `(cons ,(cdr last) '())))
               (lambda (left right)
                 (cons (max (car left) 
                            (car right)) 
                       `(cons ,(cdr left) 
                              ,(cdr right))))
               (lambda (s)
                 `(errorf 'proper-binary-tree-morpher
                          "Not a proper binary tree. ~s"
                          s)))
             bt)])
      (eval `((lambda (x) ,(cdr result))
        ,(car result))))))

;; ************
;; ** PART 5 **
;; ************

(define proper-binary-tree-depth-first-morpher
  (lambda (bt)
    (((bt-fold-right 
        (lambda (b)
          (lambda (n k)
            (k n (1+ n))))
        (lambda (last)
          (lambda (n k)
            (last n (lambda (l i)
                    (k (cons l '()) i)))))
        (lambda (left right)
          (lambda (n k)
            (left n (lambda (l i) 
                      (right i (lambda (r j)
                                 (k (cons l r) j)))))))
        (lambda (s)
          (errorf 'proper-binary-tree-morpher 
                  "Not a proper binary tree. ~s" 
                  s)))
      bt) 
     0 (lambda (x i) x))))


;; ************
;; ** PART 6 **
;; ************

(define proper-binary-tree-breath-first-morpher
  (lambda (bt)
    (((bt-fold-right 
        (lambda (b)
          (lambda (n k)
            (k n (1+ n))))
        (lambda (last)
          (lambda (n k)
            (last n (lambda (l i)
                    (k (cons l '()) i)))))
        (lambda (left right)
          (lambda (n k)
            (k 
              (left n (lambda (l i) 
                          (right i (lambda (r j)
                                     (cons l r))))) n)))
        (lambda (s)
          (errorf 'proper-binary-tree-morpher 
                  "Not a proper binary tree. ~s" 
                  s)))
      bt) 
     0 (lambda (x i) x))))

;; ***********************************
;; ** Code from project description **
;; ***********************************

(define test-proper-binary-tree-morpher
  (lambda (candidate)
    (and (equal? ((candidate '3) 4)
                 '4)
         (equal? ((candidate '(1 (2) 3)) 4)
                 '(4 (4) 4))
         (equal? ((candidate '(3 . 3)) 5)
                 '(5 . 5))
         (equal? ((candidate '((1 . 1) . (2 . 2))) 5)
                 '((5 . 5) . (5 . 5)))
         (equal? ((candidate '((1 . 0) . 0)) 1)
                 '((1 . 1) . 1))
         (equal? ((candidate '(0 . (1 . 0))) 1)
                 '(1 . (1 . 1)))
         )))
         
(define test-proper-binary-tree-minmorpher
  (lambda (candidate)
    (and (equal? (candidate '3)
                 '3)
         (equal? (candidate '(1 (2) 3))
                 '(1 (1) 1))
         (equal? (candidate '(0 . 5))
                 '(0 . 0))
         (equal? (candidate '((1 . 0) . 0))
                 '((0 . 0) . 0))
         (equal? (candidate '(0 . (1 . 0)))
                 '(0 . (0 . 0)))
         (equal? (candidate '((0 . 2) . (1 . 2)))
                 '((0 . 0) . (0 . 0)))
                 )))
         
(define test-proper-binary-tree-maxmorpher
  (lambda (candidate)
    (and (equal? (candidate '3)
                 '3)
         (equal? (candidate '(1 (2) 3))
                 '(3 (3) 3))
         (equal? (candidate '((1 . 0) . 0))
                 '((1 . 1) . 1))
         (equal? (candidate '(0 . (1 . 0)))
                 '(1 . (1 . 1)))
         (equal? (candidate '(1 . 5))
                 '(5 . 5))
         (equal? (candidate '((3 . 2) . (1 . 5)))
                 '((5 . 5) . (5 . 5)))
         )))
         
(define test-proper-binary-tree-depth-first-morpher
  (lambda (candidate)
    (and (equal? (candidate '3)
                '0)
         (equal? (candidate '(((100))))
                 '(((0))))
         (equal? (candidate '(10 (2 -5) 3))
                 '(0 (1 2) 3))
         (equal? (candidate '(1 . 2))
                 '(0 . 1))
         (equal? (candidate '(2 . (4 . 5)))
                 '(0 . (1 . 2)))
         (equal? (candidate '((3 . 3) . 3))
                 '((0 . 1) . 2))
         (equal? (candidate '((4 . 3) . ( 24 . 4)))
                 '((0 . 1) . (2 . 3)))
         )))
         
(define test-proper-binary-tree-breath-first-morpher
  (lambda (candidate)
    (and (equal? (candidate '3)
                 '0)
         (equal? (candidate '(((100))))
                 '(((0))))
         (equal? (candidate '(10 (2 -5) . 3))
                 '(0 (2 3) . 1))
         (equal? (candidate '(((((10)))) (2 -5) 3))
                 '(((((5)))) (1 3) 2))
         (equal? (candidate '(3 . 3))
                 '(0 . 1))
         (equal? (candidate '(3 . (3 . 3)))
                 '(0 . (1 . 2)))
         (equal? (candidate '((3 . 3) . 3))
                 '((1 . 2) . 0))
         (equal? '(candidate '((3 . 3) . (3 . 3)))
                 '((0 . 1) . (2 . 3)))
         )))


;; *************
;; ** Testing **
;; *************

(define is-proper-binary-tree-morpher-valid?
  (test-proper-binary-tree-morpher proper-binary-tree-morpher))

(unless is-proper-binary-tree-morpher-valid?
  (printf "proper-binary-tree-morpher failed!"))

(define is-proper-binary-tree-minmorpher-valid?
  (test-proper-binary-tree-minmorpher proper-binary-tree-minmorpher-eval))

(unless is-proper-binary-tree-minmorpher-valid?
  (printf "proper-binary-tree-minmorpher failed!"))

(define is-proper-binary-tree-maxmorpher-valid?
  (test-proper-binary-tree-maxmorpher proper-binary-tree-maxmorpher-eval))

(unless is-proper-binary-tree-maxmorpher-valid?
  (printf "proper-binary-tree-maxmorpher failed!"))

(define is-proper-binary-tree-depth-first-morpher-valid?
  (test-proper-binary-tree-depth-first-morpher proper-binary-tree-depth-first-morpher))

(unless is-proper-binary-tree-depth-first-morpher-valid?
  (printf "proper-binary-tree-depth-first-morpher failed!"))

(define is-proper-binary-tree-breath-first-morpher-valid?
  (test-proper-binary-tree-breath-first-morpher proper-binary-tree-breath-first-morpher))

(unless is-proper-binary-tree-breath-first-morpher-valid?
  (printf "proper-binary-tree-breath-first-morpher failed!"))








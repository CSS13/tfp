(load "be_random.scm")
(load "exercise4.scm")

; Most of the functionality comes from the test boiler plate we made for
; exercise 4.4-5.
; Overall idea:
;   - The feeder feeds test cases to the tester.
;   - The tester is a curried wrap of some predicate.
;   - The test combines the tester and feeder.

(define test-be-normalize-random-feeder
  (lambda (tester)
    (let ([names '(a b c d e f g)])
      (tester (be-random names 10) names))))

(define test-be-normalize-all-environments-random
  (lambda (candidate)
    (test-be-normalize-random-feeder
      (be-normalize-all-environments-tester candidate))))

(test-be-normalize-all-environments-random normalize-boolean-expression)

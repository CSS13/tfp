(load "be_base.scm")
(load "be_nnf_base.scm")

(define embed-boolean-expression_nnf-into-boolean-expression
  (lambda (v)
    (cond
      [(is-posvar? v)
       (make-var (posvar-1 v))]
      [(is-negvar? v)
       (make-neg (make-var (negvar-1 v)))]
      [(is-conj_nnf? v)
       (make-conj (embed-boolean-expression_nnf-into-boolean-expression
                    (conj_nnf-1 v))
                  (embed-boolean-expression_nnf-into-boolean-expression
                    (conj_nnf-2 v)))]
      [(is-disj_nnf? v)
       (make-disj (embed-boolean-expression_nnf-into-boolean-expression
                    (disj_nnf-1 v))
                  (embed-boolean-expression_nnf-into-boolean-expression
                    (disj_nnf-2 v)))]
      [else
       (errorf 'embed-boolean-expression_nnf-into-boolean-expression
               "not a Boolean expression in negational normal form: ~s"
               v)])))

;; Hand writen well-formed boolean expressions
(define be0-nnf-well
  '(posvar x))

(define be1-nnf-well
  '(negvar y))

(define be2-nnf-well
  '(conj_nnf (posvar x)
             (negvar y)))

(define be3-nnf-well
  '(disj_nnf (negvar x)
             (posvar y)))

(define be4-nnf-well
  '(conj_nnf (disj_nnf (posvar x)
                       (negvar y))
             (disj_nnf (negvar z)
                       (negvar x))))

;; Hand written ill-formed boolean expressions
(define be0-nnf-ill
  '())

(define be1-nnf-ill
  '#t)

(define be2-nnf-ill
  '(conj_nnf (posvar #t)
             (car (x y))))

(define be3-nnf-ill
  '(disj_nnf (negvar "hello world")
             (+ a b)))

(define be4-nnf-ill
  '(neg (1 2 3)))

;; Test boolean expression syntax checker
(define test-be-nnf-check-well-formed
  (lambda (candidate)
    (and (candidate be0-nnf-well)
         (candidate be1-nnf-well)
         (candidate be2-nnf-well)
         (candidate be3-nnf-well)
         (candidate be4-nnf-well)
         ;;;
         )))

(define test-be-nnf-check-ill-formed
  (lambda (candidate)
    (not (or (candidate be0-nnf-ill)
             (candidate be1-nnf-ill)
             (candidate be2-nnf-ill)
             (candidate be3-nnf-ill)
             (candidate be4-nnf-ill)
             ;;;;
             ))))

;; Hand writen well-formed boolean expressions
(define be0-well
  '(var x))

(define be1-well
  '(conj (var x)
         (neg (var x))))

(define be2-well
  '(neg (conj (var x)
              (var y))))

(define be3-well
  '(disj (neg (var x))
         (neg (var y))))

(define be4-well
  '(conj (disj (var x)
               (neg (var y)))
         (neg (conj (var z)
                    (var x)))))

;; Hand written ill-formed boolean expressions
(define be0-ill
  '())

(define be1-ill
  '#t)

(define be2-ill
  '(conj (var #t)
         (car (x y))))

(define be3-ill
  '(disj (var "hello world")
         (+ a b)))

(define be4-ill
  '(neg (1 2 3)))

;; Test boolean expression syntax checker
(define test-be-check-well-formed
  (lambda (candidate)
    (and (candidate be0-well)
         (candidate be1-well)
         (candidate be2-well)
         (candidate be3-well)
         (candidate be4-well)
         ;;;
         )))

(define test-be-check-ill-formed
  (lambda (candidate)
    (not (or (candidate be0-ill)
             (candidate be1-ill)
             (candidate be2-ill)
             (candidate be3-ill)
             (candidate be4-ill)
             ;;;;
             ))))

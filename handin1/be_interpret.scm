(load "alist.scm")
(load "be_base.scm")

(define evaluate-boolean-expression
  (lambda (expression environment)
    (letrec ([visit (lambda (e)
                      (cond
                        [(is-var? e)
                         (alist-lookup (var-1 e)
                                       environment
                                       (lambda (v)
                                         v)
                                       (lambda (x)
                                         (errorf 'evaluate-boolean-expression
                                                 "unbound name: ~s"
                                                 x)))]
                        [(is-conj? e)
                         (let ([v1 (visit (conj-1 e))]
                               [v2 (visit (conj-2 e))])
                           (and v1 v2))]
                        [(is-disj? e)
                         (let ([v1 (visit (disj-1 e))]
                               [v2 (visit (disj-2 e))])
                           (or v1 v2))]
                        [(is-neg? e)
                         (let ([v (visit (neg-1 e))])
                           (not v))]
                        [else
                         (errorf 'evaluate-boolean-expression
                                 "illegal sub-expression: ~s"
                                 e)]))])
      (visit expression))))

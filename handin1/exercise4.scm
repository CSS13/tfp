(load "be_nnf_test.scm")
(load "be_nnf_check.scm")

;; See "be_nnf_test.scm" for the positive and negative tests of the
;; boolean expression in negational normal form syntax checker
(test-be-nnf-check-well-formed check-boolean-expression_nnf)
(test-be-nnf-check-ill-formed check-boolean-expression_nnf)

;; *********************
;; Exercise 4.2
;; *********************
(load "be_nnf_embed.scm")
(load "be_check.scm")

(define test-be-nnf-embed-well-formed
  (lambda (candidate)
    (test-be-nnf-check-well-formed
      (lambda (expression)
        (check-boolean-expression
          (candidate expression))))))

(test-be-nnf-embed-well-formed embed-boolean-expression_nnf-into-boolean-expression)

;; **********************
;; Exercise 4.3
;; **********************
(load "be_nnf_interpret.scm")

(define test-be-nnf-evaluate
  (lambda (candidate)
    (and (equal? (candidate be0-nnf-well (alist-extend 'x #t alist-mt)) #t)
         (equal? (candidate be1-nnf-well (alist-extend 'y #t alist-mt)) #f)
         (equal? (candidate be2-nnf-well (alist-extend 'x #t
                                           (alist-extend 'y #t
                                             alist-mt))) #f)
         (equal? (candidate be3-nnf-well (alist-extend 'x #t
                                           (alist-extend 'y #t
                                             alist-mt))) #t)
         (equal? (candidate be4-nnf-well (alist-extend 'x #t
                                           (alist-extend 'y #t
                                             (alist-extend 'z #t
                                               alist-mt)))) #f)
         ;;;
         )))

(test-be-nnf-evaluate evaluate-boolean-expression_nnf)

;; ***************
;; Exercise 4.4
;; ***************
(load "be_test.scm")
(load "be_interpret.scm")
(load "be_normalize.scm")

(define be-normalize-tester
  (lambda (candidate expression environment)
    (boolean=?
      (evaluate-boolean-expression_nnf (candidate expression)
                                       environment)
      (evaluate-boolean-expression expression
                                   environment))))

(define be-normalize-tester-helper
  (lambda (candidate)
    (lambda (expression environment)
      (be-normalize-tester candidate expression environment))))

(define test-be-normalize-feeder
  (lambda (tester)
    (and (tester be0-well (alist-extend 'x #t alist-mt))
         (tester be1-well (alist-extend 'x #t alist-mt))
         (tester be2-well (alist-extend 'x #t
                            (alist-extend 'y #t
                              alist-mt)))
         (tester be3-well (alist-extend 'x #t
                            (alist-extend 'y #t
                              alist-mt)))
         (tester be4-well (alist-extend 'x #t
                            (alist-extend 'y #t
                              (alist-extend 'z #t
                                alist-mt))))
         ;;;
         )))

(define test-be-normalize
  (lambda (candidate)
    (test-be-normalize-feeder
      (be-normalize-tester-helper candidate))))

(test-be-normalize normalize-boolean-expression)

;; ***************
;; Exercise 4.5
;; ***************
(load "be_util.scm")

(define all-possible-environments
  (lambda (xs)
    (if (null? xs)
        (list '())
        (let ([es (all-possible-environments (cdr xs))])
          (append (map (lambda (e)
                         (cons (cons (car xs) #t) e))
                       es)
                  (map (lambda (e)
                         (cons (cons (car xs) #f) e))
                       es))))))

(define be-normalize-all-environments-tester
  (lambda (candidate)
    (lambda (expression names)
      (let ([results (map (lambda (environment)
                            (be-normalize-tester candidate
                                                 expression
                                                 environment))
                          (all-possible-environments names))])
           (andmap1 (lambda (result)
                      (or (equal? (car results) result)
                          (errorf 'be-normalize-all-environments-tester
                                  "differing results: ~s and ~s"
                                  (car results)
                                  result)))
               (cdr results))))))

(define test-be-normalize-all-environments-feeder
  (lambda (tester)
    (and (tester be0-well '(x))
         (tester be1-well '(x))
         (tester be2-well '(x y))
         (tester be3-well '(x y))
         (tester be4-well '(x y z))
         ;;;
         )))

(define test-be-normalize-all-environments
  (lambda (candidate)
    (test-be-normalize-all-environments-feeder
      (be-normalize-all-environments-tester candidate))))

(test-be-normalize-all-environments normalize-boolean-expression)

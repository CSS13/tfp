(load "be_base.scm")

;; Use new defintion of is-var?
;; Automatically overrides the previous one, and changes the behavior of check-boolean-expressions
(define is-var?
  (lambda (v)
    (and (pair? v)
         (equal? (car v) 'var)
         (proper-list-of-given-length? (cdr v) 1)
         (symbol? (car (cdr v)))
    )))

(check-boolean-expression '(var 123))
(check-boolean-expression '(var x))

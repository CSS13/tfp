(load "be_util.scm")
(load "be_base.scm")

(define be-random
  (lambda (names depth)
    (if (and (integer? depth)
             (positive? depth))
        (((fold-right-nat (lambda ()
                            (make-var (list-ref-random names)))
                          (lambda (p)
                            (lambda ()
                              (case (random 4)
                                [(0)
                                 (make-conj (p) (p))]
                                [(1)
                                 (make-disj (p) (p))]
                                [(2)
                                 (make-neg (p))]
                                [else ; (3)
                                  (make-var (list-ref-random names))]
                                ))))
          depth))
        (errorf 'be-random
                "not a positive integer: ~s"
                depth))))

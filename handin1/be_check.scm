(load "be_base.scm")

(define check-boolean-expression
  (lambda (v)
    (cond
      [(is-var? v)
       #t]
      [(is-conj? v)
       (and (check-boolean-expression (conj-1 v))
            (check-boolean-expression (conj-2 v)))]
      [(is-disj? v)
       (and (check-boolean-expression (disj-1 v))
            (check-boolean-expression (disj-2 v)))]
      [(is-neg? v)
       (check-boolean-expression (neg-1 v))]
      [else
       #f])))

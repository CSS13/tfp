(load "be_nnf_base.scm")

(define check-boolean-expression_nnf
  (lambda (p)
    (cond
      [(is-posvar? p)
       #t]
      [(is-negvar? p)
       #t]
      [(is-conj_nnf? p)
       (and (check-boolean-expression_nnf (conj_nnf-1 p))
            (check-boolean-expression_nnf (conj_nnf-2 p)))]
      [(is-disj_nnf? p)
       (and (check-boolean-expression_nnf (disj_nnf-1 p))
            (check-boolean-expression_nnf (disj_nnf-2 p)))]
      [else
       #f])))

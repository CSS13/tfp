(load "be_util.scm")

;; <be_nnf> ::= (posvar <name>)
;;            | (negvar <name>)
;;            | (conj_nnf <be_nnf> <be_nnf>)
;;            | (disj_nnf <be_nnf> <be_nnf>)
;;
;; <name>   ::= ...

;; Constructors
(define make-posvar
  (lambda (v)
    (list 'posvar v)))

(define make-negvar
  (lambda (v)
    (list 'negvar v)))

(define make-conj_nnf
  (lambda (be1 be2)
    (list 'conj_nnf be1 be2)))

(define make-disj_nnf
  (lambda (be1 be2)
    (list 'disj_nnf be1 be2)))

;; Predicates
(define is-posvar?
  (lambda (v)
    (and (pair? v)
         (eqv? (car v) 'posvar)
         (proper-list-of-given-length? (cdr v) 1)
         (symbol? (cadr v)))))

(define is-negvar?
  (lambda (v)
    (and (pair? v)
         (eqv? (car v) 'negvar)
         (proper-list-of-given-length? (cdr v) 1)
         (symbol? (cadr v)))))

(define is-conj_nnf?
  (lambda (v)
    (and (pair? v)
         (eqv? (car v) 'conj_nnf)
         (proper-list-of-given-length? (cdr v) 2))))

(define is-disj_nnf?
  (lambda (v)
    (and (pair? v)
         (eqv? (car v) 'disj_nnf)
         (proper-list-of-given-length? (cdr v) 2))))

;; Accessors
(define posvar-1
  (lambda (v)
    (list-ref v 1)))

(define negvar-1
  (lambda (v)
    (list-ref v 1)))

(define conj_nnf-1
  (lambda (v)
    (list-ref v 1)))

(define conj_nnf-2
  (lambda (v)
    (list-ref v 2)))

(define disj_nnf-1
  (lambda (v)
    (list-ref v 1)))

(define disj_nnf-2
  (lambda (v)
    (list-ref v 2)))

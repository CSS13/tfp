(define proper-list-of-given-length?
  (lambda (v n)
    (or (and (null? v)
             (= n 0))
        (and (pair? v)
             (> n 0)
             (proper-list-of-given-length? (cdr v)
                                           (- n 1))))))

(define fold-right-nat
  (lambda (z s)
    (lambda (n)
      (letrec ([visit (lambda (m)
                        (if (= m 0)
                            z
                            (s (visit (1- m)))))])
        (visit n)))))

(define list-ref-random
  (lambda (xs)
    (if (pair? xs)
      (list-ref xs (random (length xs)))
      (errorf 'list-ref-random
              "not a proper list: ~s"
              xs))))

(define andmap1
  (lambda (p vs)
    (letrec ([visit (lambda (ws)
                      (if (null? ws)
                          #t
                          (and (p (car ws))
                               (visit (cdr ws)))))])
      (visit vs))))

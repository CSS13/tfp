(load "alist.scm")
(load "be_nnf_base.scm")

(define evaluate-boolean-expression_nnf
  (lambda (p env)
    (letrec ([visit (lambda (q)
                      (cond
                        [(is-posvar? q)
                         (alist-lookup (posvar-1 q) env
                                       (lambda (value)
                                         value)
                                       (lambda (name)
                                         (errorf 'evaluate-boolean-expression_nnf
                                                 "unbound name: ~s"
                                                 name)))]
                        [(is-negvar? q)
                         (alist-lookup (negvar-1 q) env
                                       (lambda (value)
                                         (not value))
                                       (lambda (name)
                                       (errorf 'evaluate-boolean-expression_nnf
                                               "unbound name: ~s"
                                               name)))]
                        [(is-conj_nnf? q)
                         (and (visit (conj_nnf-1 q))
                              (visit (conj_nnf-2 q)))]
                        [(is-disj_nnf? q)
                         (or (visit (disj_nnf-1 q))
                             (visit (disj_nnf-2 q)))]
                        [else
                         (errorf 'evaluate-boolean-expression_nnf
                                 "not a proper be_nnf: ~s"
                                 q)]))])
            (visit p))))

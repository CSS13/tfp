(define alist-mt
  '())

(define alist-extend
  (lambda (name value env)
    (cons (cons name value)
          env)))

(define alist-lookup
  (lambda (name env found not-found)
    (letrec ([visit (lambda (e)
                      (if (null? e)
                          (not-found name)
                          (let ([binding (car e)])
                            (if (equal? name (car binding))
                                (found (cdr binding))
                                (visit (cdr e))))))])
      (visit env))))

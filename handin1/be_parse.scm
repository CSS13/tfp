(load "be_util.scm")
(load "be_base.scm")

(define be-unparse
  (lambda (p)
    (cond
      [(is-var? p)
        (var-1 p)]
      [(is-conj? p)
       (list 'and (be-unparse (conj-1 p))
                  (be-unparse (conj-2 p)))]
      [(is-disj? p)
       (list 'or (be-unparse (disj-1 p))
                 (be-unparse (disj-2 p)))]
      [(is-neg? p)
       (list 'not (be-unparse (neg-1 p)))]
      [else
       (errorf 'be-unparse
               "not a proper be: ~s"
               p)])))

(define be-parse
  (lambda (p)
    (cond
      [(symbol? p)
       (make-var p)]
      [(proper-list-of-given-length? p 2)
       (if (equal? (car p) 'not)
           (make-neg (be-parse (list-ref p 1)))
           (errorf 'be-parse
                   "unknown operator: ~s"
                   p))]
      [(proper-list-of-given-length? p 3)
       (case (car p)
         [(and)
          (make-conj (be-parse (list-ref p 1))
                     (be-parse (list-ref p 2)))]
         [(or)
          (make-disj (be-parse (list-ref p 1))
                     (be-parse (list-ref p 2)))]
         [else
          (errorf 'be-parse
                  "unknown operator: ~s"
                  p)])]
      [else
       (errorf 'be-parse
               "not a valid concrete syntax: ~s"
               p)])))

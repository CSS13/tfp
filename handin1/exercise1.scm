(load "be_test.scm")
(load "be_check.scm")

;; See "be_test.scm" for the positive and negative tests of the
;; boolean expression syntax checker
(test-be-check-well-formed check-boolean-expression)
(test-be-check-ill-formed check-boolean-expression)

(load "be_parse.scm")

;; See "be_parse.scm" for the unparser and parser
(define parse-unparse-involution
  (lambda (parse unparse expression)
    (equal? (parse (unparse expression))
            expression)))

(define test-parse-unparse-limited-correctness
  (lambda (parse unparse)
    (and (parse-unparse-involution parse unparse '(var x))
         (parse-unparse-involution parse unparse '(conj (var x) (var y)))
         (parse-unparse-involution parse unparse '(disj (var x) (var y)))
         (parse-unparse-involution parse unparse '(neg (var x)))
         ;;;
         )))

;; Are your parser and unparser inverses of each other?

;; Yes, if we only consider symbols as valid variable names:
;;   (var x)    <=> x
;;   (conj p q) <=> (and p q)
;;   (disj p q) <=> (or p q)
;;   (neg p)    <=> (not p)

;; Since both parse and unparse are structurally recursive on p and q.

(test-parse-unparse-limited-correctness be-parse be-unparse)

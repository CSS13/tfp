(load "be_base.scm")
(load "be_nnf_base.scm")

(define normalize-boolean-expression
  (lambda (p)
    (letrec ([visit (lambda (q b)
        (cond
          [(is-var? q)
            (if b
              (make-posvar (var-1 q))
              (make-negvar (var-1 q)))]
          [(is-conj? q)
           (if b
             (make-conj_nnf (visit (conj-1 q) b)
                            (visit (conj-2 q) b))
             (make-disj_nnf (visit (conj-1 q) b)
                            (visit (conj-2 q) b)))]
          [(is-disj? q)
           (if b
             (make-disj_nnf (visit (disj-1 q) b)
                            (visit (disj-2 q) b))
             (make-conj_nnf (visit (conj-1 q) b)
                            (visit (conj-2 q) b)))]
          [(is-neg? q)
            (visit (neg-1 q) (not b))]
          [else
            (errorf 'normalize-boolean-expression
                    "not a proper be: ~s"
                    q)]
        ))])
    (visit p #t))))

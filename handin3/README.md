For each optimizing compiler in the file `magritte-and-the-40-optimizing-compilers.scm`

* characterize its optimization in English precisely and concisely.

See `english.txt`.

For our experimentation files see:

- `bizarre_experiment.scm`
- `curious_experiment.scm`
- `quaint_experiment.scm`
- `strange_experiment.scm`
- `surprising_experiment.scm`

These files characterize how we deduced the functionality described in `english.txt`.


* implement the corresponding optimizing Magritte interpreter and verify that it passes the unit tests.

Each interpreter has it's own file; `{{EXERCISE_NAME}}_magritte.scm`

See:

- `bizarre_magritte.scm`
- `curious_magritte.scm`
- `quaint_magritte.scm`
- `strange_magritte.scm`
- `surprising_magritte.scm`

For the right-fold `suprising_magritte` interpreter, see:

- `surprising_magritte_fold.scm`


* compare the relative efficiencies of interpreting with your optimizing Magritte interpreter and compiling and decompiling with the Magritte virtual machine.
*You might need to write / generate big arithmetic expressions and / or to repeat the comparison to detect a meaningful difference.*

We've implemented a procedure to generate random arithmetic expressions using `fold-right-nat`.

We've timed the bizarre optimizing Magritte interpreter against it's corresponding compiling + decompiling Magritte virtual machine.
Our results suggest that our optimizing Magritte interpreter is significantly slower than compiling and decompiling (24685ms vs 6728ms).

Our hypothesis is that the runtime of our Magritte interpreter is expoential in regards to the input.
We have derived a function mapping running times to input sizes, from the points;
```
(10, 10)
(15, 688)
(20, 24685)
```
Which yields the function:
```
f(x) = 0.01385705339*e^(0.7196580555*x)
```
Where 'x' is the input size, and f(x) is the runtime in milliseconds.

We have experimentally found evidence supporting our hypothesis, using the input sizes of '21' and '22';

21:
    Hypothesis; 50708.9782426
    Actual; 54473

22:
    Hypothesis; 104142.591904	
    Actual; 92600

We are unable to fully confirm our hypothesis however, as our sample size is too small and features too large variance.

See: `random.scm`

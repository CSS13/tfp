(load "surprising.scm")

;;; The corresponding optimizing Magritte interpreter:
(define interpret-arithmetic-expression_Magritte_surprising
  (lambda (e)
    (cond
      [(is-literal? e)
       (make-literal (literal-1 e))
      ]
      [(is-plus? e)
       (let ([ e1 (interpret-arithmetic-expression_Magritte_surprising (plus-1 e))]
             [ e2 (interpret-arithmetic-expression_Magritte_surprising (plus-2 e))])
         (if (and (is-literal? e1)(eqv? (literal-1 e1) 0))
            e2
            (if (and (is-literal? e2)(eqv? (literal-1 e2) 0))
                e1
                (make-plus e1 e2)))
          )]
      [(is-times? e)
       (let ([ e1 (interpret-arithmetic-expression_Magritte_surprising (times-1 e))]
             [ e2 (interpret-arithmetic-expression_Magritte_surprising (times-2 e))])
         (cond
            [(and (is-literal? e1)(eqv? (literal-1 e1) 0))
              (make-literal 0)]
            [(and (is-literal? e2)(eqv? (literal-1 e2) 0))
              (make-literal 0)]
            [(and (is-literal? e1)(eqv? (literal-1 e1) 1))
              e2]
            [(and (is-literal? e2)(eqv? (literal-1 e2) 1))
              e1]
            [else
              (make-times e1 e2)])
          )]
      [else
        (errorf 'interpret-arithmetic-expression_Magritte_surprising
                "unrecognized expression: ~s"
                e)])))


(define experimenter
  (lambda (candidate)
      (lambda (e)
        (unparse-arithmetic-expression (candidate (parse-arithmetic-expression e))))))

((experimenter interpret-arithmetic-expression_Magritte_surprising)
    '(+ 1 0))

((experimenter interpret-arithmetic-expression_Magritte_surprising)
    '(+ 0 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising) 
    '(+ 1 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising) 
    '(* 0 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising) 
    '(* 1 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising) 
    '(* 1 0))
    
((experimenter interpret-arithmetic-expression_Magritte_surprising) 
    '(* 1 5))
    
((experimenter interpret-arithmetic-expression_Magritte_surprising) 
    '(* 5 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising) 
    '(* 0 5))
    
((experimenter interpret-arithmetic-expression_Magritte_surprising) 
    '(* 5 0))
    
    
(define does_interpret-arithmetic-expression_Magritte_surprising_make_the_diagram_commute?
  (lambda (source-ae)
    (let ([ae (parse-arithmetic-expression source-ae)])
      (equal? (interpret-arithmetic-expression_Magritte_surprising ae)
              (compile-and-run-arithmetic-expression_Magritte_surprising ae)))))

(define test_does_interpret-arithmetic-expression_Magritte_surprising_make_the_diagram_commute?
  (lambda ()
    (andmap does_interpret-arithmetic-expression_Magritte_surprising_make_the_diagram_commute?
            sample-of-arithmetic-expressions)))

;;; Uncomment the following lines to test your implementation when loading this file:
 (unless (test_does_interpret-arithmetic-expression_Magritte_surprising_make_the_diagram_commute?)
   (printf "(test_does_interpret-arithmetic-expression_Magritte_surprising_make_the_diagram_commute?) failed~n"))

;;; In plain English, which surprising program transformation is performed?

(define is_interpret-arithmetic-expression_Magritte_surprising_idempotent?
  (lambda (source-ae)
    (let* ([ae (parse-arithmetic-expression source-ae)]
           [ae_optimized (interpret-arithmetic-expression_Magritte_surprising ae)])
      (equal? ae_optimized
              (interpret-arithmetic-expression_Magritte_surprising ae_optimized)))))

(define test_is_interpret-arithmetic-expression_Magritte_surprising_idempotent?
  (lambda ()
    (andmap is_interpret-arithmetic-expression_Magritte_surprising_idempotent?
            sample-of-arithmetic-expressions)))

;;; Uncomment the following lines to test your implementation when loading this file:
 (unless (test_is_interpret-arithmetic-expression_Magritte_surprising_idempotent?)
   (printf "(test_is_interpret-arithmetic-expression_Magritte_surprising_idempotent?) failed~n"))

;;;;;;;;;;



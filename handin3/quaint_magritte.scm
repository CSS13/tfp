(load "quaint.scm")

;;; The corresponding optimizing Magritte interpreter:

(define interpret-arithmetic-expression_Magritte_quaint
  (lambda (e_init)
    (letrec ([visit
              (lambda (e k)
                (cond
                  [(is-literal? e)
                   (k e)]
                  [(is-plus? e)
                   (make-plus (visit (plus-1 e) k)
                              (visit (plus-2 e) k))]
                  [(is-times? e)
                   (visit (times-1 e)
                          (lambda (e1)
                            (visit (times-2 e)
                                   (lambda (e2)
                                     (k (make-times e1 e2))))))]
                  [else
                   (errorf 'interpret-arithmetic-expression_Magritte_quaint
                           "not a arithmetic expression: ~s"
                           e)]))])
      (visit e_init (lambda (e) e)))))

(define does_interpret-arithmetic-expression_Magritte_quaint_make_the_diagram_commute?
  (lambda (source-ae)
    (let ([ae (parse-arithmetic-expression source-ae)])
      (equal? (interpret-arithmetic-expression_Magritte_quaint ae)
              (compile-and-run-arithmetic-expression_Magritte_quaint ae)))))

(define test_does_interpret-arithmetic-expression_Magritte_quaint_make_the_diagram_commute?
  (lambda ()
    (andmap does_interpret-arithmetic-expression_Magritte_quaint_make_the_diagram_commute?
            sample-of-arithmetic-expressions)))

;;; Uncomment the following lines to test your implementation when loading this file:
(unless (test_does_interpret-arithmetic-expression_Magritte_quaint_make_the_diagram_commute?)
  (printf "(test_does_interpret-arithmetic-expression_Magritte_quaint_make_the_diagram_commute?) failed~n"))

;;; In plain English, which quaint program transformation is performed?

(define is_interpret-arithmetic-expression_Magritte_quaint_idempotent?
  (lambda (source-ae)
    (let* ([ae (parse-arithmetic-expression source-ae)]
           [ae_optimized (interpret-arithmetic-expression_Magritte_quaint ae)])
      (equal? ae_optimized
              (interpret-arithmetic-expression_Magritte_quaint ae_optimized)))))

(define test_is_interpret-arithmetic-expression_Magritte_quaint_idempotent?
  (lambda ()
    (andmap is_interpret-arithmetic-expression_Magritte_quaint_idempotent?
            sample-of-arithmetic-expressions)))

;;; Uncomment the following lines to test your implementation when loading this file:
(unless (test_is_interpret-arithmetic-expression_Magritte_quaint_idempotent?)
  (printf "(test_is_interpret-arithmetic-expression_Magritte_quaint_idempotent?) failed~n"))

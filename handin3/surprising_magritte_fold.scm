(define fold-right_arithmetic-expression
  (lambda (lit node_plus node_times err)
    (lambda (v_init)
      (letrec ([visit (lambda (v)
                        (cond
                          [(is-literal? v)
                           (lit (literal-1 v))]
                          [(is-plus? v)
                           (node_plus (visit (plus-1 v))
                                      (visit (plus-2 v)))]
                          [(is-times? v)
                           (node_times (visit (times-1 v))
                                       (visit (times-2 v)))]
                          [else
                           (err v)]))])
        (visit v_init)))))

(load "surprising.scm")

;;; The corresponding optimizing Magritte interpreter:
(define interpret-arithmetic-expression_Magritte_surprising_fold
  (lambda (init_e)
    (((fold-right_arithmetic-expression
        (lambda (e)
          (lambda ()
            (make-literal e)))
        (lambda (ex ey)
          (lambda ()
            (let ([ e1 (ex)]
                  [ e2 (ey)])
              (if (and (is-literal? e1)(eqv? (literal-1 e1) 0))
                e2
                (if (and (is-literal? e2)(eqv? (literal-1 e2) 0))
                  e1
                  (make-plus e1 e2)))
              )))
        (lambda (ex ey)
          (lambda ()
             (let ([ e1 (ex)]
                   [ e2 (ey)])
             (cond
                [(and (is-literal? e1)(eqv? (literal-1 e1) 0))
                  (make-literal 0)]
                [(and (is-literal? e2)(eqv? (literal-1 e2) 0))
                  (make-literal 0)]
                [(and (is-literal? e1)(eqv? (literal-1 e1) 1))
                  e2]
                [(and (is-literal? e2)(eqv? (literal-1 e2) 1))
                  e1]
                [else
                  (make-times e1 e2)])
              )))
        (lambda (e)
          (lambda ()
            (errorf 'interpret-arithmetic-expression_Magritte_surprising_fold
                    "unrecognized expression: ~s"
                    (e)))))
      init_e))))

(define experimenter
  (lambda (candidate)
      (lambda (e)
        (unparse-arithmetic-expression (candidate (parse-arithmetic-expression e))))))

((experimenter interpret-arithmetic-expression_Magritte_surprising_fold)
    '(+ 1 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising_fold)
    '(+ 0 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising_fold) 
    '(+ 1 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising_fold) 
    '(* 0 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising_fold) 
    '(* 1 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising_fold) 
    '(* 1 0))
    
((experimenter interpret-arithmetic-expression_Magritte_surprising_fold) 
    '(* 1 5))
    
((experimenter interpret-arithmetic-expression_Magritte_surprising_fold) 
    '(* 5 1))

((experimenter interpret-arithmetic-expression_Magritte_surprising_fold) 
    '(* 0 5))
    
((experimenter interpret-arithmetic-expression_Magritte_surprising_fold) 
    '(* 5 0))
    
    
(define does_interpret-arithmetic-expression_Magritte_surprising_fold_make_the_diagram_commute?
  (lambda (source-ae)
    (let ([ae (parse-arithmetic-expression source-ae)])
      (equal? (interpret-arithmetic-expression_Magritte_surprising_fold ae)
              (compile-and-run-arithmetic-expression_Magritte_surprising ae)))))

(define test_does_interpret-arithmetic-expression_Magritte_surprising_fold_make_the_diagram_commute?
  (lambda ()
    (andmap does_interpret-arithmetic-expression_Magritte_surprising_fold_make_the_diagram_commute?
            sample-of-arithmetic-expressions)))

;;; Uncomment the following lines to test your implementation when loading this file:
 (unless (test_does_interpret-arithmetic-expression_Magritte_surprising_fold_make_the_diagram_commute?)
   (printf "(test_does_interpret-arithmetic-expression_Magritte_surprising_fold_make_the_diagram_commute?) failed~n"))

;;; In plain English, which surprising program transformation is performed?

(define is_interpret-arithmetic-expression_Magritte_surprising_fold_idempotent?
  (lambda (source-ae)
    (let* ([ae (parse-arithmetic-expression source-ae)]
           [ae_optimized (interpret-arithmetic-expression_Magritte_surprising_fold ae)])
      (equal? ae_optimized
              (interpret-arithmetic-expression_Magritte_surprising_fold ae_optimized)))))

(define test_is_interpret-arithmetic-expression_Magritte_surprising_fold_idempotent?
  (lambda ()
    (andmap is_interpret-arithmetic-expression_Magritte_surprising_fold_idempotent?
            sample-of-arithmetic-expressions)))

;;; Uncomment the following lines to test your implementation when loading this file:
 (unless (test_is_interpret-arithmetic-expression_Magritte_surprising_fold_idempotent?)
   (printf "(test_is_interpret-arithmetic-expression_Magritte_surprising_fold_idempotent?) failed~n"))

;;;;;;;;;;



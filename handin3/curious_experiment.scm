(load "curious.scm")

(define experimenter
  (lambda (candidate)
      (lambda (e)
        (unparse-arithmetic-expression (candidate (parse-arithmetic-expression e))))))

(define help
  (lambda (candidate)
    (lambda (e)
        (equal? e
                ((experimenter candidate) e)))))

((help compile-and-run-arithmetic-expression_Magritte_curious)
    '(+ 0 (+ 1 2)))

((experimenter compile-and-run-arithmetic-expression_Magritte_curious)
    '(* 0 (+ 1 2)))

((experimenter compile-and-run-arithmetic-expression_Magritte_curious)
    '(* (+ 0 1) (+ 2 3)))

((experimenter compile-and-run-arithmetic-expression_Magritte_curious)
    '(* 1 (* 1 2)))


;; a * (b + c) <=> a * b + a * c
;; (a + b) * c <=> a * c + b * c
;; (a + b) * (c + d) <=> (a * c + b * c) + (a * d + b * d)

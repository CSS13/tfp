(load "bizarre.scm")

(define experimenter
  (lambda (candidate)
      (lambda (e)
        (unparse-arithmetic-expression (candidate (parse-arithmetic-expression e))))))

;; Plus
((experimenter compile-and-run-arithmetic-expression_Magritte_bizarre) 
    '(+ (+ 1 2) 3))

((experimenter compile-and-run-arithmetic-expression_Magritte_bizarre) 
    '(+ (+ (+ 0 1) 2) 3))

((experimenter compile-and-run-arithmetic-expression_Magritte_bizarre) 
    '(+ (+ (+ 0 1) (+ 2 3)) (+ (+ 4 5) (+ 6 7))))

;; Times
((experimenter compile-and-run-arithmetic-expression_Magritte_bizarre) 
    '(* (* 1 2) 3))

((experimenter compile-and-run-arithmetic-expression_Magritte_bizarre) 
    '(* (* (* 0 1) 2) 3))

((experimenter compile-and-run-arithmetic-expression_Magritte_bizarre) 
    '(* (* (* 0 1) (* 2 3)) (* (* 4 5) (* 6 7))))

;; Combined
((experimenter compile-and-run-arithmetic-expression_Magritte_bizarre) 
    '(* (+ (+ 0 1) 2) 3))

((experimenter compile-and-run-arithmetic-expression_Magritte_bizarre) 
    '(+ (* (* 0 1) (* 2 3)) (* (* 4 5) (* 6 7))))

((experimenter compile-and-run-arithmetic-expression_Magritte_bizarre) 
    '(* (* (* 0 1) (+ 2 3)) (* (* 4 5) (* 6 7))))

;; ==>
;; ((a + b) + c) <=> (a + (b + c))
;; ((a * b) * c) <=> (a * (b * c))


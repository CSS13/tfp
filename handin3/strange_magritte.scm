(load "strange.scm")

;;; The corresponding optimizing Magritte interpreter:

(define interpret-arithmetic-expression_Magritte_strange
  (lambda (e_init)
    (letrec ([visit
              (lambda (e)
                (cond
                  [(is-literal? e)
                   e]
                  [(is-plus? e)
                   (visit-plus e '())]
                  [(is-times? e)
                   (visit-times e '())]
                  [else
                   (errorf 'interpret-arithmetic-expression_Magritte_strange
                           "unrecognized expression: ~s"
                           e)]))]
             [visit-plus
              (lambda (e a)
                (cond
                  [(is-literal? e)
                   (if (not (null? a))
                       (make-plus a e)
                       e)]
                  [(is-plus? e)
                   (visit-plus (plus-2 e)
                               (visit-plus (plus-1 e)
                                           a))]
                  [(is-times? e)
                   (if (not (null? a))
                       (make-plus a (visit-times e '()))
                       (visit-times e '()))]
                  [else
                   (errorf 'interpret-arithmetic-expression_Magritte_strange
                           "unrecognized expression: ~s"
                           e)]))]
             [visit-times
              (lambda (e a)
                (cond
                  [(is-literal? e)
                   (if (not (null? a))
                       (make-times a e)
                       e)]
                  [(is-plus? e)
                   (if (not (null? a))
                       (make-times a (visit-plus e '()))
                       (visit-plus e '()))]
                  [(is-times? e)
                   (visit-times (times-2 e)
                                (visit-times (times-1 e)
                                             a))]
                  [else
                   (errorf 'interpret-arithmetic-expression_Magritte_strange
                           "unrecognized expression: ~s"
                           e)]))])
      (visit e_init))))

(define experimenter
  (lambda (candidate)
      (lambda (e)
        (unparse-arithmetic-expression (candidate (parse-arithmetic-expression e))))))

((experimenter interpret-arithmetic-expression_Magritte_strange)
    '(+ 1 (+ 2 (+ 3 4))))

((experimenter compile-and-run-arithmetic-expression_Magritte_strange)
    '(+ 1 (+ 2 (+ 3 4))))

((experimenter interpret-arithmetic-expression_Magritte_strange)
    '(* (* 0 1) 2))

((experimenter compile-and-run-arithmetic-expression_Magritte_strange)
    '(* (* 0 1) 2))

(define does_interpret-arithmetic-expression_Magritte_strange_make_the_diagram_commute?
  (lambda (source-ae)
    (let ([ae (parse-arithmetic-expression source-ae)])
      (equal? (interpret-arithmetic-expression_Magritte_strange ae)
              (compile-and-run-arithmetic-expression_Magritte_strange ae)))))

(define test_does_interpret-arithmetic-expression_Magritte_strange_make_the_diagram_commute?
  (lambda ()
    (andmap does_interpret-arithmetic-expression_Magritte_strange_make_the_diagram_commute?
            sample-of-arithmetic-expressions)))

;;; Uncomment the following lines to test your implementation when loading this file:
(unless (test_does_interpret-arithmetic-expression_Magritte_strange_make_the_diagram_commute?)
   (printf "(test_does_interpret-arithmetic-expression_Magritte_strange_make_the_diagram_commute?) failed~n"))

;;; In plain English, which strange program transformation is performed?

(define is_interpret-arithmetic-expression_Magritte_strange_idempotent?
  (lambda (source-ae)
    (let* ([ae (parse-arithmetic-expression source-ae)]
           [ae_optimized (interpret-arithmetic-expression_Magritte_strange ae)])
      (equal? ae_optimized
              (interpret-arithmetic-expression_Magritte_strange ae_optimized)))))

(define test_is_interpret-arithmetic-expression_Magritte_strange_idempotent?
  (lambda ()
    (andmap is_interpret-arithmetic-expression_Magritte_strange_idempotent?
            sample-of-arithmetic-expressions)))

;;; Uncomment the following lines to test your implementation when loading this file:
(unless (test_is_interpret-arithmetic-expression_Magritte_strange_idempotent?)
   (printf "(test_is_interpret-arithmetic-expression_Magritte_strange_idempotent?) failed~n"))

(load "surprising.scm")

(define experimenter
  (lambda (candidate)
      (lambda (e)
        (unparse-arithmetic-expression (candidate (parse-arithmetic-expression e))))))

(define help
  (lambda (candidate)
    (lambda (e)
        (equal? e
                ((experimenter candidate) e)))))
                
;((help compile-and-run-arithmetic-expression_Magritte_surprising)
;    '(+ 0 (+ 1 2)))

;; 0 * x = 0
((experimenter compile-and-run-arithmetic-expression_Magritte_surprising) 
    '(* 0 1))

;; 1 * x = x
((experimenter compile-and-run-arithmetic-expression_Magritte_surprising) 
    '(* 1 13))

;; x + 0 = x
((experimenter compile-and-run-arithmetic-expression_Magritte_surprising) 
    '(+ 1 0))

;; x * 0 = 0
((experimenter compile-and-run-arithmetic-expression_Magritte_surprising) 
    '(* 1 0))

;; x * 1= x
((experimenter compile-and-run-arithmetic-expression_Magritte_surprising) 
    '(* 13 1))

;; 0 + x = x
((experimenter compile-and-run-arithmetic-expression_Magritte_surprising) 
    '(+ 0 1))

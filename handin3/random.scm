(define fold-right-nat
  (lambda (z s)
    (lambda (n)
      (letrec ([visit (lambda (m)
                        (if (= m 0)
                            z
                            (s (visit (1- m)))))])
        (visit n)))))

;;; Generate random arithmetic expressions
(define arithmetic-expression-random
  (lambda (depth)
    (if (and (integer? depth)
             (positive? depth))
        (((fold-right-nat (lambda ()
                            (make-literal (random 10)))
                          (lambda (p)
                            (lambda ()
                              (case (random 2)
                                [(0)
                                 (make-plus (p) (p))]
                                [(1)
                                 (make-times (p) (p))]
                                ))))
          depth))
        (errorf 'be-random
                "not a positive integer: ~s"
                depth))))

;;; Generate random arithmetic expressions
(define arithmetic-expression-random-non-full
  (lambda (depth)
    (if (and (integer? depth)
             (positive? depth))
        (((fold-right-nat (lambda ()
                            (make-literal (random 10)))
                          (lambda (p)
                            (lambda ()
                              (case (random 3)
                                [(0)
                                 (make-plus (p) (p))]
                                [(1)
                                 (make-times (p) (p))]
                                [(2)
                                 (make-literal (random 10))]
                                ))))
          depth))
        (errorf 'be-random
                "not a positive integer: ~s"
                depth))))

;;; Generate an arithmetic expression with maximum depth of 10
(arithmetic-expression-random 10)

(define timer
  (lambda (candidate expression)
    (time (unparse-arithmetic-expression (candidate expression)))))

(load "bizarre_magritte.scm")
(define diff_timer
  (lambda (depth)
    (let ([expression (arithmetic-expression-random depth)])
          ((lambda () (timer interpret-arithmetic-expression_Magritte_bizarre expression) 0))
          ((lambda () (timer compile-and-run-arithmetic-expression_Magritte_bizarre expression) 0)))))

(diff_timer 22)

;;; > (diff_timer 10)
;;; (time (unparse-arithmetic-expression (candidate expression)))
;;;     1 collection
;;;     10 ms elapsed cpu time, including 0 ms collecting
;;;     9 ms elapsed real time, including 0 ms collecting
;;;     1470480 bytes allocated, including 9768832 bytes reclaimed
;;; (time (unparse-arithmetic-expression (candidate expression)))
;;;     no collections
;;;     2 ms elapsed cpu time
;;;     2 ms elapsed real time
;;;     893680 bytes allocated
;;; 
;;; > (diff_timer 15)
;;; (time (unparse-arithmetic-expression (candidate expression)))
;;;     13 collections
;;;     688 ms elapsed cpu time, including 38 ms collecting
;;;     688 ms elapsed real time, including 39 ms collecting
;;;     107379168 bytes allocated, including 109872768 bytes reclaimed
;;; (time (unparse-arithmetic-expression (candidate expression)))
;;;     4 collections
;;;     70 ms elapsed cpu time, including 11 ms collecting
;;;     70 ms elapsed real time, including 11 ms collecting
;;;     31264080 bytes allocated, including 29680160 bytes reclaimed
;;; 
;;; > (diff_timer 20)
;;; (time (unparse-arithmetic-expression (candidate expression)))
;;;     439 collections
;;;     24685 ms elapsed cpu time, including 2864 ms collecting
;;;     24665 ms elapsed real time, including 2892 ms collecting
;;;     3690210320 bytes allocated, including 3909076160 bytes reclaimed
;;; (time (unparse-arithmetic-expression (candidate expression)))
;;;     133 collections
;;;     2962 ms elapsed cpu time, including 974 ms collecting
;;;     2960 ms elapsed real time, including 976 ms collecting
;;;     1140619296 bytes allocated, including 948909440 bytes reclaimed
;;; 
;;; > (diff_timer 21)
;;; (time (unparse-arithmetic-expression (candidate expression)))
;;;     849 collections
;;;     54473 ms elapsed cpu time, including 12701 ms collecting
;;;     54430 ms elapsed real time, including 12701 ms collecting
;;;     7142694336 bytes allocated, including 6576998816 bytes reclaimed
;;; (time (unparse-arithmetic-expression (candidate expression)))
;;;     256 collections
;;;     7714 ms elapsed cpu time, including 3424 ms collecting
;;;     7708 ms elapsed real time, including 3417 ms collecting
;;;     2184144816 bytes allocated, including 2698877296 bytes reclaimed
;;; 
;;; > (diff_timer 22)
;;; (time (unparse-arithmetic-expression (candidate expression)))
;;;     1328 collections
;;;     92600 ms elapsed cpu time, including 22642 ms collecting
;;;     92535 ms elapsed real time, including 22639 ms collecting
;;;     11178600704 bytes allocated, including 9134381520 bytes reclaimed
;;; (time (unparse-arithmetic-expression (candidate expression)))
;;;     546 collections
;;;     17964 ms elapsed cpu time, including 9265 ms collecting
;;;     17953 ms elapsed real time, including 9242 ms collecting
;;;     4681287376 bytes allocated, including 5855051328 bytes reclaimed
;;; 

(load "quaint_magritte.scm")
(define diff_timer
  (lambda (depth)
    (let ([expression (arithmetic-expression-random depth)])
          ((lambda () (timer interpret-arithmetic-expression_Magritte_quaint expression) 0))
          ((lambda () (timer compile-and-run-arithmetic-expression_Magritte_quaint expression) 0)))))

(diff_timer 30)

(load "strange_magritte.scm")
(define diff_timer
  (lambda (depth)
    (let ([expression (arithmetic-expression-random depth)])
          ((lambda () (timer interpret-arithmetic-expression_Magritte_strange expression) 0))
          ((lambda () (timer compile-and-run-arithmetic-expression_Magritte_strange expression) 0)))))

(diff_timer 50)

(load "curious_magritte.scm")
(define diff_timer
  (lambda (depth)
    (let ([expression (arithmetic-expression-random depth)])
          ((lambda () (timer interpret-arithmetic-expression_Magritte_curious expression) 0))
          ((lambda () (timer compile-and-run-arithmetic-expression_Magritte_curious expression) 0)))))

(diff_timer 50)

(load "surprising_magritte.scm")
(define diff_timer
  (lambda (depth)
    (let ([expression (arithmetic-expression-random depth)])
          ((lambda () (timer interpret-arithmetic-expression_Magritte_surprising expression) 0))
          ((lambda () (timer compile-and-run-arithmetic-expression_Magritte_surprising expression) 0)))))

(diff_timer 50)

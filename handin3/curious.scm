(load "magritte-and-the-40-optimizing-compilers.scm")

;;;;;;;;;;

;;; Curious exercise:

(define compile-arithmetic-expression_curious
  (lambda (e)
    (letrec ([visit
              (lambda (e k mk)
                (cond
                  [(is-literal? e)
                   (k (list (make-PUSH (literal-1 e))) mk)]
                  [(is-plus? e)
                   (visit (plus-1 e)
                          k
                          (lambda (e1)
                            (visit (plus-2 e)
                                   k
                                   (lambda (e2)
                                     (mk (append e1 e2 (list (make-ADD))))))))]
                  [(is-times? e)
                   (visit (times-2 e)
                          (lambda (e2 mk)
                            (visit (times-1 e)
                                   (lambda (e1 mk)
                                     (k (append e1 e2 (list (make-MUL))) mk))
                                   mk))
                          mk)]
                  [else
                   (errorf 'compile-arithmetic-expression_curious
                           "unrecognized expression: ~s"
                           e)]))])
      (visit e (lambda (e mk) (mk e)) make-byte-code-program))))

(unless (test-compile-and-run-arithmetic-expression compile-arithmetic-expression_curious run-byte-code-program)
  (printf "(test-compile-and-run-arithmetic-expression compile-arithmetic-expression_curious run-byte-code-program) failed~n"))

;;; The corresponding "just-in-time" optimizing compiler:

(define compile-and-run-arithmetic-expression_curious
  (lambda (ae)
    (run-byte-code-program (compile-arithmetic-expression_curious ae))))

(unless (test-interpret-arithmetic-expression compile-and-run-arithmetic-expression_curious)
  (printf "(test-interpret-arithmetic-expression compile-and-run-arithmetic-expression_curious) failed~n"))

(define does_interpret-arithmetic-expression_make_the_curious_diagram_commute?
  (lambda (source-ae)
    (let ([ae (parse-arithmetic-expression source-ae)])
      (equal? (interpret-arithmetic-expression ae)
              (compile-and-run-arithmetic-expression_curious ae)))))

(define test_does_interpret-arithmetic-expression_make_the_curious_diagram_commute?
  (lambda ()
    (andmap does_interpret-arithmetic-expression_make_the_curious_diagram_commute?
            sample-of-arithmetic-expressions)))

(unless (test_does_interpret-arithmetic-expression_make_the_curious_diagram_commute?)
  (printf "(test_does_interpret-arithmetic-expression_make_the_curious_diagram_commute?) failed~n"))

;;; The corresponding "just-in-time" optimizing decompiler:

(define compile-and-run-arithmetic-expression_Magritte_curious
  (lambda (ae)
    (run-byte-code-program_Magritte (compile-arithmetic-expression_curious ae))))

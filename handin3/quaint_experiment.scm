(load "quaint.scm")

(define experimenter
  (lambda (candidate)
      (lambda (e)
        (unparse-arithmetic-expression (candidate (parse-arithmetic-expression e))))))

(define help
  (lambda (candidate)
    (lambda (e)
        (equal? e
                ((experimenter candidate) e)))))


((help compile-and-run-arithmetic-expression_Magritte_quaint)
    '(* (+ 0 1) (+ 2 3)))


;; Plus
((experimenter compile-and-run-arithmetic-expression_Magritte_quaint)
    '(+ 1 (* 2 3)))

((experimenter compile-and-run-arithmetic-expression_Magritte_quaint)
    '(* (+ 0 1) (+ 2 3)))

;; a * (b + c) <=> a * b + a * c
;; (a + b) * c <=> a * c + b * c
;; (a + b) * (c + d) <=> (a * c + a * d) + (b * c + b * d)
